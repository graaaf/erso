-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.25 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2012-10-04 00:06:12
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for esro_site
CREATE DATABASE IF NOT EXISTS `esro_site` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `esro_site`;


-- Dumping structure for table esro_site.auth_assignments
CREATE TABLE IF NOT EXISTS `auth_assignments` (
  `itemname` varchar(64) NOT NULL,
  `userid` int(11) unsigned NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.auth_assignments: 8 rows
/*!40000 ALTER TABLE `auth_assignments` DISABLE KEYS */;
REPLACE INTO `auth_assignments` (`itemname`, `userid`, `bizrule`, `data`) VALUES
	('admin', 1, NULL, NULL),
	('moderator', 2, NULL, NULL),
	('user', 3, NULL, NULL),
	('user', 4, NULL, NULL),
	('user', 40, NULL, NULL),
	('user', 39, NULL, NULL),
	('user', 38, NULL, NULL),
	('moderator', 37, NULL, NULL);
/*!40000 ALTER TABLE `auth_assignments` ENABLE KEYS */;


-- Dumping structure for table esro_site.auth_items
CREATE TABLE IF NOT EXISTS `auth_items` (
  `name` varchar(64) NOT NULL COMMENT 'Название',
  `type` int(11) NOT NULL COMMENT 'Тип',
  `description` text COMMENT 'Описание',
  `bizrule` text COMMENT 'Бизнес-правило',
  `data` text COMMENT 'Данные',
  `allow_for_all` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Доступно всем',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.auth_items: 276 rows
/*!40000 ALTER TABLE `auth_items` DISABLE KEYS */;
REPLACE INTO `auth_items` (`name`, `type`, `description`, `bizrule`, `data`, `allow_for_all`) VALUES
	('ActionAdmin_Create', 0, 'Добавление мероприятия (админка)', NULL, 'N;', 0),
	('ActionAdmin_Delete', 0, 'Удаление мероприятия (админка)', NULL, 'N;', 0),
	('ActionAdmin_Manage', 0, 'Управление мероприятиями (админка)', NULL, 'N;', 0),
	('ActionAdmin_Update', 0, 'Редактирование мероприятия (админка)', NULL, 'N;', 0),
	('ActionAdmin_View', 0, 'Просмотр мероприятия (админка)', NULL, 'N;', 0),
	('ActionFileAdmin_Create', 0, 'Добавление файла мероприятия (админка)', NULL, 'N;', 0),
	('ActionFileAdmin_Delete', 0, 'Удаление файла мероприятия (админка)', NULL, 'N;', 0),
	('ActionFileAdmin_Manage', 0, 'Управление файлами мероприятий (админка)', NULL, 'N;', 0),
	('Actions_Admin', 1, 'Управление мероприятиями', '', 's:0:"";', 0),
	('Action_Index', 0, 'Просмотр списка мероприятий', NULL, 'N;', 1),
	('Action_View', 0, 'Просмотр мероприятия', NULL, 'N;', 1),
	('admin', 2, 'Администратор', '', 's:0:"";', 0),
	('Admin_Cities_Countries', 1, 'Управление городами и странами', NULL, 's:0:"";', 0),
	('Admin_Content', 1, 'Управление контентом', '', 's:0:"";', 0),
	('Admin_Documents', 1, 'Управление документами', '', 's:0:"";', 0),
	('Admin_Faq', 1, 'Управление вопросами', '', 's:0:"";', 0),
	('Admin_Feedback', 1, 'Управление обратной связью', '', 's:0:"";', 0),
	('Admin_Languages', 1, 'Управление языками', '', 's:0:"";', 0),
	('Admin_Main', 1, 'Админ панель', NULL, 's:0:"";', 0),
	('Admin_News', 1, 'Управление новостями', '', 's:0:"";', 0),
	('Admin_RBAC', 1, 'Управление контролем доступа', NULL, 's:0:"";', 0),
	('Admin_Settings', 1, 'Управление настройками', '', 's:0:"";', 0),
	('Admin_Users', 1, 'Управление пользователями', '', 's:0:"";', 0),
	('ArticleAdmin_Create', 0, 'Добавление статьи (админка)', NULL, 'N;', 0),
	('ArticleAdmin_Delete', 0, 'Удаление статьи (админка)', NULL, 'N;', 0),
	('ArticleAdmin_Manage', 0, 'Управление статьями (админка)', NULL, 'N;', 0),
	('ArticleAdmin_Update', 0, 'Редактирование статьи (админка)', NULL, 'N;', 0),
	('ArticleAdmin_View', 0, 'Просмотр статьи (админка)', NULL, 'N;', 0),
	('ArticleSectionAdmin_Create', 0, 'Добавление раздела статьи(админка)', '', 's:2:"N;";', 0),
	('ArticleSectionAdmin_Delete', 0, 'Удаление раздела статьи(админка)', '', 's:2:"N;";', 0),
	('ArticleSectionAdmin_GetSectionInSidebar', 0, 'Получить раздел статей, который в сайдбаре (админка)', '', 's:2:"N;";', 0),
	('ArticleSectionAdmin_Manage', 0, 'Управление разделами статей(админка)', '', 's:2:"N;";', 0),
	('ArticleSectionAdmin_Update', 0, 'Редактирование раздела статьи(админка)', '', 's:2:"N;";', 0),
	('ArticleSectionAdmin_View', 0, 'Просмотр раздела статьи(админка)', '', 's:2:"N;";', 0),
	('ArticleSection_GetChilds', 0, 'Получить подразделы статей', '', 's:2:"N;";', 1),
	('Articles_Admin', 1, 'Управление статьями', '', 's:0:"";', 0),
	('Article_Index', 0, 'Просмотр списка статей', NULL, 'N;', 1),
	('Article_Search', 0, 'Поиск статей', NULL, 'N;', 1),
	('Article_SectionArticles', 0, 'Просмотр статей раздела', NULL, 'N;', 1),
	('Article_View', 0, 'Просмотр статьи', NULL, 'N;', 1),
	('BannerAdmin_Create', 0, 'Создание баннера (админка)', NULL, 'N;', 0),
	('BannerAdmin_Delete', 0, 'Удаление баннера (админка)', NULL, 'N;', 0),
	('BannerAdmin_Manage', 0, 'Управление баннерами (админка)', NULL, 'N;', 0),
	('BannerAdmin_MovePosition', 0, 'Изменение приоритета банера (админка)', NULL, 'N;', 0),
	('BannerAdmin_Update', 0, 'Редактирование баннера (админка)', NULL, 'N;', 0),
	('BannerAdmin_View', 0, 'Просмотр баннера (админка)', NULL, 'N;', 0),
	('Cabinet_Index', 0, 'Личный кабинет', NULL, 'N;', 0),
	('CertificateGroupAdmin_Create', 0, 'Создание группы сертификатов (админка)', NULL, 'N;', 0),
	('CertificateGroupAdmin_Delete', 0, 'Удаление группы сертификатов (админка)', NULL, 'N;', 0),
	('CertificateGroupAdmin_Manage', 0, 'Управление группами сертификатов (админка)', NULL, 'N;', 0),
	('CertificateGroupAdmin_Update', 0, 'Редактирование группы сертификатов (админка)', NULL, 'N;', 0),
	('CertificateTypeAdmin_Create', 0, 'Создание типа сертификатов (админка)', NULL, 'N;', 0),
	('CertificateTypeAdmin_Delete', 0, 'Удаление типа сертификатов (админка)', NULL, 'N;', 0),
	('CertificateTypeAdmin_Manage', 0, 'Управление типами сертификатов (админка)', NULL, 'N;', 0),
	('CertificateTypeAdmin_Update', 0, 'Редактирование типа сертификатов (админка)', NULL, 'N;', 0),
	('CityAdmin_Create', 0, 'Добавление города (админка)', NULL, 'N;', 0),
	('CityAdmin_Delete', 0, 'Удаление города (админка)', NULL, 'N;', 0),
	('CityAdmin_Manage', 0, 'Управление городами (админка)', NULL, 'N;', 0),
	('CityAdmin_Update', 0, 'Редактирование города (админка)', NULL, 'N;', 0),
	('City_AutoComplete', 0, 'Автодополнение городов', NULL, 'N;', 1),
	('CommentAdmin_Delete', 0, 'Удаление комментария (ПУ)', NULL, 'N;', 0),
	('CommentAdmin_Manage', 0, 'Управление комментариями (ПУ)', NULL, 'N;', 0),
	('CommentAdmin_Update', 0, 'Редактирование комментария (ПУ)', NULL, 'N;', 0),
	('CommentAdmin_View', 0, 'Просмотр комментария (ПУ)', NULL, 'N;', 0),
	('Comment_Add', 0, 'Добавление комментария', NULL, 'N;', 1),
	('CountryAdmin_Create', 0, 'Добавление страны (админка)', NULL, 'N;', 0),
	('CountryAdmin_Delete', 0, 'Удаление страны (админка)', NULL, 'N;', 0),
	('CountryAdmin_Manage', 0, 'Управление странами (админка)', NULL, 'N;', 0),
	('CountryAdmin_Update', 0, 'Редактирование страны (админка)', NULL, 'N;', 0),
	('Country_AutoComplete', 0, 'Автодополнение стран', NULL, 'N;', 1),
	('DocumentAdmin_Create', 0, 'Добавление документа (админка)', NULL, 'N;', 0),
	('DocumentAdmin_Delete', 0, 'Удаление документа (админка)', NULL, 'N;', 0),
	('DocumentAdmin_Manage', 0, 'Управление документами (админка)', NULL, 'N;', 0),
	('DocumentAdmin_Update', 0, 'Редактирование документа (админка)', NULL, 'N;', 0),
	('DocumentAdmin_View', 0, 'Просмотр документа (админка)', NULL, 'N;', 0),
	('DocumentFileAdmin_Create', 0, 'Добавление файла документа (админка)', NULL, 'N;', 0),
	('DocumentFileAdmin_Delete', 0, 'Удаление файла документа (админка)', NULL, 'N;', 0),
	('DocumentFileAdmin_Manage', 0, 'Управление файлами документов (админка)', NULL, 'N;', 0),
	('DocumentFileAdmin_Update', 0, 'Редактирование файла документа (админка)', NULL, 'N;', 0),
	('Document_Index', 0, 'Просмотр списка документов', NULL, 'N;', 1),
	('Document_View', 0, 'Просмотр документа', NULL, 'N;', 1),
	('FaqAdmin_Create', 0, 'Добавление вопроса (админка)', NULL, 'N;', 0),
	('FaqAdmin_Delete', 0, 'Удаление вопроса (админка)', NULL, 'N;', 0),
	('FaqAdmin_Manage', 0, 'Управление вопросами (админка)', NULL, 'N;', 0),
	('FaqAdmin_SendNotification', 0, 'Отправить уведомление (админка)', NULL, 'N;', 0),
	('FaqAdmin_Update', 0, 'Редактирование вопроса (админка)', NULL, 'N;', 0),
	('FaqAdmin_View', 0, 'Просмотр вопроса (админка)', NULL, 'N;', 0),
	('FaqSectionAdmin_Create', 0, 'Добавление раздела вопросов (админка)', NULL, 'N;', 0),
	('FaqSectionAdmin_Delete', 0, 'Удаление раздела вопросов (админка)', NULL, 'N;', 0),
	('FaqSectionAdmin_Manage', 0, 'Управление разделами вопросов (админка)', NULL, 'N;', 0),
	('FaqSectionAdmin_Update', 0, 'Редактирование раздела вопросов (админка)', NULL, 'N;', 0),
	('FaqSectionAdmin_View', 0, 'Просмотр раздела вопросов (админка)', NULL, 'N;', 0),
	('FaqSection_Index', 0, 'Просмотр разделов вопросов', NULL, 'N;', 1),
	('Faq_Create', 0, 'Добавление вопроса', NULL, 'N;', 1),
	('Faq_Index', 0, 'Просмотр списка вопросов', NULL, 'N;', 1),
	('FeedbackAdmin_Delete', 0, 'Удаление сообщений (админка)', NULL, 'N;', 0),
	('FeedbackAdmin_Manage', 0, 'Управление сообщениями (админка)', NULL, 'N;', 0),
	('FeedbackAdmin_View', 0, 'Просмотр сообщений (админка)', NULL, 'N;', 0),
	('Feedback_Create', 0, 'Добавление сообщения', NULL, 'N;', 1),
	('FileManagerAdmin_Delete', 0, 'Файловый менеджер:Удаление файла (админка)', NULL, 'N;', 0),
	('FileManagerAdmin_ExistFiles', 0, 'Файловый менеджер:Загрузка существующих файлов (админка)', NULL, 'N;', 0),
	('FileManagerAdmin_Manage', 0, 'Файловый менеджер:Управление файлами (админка)', NULL, 'N;', 0),
	('FileManagerAdmin_SavePriority', 0, 'Файловый менеджер:Сортировка (админка)', NULL, 'N;', 0),
	('FileManagerAdmin_UpdateAttr', 0, 'Файловый менеджер:Редактирование файла (админка)', NULL, 'N;', 0),
	('FileManagerAdmin_Upload', 0, 'Файловый менеджер:Загрузка файлов (админка)', NULL, 'N;', 0),
	('FileManager_DownloadFile', 0, 'Файловый менеджер:Скачать файл', NULL, 'N;', 1),
	('GlossaryAdmin_Create', 0, 'Добавление статьи (админка)', NULL, 'N;', 0),
	('GlossaryAdmin_Delete', 0, 'Удаление статьи (админка)', NULL, 'N;', 0),
	('GlossaryAdmin_Manage', 0, 'Управление статьи (админка)', NULL, 'N;', 0),
	('GlossaryAdmin_Update', 0, 'Редактирование статьи (админка)', NULL, 'N;', 0),
	('GlossaryAdmin_View', 0, 'Просмотр статьи (админка)', NULL, 'N;', 0),
	('Glossary_Index', 0, 'Список определений', NULL, 'N;', 0),
	('Glossary_View', 0, 'Просмотр определения', NULL, 'N;', 0),
	('guest', 2, 'Гость', '', 's:0:"";', 0),
	('LanguageAdmin_Create', 0, 'Добавление языка (админка)', NULL, 'N;', 0),
	('LanguageAdmin_Delete', 0, 'Удаление языка (админка)', NULL, 'N;', 0),
	('LanguageAdmin_Manage', 0, 'Управление языками (админка)', NULL, 'N;', 0),
	('LanguageAdmin_Update', 0, 'Редактирование языка (админка)', NULL, 'N;', 0),
	('LogAdmin_Manage', 0, 'Управление логами (админка)', NULL, 'N;', 0),
	('LogAdmin_View', 0, 'Просмотр логово (админка)', NULL, 'N;', 0),
	('MailerFieldAdmin_Create', 0, 'Добавление генерируемого поля (админка)', NULL, 'N;', 0),
	('MailerFieldAdmin_Delete', 0, 'Удаление генерируемого поля (админка)', NULL, 'N;', 0),
	('MailerFieldAdmin_Manage', 0, 'Управление генерируемыми полями (админка)', NULL, 'N;', 0),
	('MailerFieldAdmin_Update', 0, 'Редактирование генерируемого поля (админка)', NULL, 'N;', 0),
	('MailerLetterAdmin_Create', 0, 'Cоздание рассылки (админка)', NULL, 'N;', 0),
	('MailerLetterAdmin_Delete', 0, 'Удаление рассылки (админка)', NULL, 'N;', 0),
	('MailerLetterAdmin_Manage', 0, 'Отчеты о рассылках (админка)', NULL, 'N;', 0),
	('MailerLetterAdmin_Update', 0, 'Редактирование рассылки (админка)', NULL, 'N;', 0),
	('MailerLetterAdmin_View', 0, 'Отчет об отправке (админка)', NULL, 'N;', 0),
	('MailerRecipientAdmin_Manage', 0, 'Статистика получателей рассылки (админка)', NULL, 'N;', 0),
	('MailerTemplateAdmin_Create', 0, 'Добавление шаблона рассылки (админка)', NULL, 'N;', 0),
	('MailerTemplateAdmin_Delete', 0, 'Удаление шаблона рассылки (админка)', NULL, 'N;', 0),
	('MailerTemplateAdmin_Manage', 0, 'Управление шаблонами рассылки (админка)', NULL, 'N;', 0),
	('MailerTemplateAdmin_Update', 0, 'Редактирование шаблона рассылки (админка)', NULL, 'N;', 0),
	('MailerTemplateAdmin_View', 0, 'Просмотр шаблона рассылки (админка)', NULL, 'N;', 0),
	('Mailer_ConfirmReceipt', 0, 'Подтверждение получения письма', NULL, 'N;', 1),
	('Mailer_SendMails', 0, 'Отправить письма', NULL, 'N;', 1),
	('MainAdmin_AdminLinkProcess', 0, 'Переход по ссылке в админ панель (админка)', NULL, 'N;', 0),
	('MainAdmin_ChangeOrder', 0, 'Сортировка (админка)', NULL, 'N;', 0),
	('MainAdmin_Index', 0, 'Просмотр главной страницы (админка)', NULL, 'N;', 0),
	('MainAdmin_Modules', 0, 'Просмотр списка модулей (админка)', NULL, 'N;', 0),
	('MainAdmin_SessionLanguage', 0, 'Установка языка (админка)', NULL, 'N;', 0),
	('MainAdmin_SessionPerPage', 0, 'Установки кол-ва элементов на странице (админка)', NULL, 'N;', 0),
	('Main_Error', 0, 'Ошибка на странице', NULL, 'N;', 1),
	('Main_Search', 0, 'Поиск по сайту', NULL, 'N;', 1),
	('MenuAdmin_Create', 0, 'Добавление меню (админка)', NULL, 'N;', 0),
	('MenuAdmin_Delete', 0, 'Удаление меню (админка)', NULL, 'N;', 0),
	('MenuAdmin_Manage', 0, 'Управление меню (админка)', NULL, 'N;', 0),
	('MenuAdmin_Update', 0, 'Редактирование меню (админка)', NULL, 'N;', 0),
	('MenuLinkAdmin_AjaxFillTree', 0, 'Загрузка дерева ссылок (админка)', NULL, 'N;', 0),
	('MenuLinkAdmin_Create', 0, 'Добавление ссылки меню (админка)', NULL, 'N;', 0),
	('MenuLinkAdmin_Delete', 0, 'Удаление ссылки меню (админка)', NULL, 'N;', 0),
	('MenuLinkAdmin_Index', 0, 'Управление ссылками меню (админка)', NULL, 'N;', 0),
	('MenuLinkAdmin_Update', 0, 'Редактирование ссылки меню (админка)', NULL, 'N;', 0),
	('MenuLinkAdmin_View', 0, 'Просмотр ссылки меню (админка)', NULL, 'N;', 0),
	('MetaTagAdmin_Create', 0, 'Создание мета-тега (админка)', NULL, 'N;', 0),
	('MetaTagAdmin_Delete', 0, 'Удаление мета-тега (админка)', NULL, 'N;', 0),
	('MetaTagAdmin_GetMetaTagData', 0, 'Получение данных тега (админка)', NULL, 'N;', 0),
	('MetaTagAdmin_GetModelObjects', 0, 'Получение объектов модели (админка)', NULL, 'N;', 0),
	('MetaTagAdmin_Manage', 0, 'Управление мета-тегами (админка)', NULL, 'N;', 0),
	('MetaTagAdmin_Update', 0, 'Редактирование мета-тега (админка)', NULL, 'N;', 0),
	('MetaTagAdmin_View', 0, 'Просмотр мета-тега (админка)', NULL, 'N;', 0),
	('moderator', 2, 'Модератор', '', 's:0:"";', 0),
	('NewsAdmin_Create', 0, 'Добавление новости (админка)', NULL, 'N;', 0),
	('NewsAdmin_Delete', 0, 'Удаление новости (админка)', NULL, 'N;', 0),
	('NewsAdmin_Manage', 0, 'Управление новостями (админка)', NULL, 'N;', 0),
	('NewsAdmin_Update', 0, 'Редактирование новости (админка)', NULL, NULL, 0),
	('NewsAdmin_View', 0, 'Просмотр новости (админка)', NULL, 'N;', 0),
	('News_Index', 0, 'Список новостей', NULL, 'N;', 1),
	('News_View', 0, 'Просмотр новости', NULL, 'N;', 1),
	('OperationAdmin_AddAllOperations', 0, 'Добавление всех операций модулей (админка)', NULL, 'N;', 0),
	('OperationAdmin_Create', 0, 'Добавление операции (админка)', NULL, 'N;', 0),
	('OperationAdmin_Delete', 0, 'Удаление операции (админка)', NULL, 'N;', 0),
	('OperationAdmin_GetModuleActions', 0, 'Получение операции модуля, JSON (админка)', NULL, 'N;', 0),
	('OperationAdmin_GetModules', 0, 'Получение модулей, JSON (админка)', NULL, 'N;', 0),
	('OperationAdmin_Manage', 0, 'Управление операциями (админка)', NULL, 'N;', 0),
	('OperationAdmin_Update', 0, 'Редактирование операции (админка)', NULL, 'N;', 0),
	('OperationAdmin_View', 0, 'Просмотр операции (админка)', NULL, 'N;', 0),
	('PageAdmin_Create', 0, 'Добавление страницы (админка)', NULL, 'N;', 0),
	('PageAdmin_Delete', 0, 'Удаление страницы (админка)', NULL, 'N;', 0),
	('PageAdmin_GetJsonData', 0, 'Получение данных страницы (JSON) (админка)', NULL, 'N;', 0),
	('PageAdmin_Manage', 0, 'Управление страницами (админка)', NULL, 'N;', 0),
	('PageAdmin_Update', 0, 'Редактирование страницы (админка)', NULL, 'N;', 0),
	('PageAdmin_View', 0, 'Просмотр страницы (админка)', NULL, 'N;', 0),
	('PageBlockAdmin_Create', 0, 'Добавление контентного блока (админка)', NULL, 'N;', 0),
	('PageBlockAdmin_Delete', 0, 'Удаление контентного блока (админка)', NULL, 'N;', 0),
	('PageBlockAdmin_Manage', 0, 'Управление контентными блоками (админка)', NULL, 'N;', 0),
	('PageBlockAdmin_Update', 0, 'Редактирование контентного блока (админка)', NULL, 'N;', 0),
	('PageBlockAdmin_View', 0, 'Просмотр контентного блока (админка)', NULL, 'N;', 0),
	('Page_Main', 0, 'Главная страница', NULL, 'N;', 1),
	('Page_View', 0, 'Просмотр страницы', NULL, 'N;', 1),
	('PortfolioAdmin_Create', 0, 'Добавление работы (ПУ)', NULL, 'N;', 0),
	('PortfolioAdmin_Delete', 0, 'Удаление работы (ПУ)', NULL, 'N;', 0),
	('PortfolioAdmin_Manage', 0, 'Управление работами (ПУ)', NULL, 'N;', 0),
	('PortfolioAdmin_Update', 0, 'Редактирование работы (ПУ)', NULL, 'N;', 0),
	('PortfolioAdmin_View', 0, 'Просмотр работы (ПУ)', NULL, 'N;', 0),
	('Portfolio_Index', 0, 'Список работ', NULL, 'N;', 1),
	('Portfolio_View', 0, 'Просмотр работы', NULL, 'N;', 1),
	('PostAdmin_Create', 0, 'Добавление записи (ПУ)', NULL, 'N;', 0),
	('PostAdmin_Delete', 0, 'Удаление записи (ПУ)', NULL, 'N;', 0),
	('PostAdmin_Manage', 0, 'Управление записями (ПУ)', NULL, 'N;', 0),
	('PostAdmin_Update', 0, 'Редактирование записи (ПУ)', NULL, 'N;', 0),
	('PostAdmin_View', 0, 'Просмотр записи (ПУ)', NULL, 'N;', 0),
	('Post_Index', 0, 'Блог', NULL, 'N;', 1),
	('Post_View', 0, 'Просмотр записи', NULL, 'N;', 1),
	('ReviewAdmin_Create', 0, 'Добавление отзыва (ПУ)', NULL, 'N;', 0),
	('ReviewAdmin_Delete', 0, 'Удаление отзыва (ПУ)', NULL, 'N;', 0),
	('ReviewAdmin_Manage', 0, 'Управление отзывами (ПУ)', NULL, 'N;', 0),
	('ReviewAdmin_Update', 0, 'Редактирование отзыва (ПУ)', NULL, 'N;', 0),
	('ReviewAdmin_View', 0, 'Просмотр отзыва (ПУ)', NULL, 'N;', 0),
	('Review_Index', 0, 'Отзывы', NULL, 'N;', 1),
	('Review_View', 0, 'Просмотр отзыва', NULL, 'N;', 1),
	('RoleAdmin_Assignment', 0, 'Назначение ролей (админка)', NULL, 'N;', 0),
	('RoleAdmin_Create', 0, 'Добавление роли (админка)', NULL, 'N;', 0),
	('RoleAdmin_Delete', 0, 'Удаление роли (админка)', NULL, 'N;', 0),
	('RoleAdmin_Manage', 0, 'Управление ролями (админка)', NULL, 'N;', 0),
	('RoleAdmin_Update', 0, 'Редактирование роли (админка)', NULL, 'N;', 0),
	('RoleAdmin_View', 0, 'Просмотр роли (админка)', NULL, 'N;', 0),
	('ServiceAdmin_Create', 0, 'Добавление услуги (ПУ)', NULL, 'N;', 0),
	('ServiceAdmin_Delete', 0, 'Удаление услуги (ПУ)', NULL, 'N;', 0),
	('ServiceAdmin_Manage', 0, 'Управление услугами (ПУ)', NULL, 'N;', 0),
	('ServiceAdmin_Update', 0, 'Редактирование услуги (ПУ)', NULL, 'N;', 0),
	('ServiceAdmin_View', 0, 'Просмотр услуги (ПУ)', NULL, 'N;', 0),
	('ServicesService_Index', 0, 'Список услуг', NULL, 'N;', 1),
	('Service_Index', 0, 'Список услуг', NULL, 'N;', 1),
	('Service_View', 0, 'Просмотр услуги', NULL, 'N;', 1),
	('SettingAdmin_Manage', 0, 'Управление настройками (админка)', NULL, 'N;', 0),
	('SettingAdmin_Update', 0, 'Редактирование настройки (админка)', NULL, 'N;', 0),
	('SettingAdmin_View', 0, 'Просмотр настройки (админка)', NULL, 'N;', 0),
	('SiteActionAdmin_Index', 0, 'Просмотр действий сайта (админка)', NULL, 'N;', 0),
	('SiteActionAdmin_Manage', 0, 'Просмотр действий сайта (админка)', NULL, 'N;', 0),
	('TaskAdmin_Allow', 0, 'Разрешение задачи для роли (админка)', NULL, 'N;', 0),
	('TaskAdmin_Create', 0, 'Добавление задачи (админка)', NULL, 'N;', 0),
	('TaskAdmin_Delete', 0, 'Удаление задачи (админка)', NULL, 'N;', 0),
	('TaskAdmin_Deny', 0, 'Запрещение задачи для роли (админка)', NULL, 'N;', 0),
	('TaskAdmin_Manage', 0, 'Управление задачами (админка)', NULL, 'N;', 0),
	('TaskAdmin_RolesTasks', 0, 'Назначение задач для роли (админка)', NULL, 'N;', 0),
	('TaskAdmin_Update', 0, 'Редактирование задачи (админка)', NULL, 'N;', 0),
	('TaskAdmin_View', 0, 'Просмотр задачи (админка)', NULL, 'N;', 0),
	('user', 2, 'Пользователь', '', 's:7:"s:0:"";";', 0),
	('UserAdmin_Create', 0, 'Добавление пользователя (админка)', NULL, 'N;', 0),
	('UserAdmin_Delete', 0, 'Удаление пользователя (админка)', NULL, 'N;', 0),
	('UserAdmin_Login', 0, 'Авторизация (админка)', NULL, 's:9:"s:2:"N;";";', 1),
	('UserAdmin_Manage', 0, 'Управление пользователями (админка)', NULL, 'N;', 0),
	('UserAdmin_Update', 0, 'Редактирование пользователя (админка)', NULL, 'N;', 0),
	('UserAdmin_View', 0, 'Просмотр пользователя (админка)', NULL, 'N;', 0),
	('Users_Account', 1, 'Авторизация, регистрация, смена пароля', NULL, 's:0:"";', 1),
	('User_ActivateAccount', 0, 'Активация аккаунта', NULL, 'N;', 0),
	('User_ActivateAccountRequest', 0, 'Пользователи:Запрос на активацию аккаунта', NULL, 'N;', 0),
	('User_ChangePassword', 0, 'Смена пароля', NULL, 'N;', 0),
	('User_ChangePasswordRequest', 0, 'Запрос на смену пароля', NULL, 'N;', 1),
	('User_Login', 0, 'Авторизация', NULL, 'N;', 1),
	('User_Logout', 0, 'Выход', NULL, 'N;', 1),
	('User_Registration', 0, 'Регистрация', NULL, 'N;', 1),
	('YmarketBrandAdmin_Manage', 0, 'Бренды (админка)', NULL, 'N;', 0),
	('YmarketCronAdmin_Manage', 0, 'Фоновые задания (админка)', NULL, 'N;', 0),
	('YmarketCronAdmin_Update', 0, 'Редактирование фонового задания (админка)', NULL, 'N;', 0),
	('YmarketIPAdmin_Create', 0, 'Добавление IP адреса яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketIPAdmin_Delete', 0, 'Удаление IP адреса яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketIPAdmin_Manage', 0, 'IP адреса яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketIPAdmin_Update', 0, 'Редактирование IP адреса яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketProductAdmin_Delete', 0, 'Удаление продукта Яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketProductAdmin_Manage', 0, 'Продукты Яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketProductAdmin_View', 0, 'Просмотр продукта Яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketSectionAdmin_Create', 0, 'Создание раздела яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketSectionAdmin_Delete', 0, 'Удаление раздела яндекс маркета? (админка)', NULL, 'N;', 0),
	('YmarketSectionAdmin_Manage', 0, 'Разделы яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketSectionAdmin_Update', 0, 'Редактирование раздела яндекс маркета (админка)', NULL, 'N;', 0),
	('YmarketSectionAdmin_View', 0, 'Просмотр раздела яндекс маркета (админка)', NULL, 'N;', 0),
	('Post_Captcha', 0, 'Капча', NULL, NULL, 1),
	('Comment_Captcha', 0, 'Капча', NULL, NULL, 1),
	('Main_Redirect', 0, 'Редирект', NULL, NULL, 1),
	('Faq_View', 0, 'Просмотр вопроса', NULL, NULL, 1),
	('Page_Index', 0, 'Просмотр всех страниц', NULL, 'N;', 1),
	('FaqAdmin_Notify', 0, 'Почтовое уведомление (ПУ)', NULL, 'N;', 0),
	('NewsAdmin_UpdateOwnNews', 1, 'Редактирование собственной новости', 'return Yii::app()->user->id==$params["news"]->author->id;', NULL, 0);
/*!40000 ALTER TABLE `auth_items` ENABLE KEYS */;


-- Dumping structure for table esro_site.auth_items_childs
CREATE TABLE IF NOT EXISTS `auth_items_childs` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.auth_items_childs: 126 rows
/*!40000 ALTER TABLE `auth_items_childs` DISABLE KEYS */;
REPLACE INTO `auth_items_childs` (`parent`, `child`) VALUES
	('Actions_Admin', 'ActionAdmin_Create'),
	('Actions_Admin', 'ActionAdmin_Delete'),
	('Actions_Admin', 'ActionAdmin_Manage'),
	('Actions_Admin', 'ActionAdmin_Update'),
	('Actions_Admin', 'ActionAdmin_View'),
	('Actions_Admin', 'ActionFileAdmin_Create'),
	('Actions_Admin', 'ActionFileAdmin_Delete'),
	('Actions_Admin', 'ActionFileAdmin_Manage'),
	('Admin_Cities_Countries', 'CityAdmin_Create'),
	('Admin_Cities_Countries', 'CityAdmin_Delete'),
	('Admin_Cities_Countries', 'CityAdmin_Manage'),
	('Admin_Cities_Countries', 'CityAdmin_Update'),
	('Admin_Cities_Countries', 'CountryAdmin_Create'),
	('Admin_Cities_Countries', 'CountryAdmin_Delete'),
	('Admin_Cities_Countries', 'CountryAdmin_Manage'),
	('Admin_Cities_Countries', 'CountryAdmin_Update'),
	('Admin_Content', 'MenuAdmin_Create'),
	('Admin_Content', 'MenuAdmin_Delete'),
	('Admin_Content', 'MenuAdmin_Manage'),
	('Admin_Content', 'MenuAdmin_Update'),
	('Admin_Content', 'MenuLinkAdmin_AjaxFillTree'),
	('Admin_Content', 'MenuLinkAdmin_Create'),
	('Admin_Content', 'MenuLinkAdmin_Delete'),
	('Admin_Content', 'MenuLinkAdmin_Index'),
	('Admin_Content', 'MenuLinkAdmin_Update'),
	('Admin_Content', 'MenuLinkAdmin_View'),
	('Admin_Content', 'PageAdmin_Create'),
	('Admin_Content', 'PageAdmin_Delete'),
	('Admin_Content', 'PageAdmin_Manage'),
	('Admin_Content', 'PageAdmin_Update'),
	('Admin_Content', 'PageAdmin_View'),
	('Admin_Content', 'PageBlockAdmin_Create'),
	('Admin_Content', 'PageBlockAdmin_Delete'),
	('Admin_Content', 'PageBlockAdmin_Manage'),
	('Admin_Content', 'PageBlockAdmin_Update'),
	('Admin_Content', 'PageBlockAdmin_View'),
	('Admin_Documents', 'DocumentAdmin_Create'),
	('Admin_Documents', 'DocumentAdmin_Delete'),
	('Admin_Documents', 'DocumentAdmin_Manage'),
	('Admin_Documents', 'DocumentAdmin_Update'),
	('Admin_Documents', 'DocumentAdmin_View'),
	('Admin_Documents', 'DocumentFileAdmin_Create'),
	('Admin_Documents', 'DocumentFileAdmin_Delete'),
	('Admin_Documents', 'DocumentFileAdmin_Manage'),
	('Admin_Documents', 'DocumentFileAdmin_Update'),
	('Admin_Faq', 'FaqAdmin_Create'),
	('Admin_Faq', 'FaqAdmin_Delete'),
	('Admin_Faq', 'FaqAdmin_Manage'),
	('Admin_Faq', 'FaqAdmin_Update'),
	('Admin_Faq', 'FaqAdmin_View'),
	('Admin_Faq', 'FaqSectionAdmin_Create'),
	('Admin_Faq', 'FaqSectionAdmin_Delete'),
	('Admin_Faq', 'FaqSectionAdmin_Manage'),
	('Admin_Faq', 'FaqSectionAdmin_Update'),
	('Admin_Faq', 'FaqSectionAdmin_View'),
	('Admin_Feedback', 'FeedbackAdmin_Delete'),
	('Admin_Feedback', 'FeedbackAdmin_Manage'),
	('Admin_Feedback', 'FeedbackAdmin_View'),
	('Admin_Languages', 'LanguageAdmin_Create'),
	('Admin_Languages', 'LanguageAdmin_Delete'),
	('Admin_Languages', 'LanguageAdmin_Manage'),
	('Admin_Languages', 'LanguageAdmin_Update'),
	('Admin_Main', 'LogAdmin_View'),
	('Admin_Main', 'MainAdmin_ChangeOrder'),
	('Admin_Main', 'MainAdmin_Index'),
	('Admin_Main', 'MainAdmin_Modules'),
	('Admin_Main', 'MainAdmin_SessionLanguage'),
	('Admin_Main', 'MainAdmin_SessionPerPage'),
	('Admin_Main', 'OperationAdmin_GetModules'),
	('Admin_Main', 'SiteActionAdmin_Index'),
	('Admin_News', 'NewsAdmin_Create'),
	('Admin_News', 'NewsAdmin_Delete'),
	('Admin_News', 'NewsAdmin_Manage'),
	('Admin_News', 'NewsAdmin_Update'),
	('Admin_News', 'NewsAdmin_View'),
	('Admin_RBAC', 'OperationAdmin_AddAllOperations'),
	('Admin_RBAC', 'OperationAdmin_Create'),
	('Admin_RBAC', 'OperationAdmin_Delete'),
	('Admin_RBAC', 'OperationAdmin_GetModuleActions'),
	('Admin_RBAC', 'OperationAdmin_Manage'),
	('Admin_RBAC', 'OperationAdmin_Update'),
	('Admin_RBAC', 'OperationAdmin_View'),
	('Admin_RBAC', 'RoleAdmin_Create'),
	('Admin_RBAC', 'RoleAdmin_Delete'),
	('Admin_RBAC', 'RoleAdmin_Manage'),
	('Admin_RBAC', 'RoleAdmin_Update'),
	('Admin_RBAC', 'RoleAdmin_View'),
	('Admin_RBAC', 'TaskAdmin_Create'),
	('Admin_RBAC', 'TaskAdmin_Delete'),
	('Admin_RBAC', 'TaskAdmin_Manage'),
	('Admin_RBAC', 'TaskAdmin_Update'),
	('Admin_RBAC', 'TaskAdmin_View'),
	('Admin_Settings', 'SettingAdmin_Manage'),
	('Admin_Settings', 'SettingAdmin_Update'),
	('Admin_Settings', 'SettingAdmin_View'),
	('Admin_Users', 'UserAdmin_Create'),
	('Admin_Users', 'UserAdmin_Delete'),
	('Admin_Users', 'UserAdmin_Manage'),
	('Admin_Users', 'UserAdmin_Update'),
	('Admin_Users', 'UserAdmin_View'),
	('Articles_Admin', 'ArticleAdmin_Create'),
	('Articles_Admin', 'ArticleAdmin_Delete'),
	('Articles_Admin', 'ArticleAdmin_Manage'),
	('Articles_Admin', 'ArticleAdmin_Update'),
	('Articles_Admin', 'ArticleAdmin_View'),
	('Articles_Admin', 'ArticleSectionAdmin_Create'),
	('Articles_Admin', 'ArticleSectionAdmin_Delete'),
	('Articles_Admin', 'ArticleSectionAdmin_GetSectionInSidebar'),
	('Articles_Admin', 'ArticleSectionAdmin_Manage'),
	('Articles_Admin', 'ArticleSectionAdmin_Update'),
	('Articles_Admin', 'ArticleSectionAdmin_View'),
	('moderator', 'Actions_Admin'),
	('moderator', 'Admin_Content'),
	('moderator', 'Admin_Documents'),
	('moderator', 'Admin_Faq'),
	('moderator', 'Admin_Feedback'),
	('moderator', 'Admin_Main'),
	('moderator', 'Admin_News'),
	('moderator', 'Articles_Admin'),
	('moderator', 'NewsAdmin_UpdateOwnNews'),
	('Users_Account', 'UserAdmin_Login'),
	('Users_Account', 'User_ActivateAccount'),
	('Users_Account', 'User_ChangePassword'),
	('Users_Account', 'User_ChangePasswordRequest'),
	('Users_Account', 'User_Login'),
	('Users_Account', 'User_Registration');
/*!40000 ALTER TABLE `auth_items_childs` ENABLE KEYS */;


-- Dumping structure for table esro_site.auth_objects
CREATE TABLE IF NOT EXISTS `auth_objects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) unsigned NOT NULL COMMENT 'Объект',
  `model_id` varchar(50) NOT NULL COMMENT 'Модель',
  `role` varchar(64) NOT NULL COMMENT 'Роль',
  PRIMARY KEY (`id`),
  KEY `role` (`role`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.auth_objects: 0 rows
/*!40000 ALTER TABLE `auth_objects` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_objects` ENABLE KEYS */;


-- Dumping structure for table esro_site.banners
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL COMMENT 'Раздел сайта',
  `name` varchar(50) NOT NULL COMMENT 'Название',
  `image` varchar(37) NOT NULL COMMENT 'Изображение',
  `url` varchar(500) DEFAULT NULL COMMENT 'URL-адрес',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Активен',
  `page_position` varchar(255) NOT NULL COMMENT 'Позиция',
  `priority` int(11) NOT NULL DEFAULT '0' COMMENT 'Приоритет',
  `date_start` date DEFAULT NULL COMMENT 'Дата начала показа',
  `date_end` date DEFAULT NULL COMMENT 'Дата окончания показа',
  `order` int(11) NOT NULL COMMENT 'Сортировка',
  `position` varchar(255) NOT NULL COMMENT 'Позиция',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.banners: 3 rows
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
REPLACE INTO `banners` (`id`, `page_id`, `name`, `image`, `url`, `is_active`, `page_position`, `priority`, `date_start`, `date_end`, `order`, `position`) VALUES
	(45, NULL, 'asdfgvasdf', '8598f68320c15d272d8b8b1a6ae9ae5a.jpg', '#', 1, '', 0, NULL, NULL, 0, 'right2'),
	(43, NULL, 'йцукйцук', 'd23e2147e4ff5b090b58e2c227df4d0e.jpg', '#', 1, '', 0, NULL, NULL, 0, 'top'),
	(44, 87, 'sdfgsadf', '843e3c7966013b2847725da4bb066472.jpg', NULL, 1, '', 0, NULL, NULL, 0, 'right1');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;


-- Dumping structure for table esro_site.banners_pages
CREATE TABLE IF NOT EXISTS `banners_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL COMMENT 'Роут',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1203 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.banners_pages: 75 rows
/*!40000 ALTER TABLE `banners_pages` DISABLE KEYS */;
REPLACE INTO `banners_pages` (`id`, `banner_id`, `route`) VALUES
	(1129, 44, 'Content/Page/View'),
	(1130, 44, 'Content/Page/Main'),
	(1131, 44, 'Faq/Faq/View'),
	(1132, 44, 'Faq/Faq/Index'),
	(1133, 44, 'Faq/Faq/Create'),
	(1134, 44, 'Faq/FaqSection/Index'),
	(1135, 44, 'Mailer/Mailer/ConfirmReceipt'),
	(1136, 44, 'Mailer/Mailer/SendMails'),
	(1137, 44, 'Main/Feedback/Create'),
	(1138, 44, 'Main/Main/Search'),
	(1139, 44, 'Main/Main/Redirect'),
	(1140, 44, 'Main/Main/Error'),
	(1141, 44, 'News/News/View'),
	(1142, 44, 'News/News/Index'),
	(1143, 44, 'Reviews/Review/Index'),
	(1144, 44, 'Reviews/Review/View'),
	(1145, 44, 'Users/Cabinet/Index'),
	(1146, 44, 'Users/User/Login'),
	(1147, 44, 'Users/User/Logout'),
	(1148, 44, 'Users/User/Registration'),
	(1149, 44, 'Users/User/ActivateAccount'),
	(1150, 44, 'Users/User/ActivateAccountRequest'),
	(1151, 44, 'Users/User/ChangePasswordRequest'),
	(1152, 44, 'Users/User/ChangePassword'),
	(1178, 45, 'Content/Page/Index'),
	(1179, 45, 'Content/Page/View'),
	(1180, 45, 'Content/Page/Main'),
	(1181, 45, 'Faq/Faq/View'),
	(1182, 45, 'Faq/Faq/Index'),
	(1183, 45, 'Faq/Faq/Create'),
	(1184, 45, 'Faq/FaqSection/Index'),
	(1185, 45, 'Mailer/Mailer/ConfirmReceipt'),
	(1186, 45, 'Mailer/Mailer/SendMails'),
	(1187, 45, 'Main/Feedback/Create'),
	(1188, 45, 'Main/Main/Search'),
	(1189, 45, 'Main/Main/Redirect'),
	(1190, 45, 'Main/Main/Error'),
	(1191, 45, 'News/News/View'),
	(1192, 45, 'News/News/Index'),
	(1193, 45, 'Reviews/Review/Index'),
	(1194, 45, 'Reviews/Review/View'),
	(1195, 45, 'Users/Cabinet/Index'),
	(1196, 45, 'Users/User/Login'),
	(1197, 45, 'Users/User/Logout'),
	(1198, 45, 'Users/User/Registration'),
	(1199, 45, 'Users/User/ActivateAccount'),
	(1200, 45, 'Users/User/ActivateAccountRequest'),
	(1201, 45, 'Users/User/ChangePasswordRequest'),
	(1202, 45, 'Users/User/ChangePassword'),
	(1128, 44, 'Content/Page/Index'),
	(1127, 43, 'Users/User/ChangePassword'),
	(1126, 43, 'Users/User/ChangePasswordRequest'),
	(1125, 43, 'Users/User/ActivateAccountRequest'),
	(1124, 43, 'Users/User/ActivateAccount'),
	(1123, 43, 'Users/User/Registration'),
	(1122, 43, 'Users/User/Logout'),
	(1121, 43, 'Users/User/Login'),
	(1106, 43, 'Faq/Faq/View'),
	(1107, 43, 'Faq/Faq/Index'),
	(1108, 43, 'Faq/Faq/Create'),
	(1109, 43, 'Faq/FaqSection/Index'),
	(1110, 43, 'Mailer/Mailer/ConfirmReceipt'),
	(1111, 43, 'Mailer/Mailer/SendMails'),
	(1112, 43, 'Main/Feedback/Create'),
	(1113, 43, 'Main/Main/Search'),
	(1114, 43, 'Main/Main/Redirect'),
	(1115, 43, 'Main/Main/Error'),
	(1116, 43, 'News/News/View'),
	(1117, 43, 'News/News/Index'),
	(1118, 43, 'Reviews/Review/Index'),
	(1119, 43, 'Reviews/Review/View'),
	(1120, 43, 'Users/Cabinet/Index'),
	(1103, 43, 'Content/Page/Index'),
	(1104, 43, 'Content/Page/View'),
	(1105, 43, 'Content/Page/Main');
/*!40000 ALTER TABLE `banners_pages` ENABLE KEYS */;


-- Dumping structure for table esro_site.banners_roles
CREATE TABLE IF NOT EXISTS `banners_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) unsigned NOT NULL,
  `role` varchar(64) NOT NULL COMMENT 'Роль',
  PRIMARY KEY (`id`),
  KEY `banner_id` (`banner_id`),
  KEY `role` (`role`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.banners_roles: 12 rows
/*!40000 ALTER TABLE `banners_roles` DISABLE KEYS */;
REPLACE INTO `banners_roles` (`id`, `banner_id`, `role`) VALUES
	(52, 1, 'user'),
	(51, 1, 'moderator'),
	(50, 1, 'guest'),
	(49, 1, 'admin'),
	(68, 3, 'user'),
	(67, 3, 'moderator'),
	(66, 3, 'guest'),
	(65, 3, 'admin'),
	(80, 4, 'user'),
	(79, 4, 'moderator'),
	(78, 4, 'guest'),
	(77, 4, 'admin');
/*!40000 ALTER TABLE `banners_roles` ENABLE KEYS */;


-- Dumping structure for table esro_site.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентефикатор',
  `name` varchar(255) NOT NULL COMMENT 'Имя',
  `email` varchar(255) NOT NULL COMMENT 'E-mail',
  `date_created` datetime NOT NULL COMMENT 'Дата публикации',
  `date_moderated` datetime NOT NULL COMMENT 'Дата модерации',
  `is_published` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Прошел модерацию?',
  `text` text NOT NULL,
  `model_name` varchar(255) NOT NULL COMMENT 'Название модели',
  `object_id` int(11) NOT NULL COMMENT 'Идентификатор модели',
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `is_published` (`is_published`),
  KEY `model_name` (`model_name`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.comments: 1 rows
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;


-- Dumping structure for table esro_site.faq
CREATE TABLE IF NOT EXISTS `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL COMMENT 'Имя',
  `last_name` varchar(40) DEFAULT NULL COMMENT 'Фамилия',
  `phone` varchar(50) DEFAULT NULL COMMENT 'Телефон',
  `email` varchar(80) NOT NULL COMMENT 'Email',
  `question` longtext NOT NULL COMMENT 'Вопрос',
  `answer` longtext COMMENT 'Ответ',
  `answer_position` varchar(30) NOT NULL COMMENT 'Должность',
  `is_published` int(1) NOT NULL DEFAULT '0' COMMENT 'Опубликовано',
  `answer_author` int(11) DEFAULT NULL COMMENT 'Автор ответа',
  `date_question` timestamp NULL DEFAULT NULL COMMENT 'Дата вопроса',
  `date_answer` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата ответа',
  `date_notify` datetime DEFAULT NULL COMMENT 'Дата отправки уведомления',
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Добалено',
  PRIMARY KEY (`id`),
  KEY `date` (`date_create`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.faq: 18 rows
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;


-- Dumping structure for table esro_site.faq_sections
CREATE TABLE IF NOT EXISTS `faq_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` char(2) DEFAULT 'ru' COMMENT 'Язык',
  `name` varchar(200) NOT NULL COMMENT 'Название',
  `is_published` int(1) NOT NULL DEFAULT '0' COMMENT 'Опубликован',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `lang` (`lang`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.faq_sections: 2 rows
/*!40000 ALTER TABLE `faq_sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq_sections` ENABLE KEYS */;


-- Dumping structure for table esro_site.feedback
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL COMMENT 'Имя',
  `last_name` varchar(40) DEFAULT NULL COMMENT 'Фамилия',
  `patronymic` varchar(40) DEFAULT NULL COMMENT 'Отчество',
  `company` varchar(80) DEFAULT NULL COMMENT 'Компания',
  `position` varchar(40) DEFAULT NULL COMMENT 'Должность',
  `phone` varchar(50) DEFAULT NULL COMMENT 'Телефон',
  `email` varchar(80) NOT NULL COMMENT 'Email',
  `comment` varchar(1000) NOT NULL COMMENT 'Текст',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Создано',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.feedback: 103 rows
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;


-- Dumping structure for table esro_site.file_manager
CREATE TABLE IF NOT EXISTS `file_manager` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` varchar(100) DEFAULT NULL COMMENT 'ID объекта',
  `model_id` varchar(100) DEFAULT NULL COMMENT 'Модель',
  `name` varchar(100) NOT NULL COMMENT 'Файл',
  `path` varchar(100) NOT NULL COMMENT 'Путь до файла',
  `tag` varchar(100) DEFAULT NULL COMMENT 'Тег',
  `title` text COMMENT 'Название',
  `descr` text COMMENT 'Описание',
  `order` int(11) unsigned NOT NULL COMMENT 'Порядок',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=456 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.file_manager: 208 rows
/*!40000 ALTER TABLE `file_manager` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_manager` ENABLE KEYS */;


-- Dumping structure for table esro_site.languages
CREATE TABLE IF NOT EXISTS `languages` (
  `id` char(2) NOT NULL COMMENT 'ID',
  `name` varchar(15) NOT NULL COMMENT 'Название',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.languages: 2 rows
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
REPLACE INTO `languages` (`id`, `name`) VALUES
	('en', 'english'),
	('ru', 'русский');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;


-- Dumping structure for table esro_site.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(128) DEFAULT NULL COMMENT 'Тип',
  `category` varchar(128) DEFAULT NULL COMMENT 'Категория',
  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время',
  `message` text COMMENT 'Сообщение',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.log: 0 rows
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;


-- Dumping structure for table esro_site.mailer_fields
CREATE TABLE IF NOT EXISTS `mailer_fields` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL COMMENT 'Код ',
  `name` varchar(200) NOT NULL COMMENT 'Наименование',
  `value` varchar(250) NOT NULL COMMENT 'Значение',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `value` (`value`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.mailer_fields: 14 rows
/*!40000 ALTER TABLE `mailer_fields` DISABLE KEYS */;
REPLACE INTO `mailer_fields` (`id`, `code`, `name`, `value`) VALUES
	(1, '{FIRST_NAME}', 'Имя', '$user->first_name'),
	(2, '{LAST_NAME}', 'Фамилия', '$user->last_name'),
	(3, '{PATRONYMIC}', 'Отчество', '$user->patronymic'),
	(5, '{DATE}', 'Текущая дата', 'date(\'d.m.Y\')'),
	(6, '{ROLE}', 'Наименование группы к которой принадлежит пользователь', '$user->role->description'),
	(7, '{APPEAL}', 'Обращение к пользователю', '$user->gender == User::GENDER_MAN ? \'Уважаемый\' : \'Уважаемая\''),
	(9, '{SITE_NAME}', 'Название сайта', '"http://www.esro.ru"'),
	(10, '{ACTIVATE_ACCOUNT_URL}', 'URL ссылки активации аккаунта', '$user->activateAccountUrl();'),
	(11, '{NATIVE_PASSWORD}', 'Не кодированный пароль пользователя', '$user->nativePassword'),
	(12, '{SUPPORT_EMAIL}', 'Email службы поддержки', '"support@esro.ru"'),
	(13, '{USER_EMAIL}', 'Email пользователя', '$user->email'),
	(14, '{QUESTION}', 'Текст вопроса', '$question'),
	(15, '{ADMIN_ANSWER_URL}', 'Ссылка на страницу для редактирования вопроса', '$adminAnswerUrl'),
	(16, '{ANSWER_URL}', 'Ссылка на страницу просмотра ответа', '$answerUrl'),
	(18, '{SRO_NAME}', 'Название СРО', '$sroName'),
	(19, '{SRO_HREF}', 'Ссылка на СРО', '$sroHref'),
	(20, '{COMMENT_TEXT}', 'Тест комментария', '$commentText');
/*!40000 ALTER TABLE `mailer_fields` ENABLE KEYS */;


-- Dumping structure for table esro_site.mailer_letters
CREATE TABLE IF NOT EXISTS `mailer_letters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(11) unsigned DEFAULT NULL COMMENT 'Шаблон',
  `subject` varchar(150) NOT NULL COMMENT 'Тема письма',
  `text` text NOT NULL COMMENT 'Текст письма',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата отправки',
  PRIMARY KEY (`id`),
  KEY `template_id` (`template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.mailer_letters: 1 rows
/*!40000 ALTER TABLE `mailer_letters` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_letters` ENABLE KEYS */;


-- Dumping structure for table esro_site.mailer_recipients
CREATE TABLE IF NOT EXISTS `mailer_recipients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `letter_id` int(11) unsigned NOT NULL COMMENT 'Рассылка',
  `user_id` int(11) unsigned NOT NULL COMMENT 'Пользователь',
  `status` enum('accepted','fail','waiting','sent') DEFAULT 'waiting' COMMENT 'Статус',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата отправки',
  PRIMARY KEY (`id`),
  UNIQUE KEY `letter_id_2` (`letter_id`,`user_id`),
  KEY `letter_id` (`letter_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.mailer_recipients: 2 rows
/*!40000 ALTER TABLE `mailer_recipients` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_recipients` ENABLE KEYS */;


-- Dumping structure for table esro_site.mailer_templates
CREATE TABLE IF NOT EXISTS `mailer_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL COMMENT 'Название',
  `subject` varchar(150) NOT NULL COMMENT 'Тема письма',
  `text` text NOT NULL COMMENT 'Текст письма',
  `is_basic` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Основной',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Создан',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.mailer_templates: 0 rows
/*!40000 ALTER TABLE `mailer_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_templates` ENABLE KEYS */;


-- Dumping structure for table esro_site.mailer_templates_recipients
CREATE TABLE IF NOT EXISTS `mailer_templates_recipients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_id_2` (`template_id`,`user_id`),
  KEY `template_id` (`template_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.mailer_templates_recipients: 0 rows
/*!40000 ALTER TABLE `mailer_templates_recipients` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_templates_recipients` ENABLE KEYS */;


-- Dumping structure for table esro_site.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT 'Название',
  `is_visible` tinyint(1) NOT NULL COMMENT 'Отображать',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.menu: 1 rows
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


-- Dumping structure for table esro_site.menu_links
CREATE TABLE IF NOT EXISTS `menu_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang` char(2) DEFAULT 'ru' COMMENT 'Язык',
  `parent_id` int(11) unsigned DEFAULT NULL COMMENT 'Родитель',
  `page_id` int(11) unsigned DEFAULT NULL COMMENT 'Привязка к странице',
  `menu_id` int(11) unsigned NOT NULL COMMENT 'Меню',
  `title` varchar(50) NOT NULL COMMENT 'Заголовок',
  `url` varchar(200) NOT NULL COMMENT 'Адрес',
  `user_role` varchar(64) DEFAULT NULL COMMENT 'Только для',
  `not_user_role` varchar(64) DEFAULT NULL COMMENT 'Для всех кроме',
  `order` int(11) NOT NULL COMMENT 'Порядок',
  `is_visible` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отображать',
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  KEY `menu_id` (`menu_id`),
  KEY `parent_id` (`parent_id`),
  KEY `user_role` (`user_role`),
  KEY `not_user_role` (`not_user_role`),
  KEY `lang` (`lang`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.menu_links: 11 rows
/*!40000 ALTER TABLE `menu_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_links` ENABLE KEYS */;


-- Dumping structure for table esro_site.meta_tags
CREATE TABLE IF NOT EXISTS `meta_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) unsigned DEFAULT NULL COMMENT 'ID объекта',
  `model_id` varchar(50) NOT NULL COMMENT 'Модель',
  `title` varchar(300) DEFAULT NULL COMMENT 'Заголовок',
  `keywords` varchar(300) DEFAULT NULL COMMENT 'Ключевые слова',
  `description` varchar(300) DEFAULT NULL COMMENT 'Описание',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Создано',
  `date_update` datetime DEFAULT NULL COMMENT 'Отредактирован',
  PRIMARY KEY (`id`),
  UNIQUE KEY `object_id` (`object_id`,`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.meta_tags: 85 rows
/*!40000 ALTER TABLE `meta_tags` DISABLE KEYS */;
REPLACE INTO `meta_tags` (`id`, `object_id`, `model_id`, `title`, `keywords`, `description`, `date_create`, `date_update`) VALUES
	(91, 89, 'Page', 'Commodo nostrum alias pariatur Hic', 'Aliquip reiciendis iusto voluptatem temporibus', 'Cum autem expedita repellendus Ipsa', '2012-09-26 00:49:20', NULL),
	(92, 90, 'Page', 'Dolores rerum perferendis ut consequatur', 'Aut pariatur Ipsum quasi voluptas', 'Deserunt eligendi ad porro laudantium', '2012-09-26 00:54:20', '2012-09-26 00:55:48'),
	(93, 91, 'Page', 'Rerum sint similique corporis dolor', 'Minima voluptatem est sit mollit', 'Qui error quo consequuntur illo', '2012-09-26 01:31:55', NULL);
/*!40000 ALTER TABLE `meta_tags` ENABLE KEYS */;


-- Dumping structure for table esro_site.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `text` text NOT NULL COMMENT 'Текст',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `order` int(11) NOT NULL COMMENT 'Сортировка',
  `is_published` tinyint(1) NOT NULL COMMENT 'Показывать на сайте?',
  `in_main_page` tinyint(1) NOT NULL COMMENT 'Показывать на главной?',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `user_id` int(11) NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0' COMMENT 'Просмотров',
  `source` varchar(255) DEFAULT NULL COMMENT 'Источник',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.news: 11 rows
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
REPLACE INTO `news` (`id`, `title`, `text`, `date`, `order`, `is_published`, `in_main_page`, `image`, `user_id`, `views`, `source`) VALUES
	(26, 'не на главной 4', '<p>Во-первых, проблема с легко царапающимися корпусами становится настоящим массовым явлением &mdash; о ней пишут практически все владельцы новых устройств. Покрытие у iPhone 5 наверняка повредится, если вы положите девайс в карман с ключами, монетами и другими металлическими предметами.</p>\r\n<p>Но самое страшное даже не это, а то, что смартфоны со сколотой краской поставляются прямо с заводов Foxconn. Таким образом, у покупателей есть большая вероятность получения "облезшего" iPhone 5 даже без каких-либо действий с аппаратом.&nbsp;При этом одинаково повреждаются как черные, так и белые аппараты. Просто на белых смартфонов сколы не так заметны.</p>', '2012-09-12 09:19:00', 26, 1, 0, NULL, 1, 6, NULL),
	(41, 'не на главной 5', '<p>Во-вторых, некоторые пользователи отмечают у устройств проблему с установлением Wi-Fi соединения.&nbsp;Новый смартфон, по их данным, некорректно работает в сетях, защищенных по протоколу WPA2 &mdash; он может работать очень медленно или вообще отказывается выходить в интернет. При переключении роутера на WEP-защиту, все, якобы, становится нормально.</p>\r\n<p>Еще одной замеченной проблемой стала некорректная работа тачскрина у iPhone 5. Иногда аппарат выполняет какие-либо действия без касания &mdash; для прокрутки, например, достаточно провести пальцем над экраном, а иногда может вообще не реагировать на нажатия в течение определенного времени. Кроме этого, сообщается о случаях, когда нажимается иконка соседнего приложения (т.е. нажатие фиксируется в другом месте дисплея) и о самопроизвольных действиях &mdash; прокрутке документа без какого-либо вмешательства пользователя и т.п.</p>', '2014-09-25 20:39:26', 29, 1, 0, NULL, 1, 4, NULL),
	(44, 'не на главной 8', '<p>фывафыва</p>', '2012-09-23 19:02:13', 32, 1, 0, NULL, 1, 0, NULL),
	(42, 'не на главной 6', '<p>фыва</p>', '2012-09-23 18:59:00', 30, 1, 0, NULL, 1, 0, NULL),
	(43, 'не на главной 7', '<p>фыва</p>', '2012-09-23 19:01:17', 31, 1, 0, NULL, 1, 0, NULL),
	(176, 'Sint ipsa labore veniam aut', '<p>Lacus ut vut enim habitasse lacus, integer? Ridiculus enim integer sit, turpis urna sagittis? Tincidunt ut risus dolor hac! In, adipiscing sed sed sagittis. Phasellus! Augue porta nascetur lacus platea, pulvinar parturient lectus, quis tincidunt odio et? Pid lorem ut. Placerat penatibus, a pellentesque, non sociis. Ac nec scelerisque placerat risus scelerisque pid sagittis? Arcu enim porttitor magna non aenean! Sit nec mid elementum turpis dictumst, est ultricies, pid, pulvinar, ac phasellus turpis aliquet ut magna enim vel sit quis tristique magna, arcu amet, et nec turpis magna massa sed, elit sagittis ac facilisis elementum pid! In mid auctor tincidunt integer in, vel sit pellentesque. Lectus, enim porta, platea placerat, nisi ut, pellentesque tempor pulvinar ac. Tincidunt magna vel? Ridiculus.</p>\r\n<p>Nec aenean ac! Etiam, lacus vel scelerisque, porta augue, pulvinar sociis sed, ultrices mauris, mattis a ultricies habitasse nunc? Sed elementum cum eros, tincidunt dolor ultricies magnis ridiculus porta in cras ultrices scelerisque habitasse placerat. Enim augue cum augue est phasellus turpis velit tristique etiam augue habitasse. Lacus auctor, sed arcu dolor cursus. Lundium integer! Habitasse, dapibus sit arcu? Sit, lacus! Vut ac, vel augue nunc. Placerat ut, sed quis ac eros pid, phasellus, in, et elit. Tincidunt cum, lectus, facilisis tristique rhoncus placerat in eu et. Lundium in! Aenean dictumst nec, ut! Magna arcu. Nisi, mid montes pellentesque risus scelerisque, porta penatibus, pellentesque adipiscing integer dolor, mus auctor porttitor lundium auctor facilisis nisi pid magna ut ac. Dignissim.</p>', '2012-09-24 21:55:07', 36, 1, 1, 'c2bb5f4e37257ff6f42c2d425e8dec13.jpg', 1, 0, NULL),
	(175, 'Exercitation eum voluptas elit et', '<p>Lacus ut vut enim habitasse lacus, integer? Ridiculus enim integer sit, turpis urna sagittis? Tincidunt ut risus dolor hac! In, adipiscing sed sed sagittis. Phasellus! Augue porta nascetur lacus platea, pulvinar parturient lectus, quis tincidunt odio et? Pid lorem ut. Placerat penatibus, a pellentesque, non sociis. Ac nec scelerisque placerat risus scelerisque pid sagittis? Arcu enim porttitor magna non aenean! Sit nec mid elementum turpis dictumst, est ultricies, pid, pulvinar, ac phasellus turpis aliquet ut magna enim vel sit quis tristique magna, arcu amet, et nec turpis magna massa sed, elit sagittis ac facilisis elementum pid! In mid auctor tincidunt integer in, vel sit pellentesque. Lectus, enim porta, platea placerat, nisi ut, pellentesque tempor pulvinar ac. Tincidunt magna vel? Ridiculus.</p>\r\n<p>Nec aenean ac! Etiam, lacus vel scelerisque, porta augue, pulvinar sociis sed, ultrices mauris, mattis a ultricies habitasse nunc? Sed elementum cum eros, tincidunt dolor ultricies magnis ridiculus porta in cras ultrices scelerisque habitasse placerat. Enim augue cum augue est phasellus turpis velit tristique etiam augue habitasse. Lacus auctor, sed arcu dolor cursus. Lundium integer! Habitasse, dapibus sit arcu? Sit, lacus! Vut ac, vel augue nunc. Placerat ut, sed quis ac eros pid, phasellus, in, et elit. Tincidunt cum, lectus, facilisis tristique rhoncus placerat in eu et. Lundium in! Aenean dictumst nec, ut! Magna arcu. Nisi, mid montes pellentesque risus scelerisque, porta penatibus, pellentesque adipiscing integer dolor, mus auctor porttitor lundium auctor facilisis nisi pid magna ut ac. Dignissim.</p>', '2012-09-25 21:54:37', 35, 1, 1, 'fed1236b669b34cd28494a98a8b7ce80.jpg', 1, 0, NULL),
	(45, 'не на главной 9', '<p>ывап</p>', '2012-09-23 19:02:43', 33, 1, 0, NULL, 1, 0, NULL),
	(71, 'не на главной 4', '<p>Ultricies augue mauris urna augue in a vut phasellus hac lacus sed arcu pulvinar tincidunt augue rhoncus cum, elit eros montes vel, mattis egestas, dapibus porta diam nunc, rhoncus auctor auctor cras dignissim nec hac odio mus massa enim amet, facilisis nascetur, odio aliquam augue lorem turpis tincidunt magna, velit penatibus, tincidunt a et porta ridiculus arcu massa augue pid. Tincidunt cras nascetur penatibus? Sed, nunc, ridiculus amet velit duis, urna rhoncus vel in, habitasse a nunc. Lorem mid augue, sed arcu augue! Dapibus, pid, nunc rhoncus nunc et turpis nascetur ridiculus, integer? Ultrices hac integer, vut proin adipiscing, enim aliquam elementum mus in tincidunt velit, in amet aenean placerat placerat a pellentesque scelerisque amet augue cras quis turpis. Dictumst.</p>\r\n<p>Ac scelerisque placerat vel mus, risus ac? Et turpis quis scelerisque adipiscing turpis mattis aliquam tincidunt sit dis in, elementum ut pid? Aliquam risus mauris elementum mid aenean dapibus ultrices, velit platea sed, eu placerat vel odio, a nisi! Nec enim lundium lacus. Tempor nec aliquam magnis! Lorem aliquet! Montes adipiscing sed lundium lectus adipiscing tincidunt, nec nisi amet? Ut, odio, dapibus nunc, proin amet tincidunt sed, ac! Amet et elementum odio et scelerisque, eros dapibus augue rhoncus adipiscing, augue ultrices risus. Tincidunt tortor penatibus risus nec! Adipiscing nunc. Duis nec. Ac! Sagittis nisi porttitor, quis porttitor aliquam nisi porta pellentesque elementum in aliquam placerat augue integer phasellus ac! Turpis nec. Ac sagittis ultrices, arcu in adipiscing! Purus urna.</p>', '2012-09-12 09:19:00', 26, 1, 0, NULL, 1, 0, NULL),
	(174, 'Minim tenetur sint porro obcaecati', '<p>Integer elementum elit placerat sit rhoncus in sit aliquam in tincidunt. Porta, nunc dictumst aliquam non eros ac, parturient nunc, urna velit, nunc parturient facilisis. Sit natoque adipiscing amet platea purus tristique natoque penatibus, augue vel aliquet sociis turpis in! Elit vut? Risus tincidunt mid et! Nec auctor, dapibus! Turpis tortor. Pid! Eu turpis eros nunc? A hac odio cras, mid eu habitasse amet nunc nec magna velit? Nec rhoncus, ut turpis, magna urna. Dis et eu montes adipiscing pulvinar nunc tristique natoque adipiscing dolor habitasse dictumst, duis est mus phasellus sed placerat sit dis tempor egestas phasellus. Nunc rhoncus, odio platea elementum dignissim risus, turpis cras nec, ridiculus adipiscing lorem lundium. A a et eu mus, lorem in. Magna.</p>\r\n<p>Nunc duis arcu, dapibus nec! Odio purus, ac nunc diam montes tristique in integer. Tortor, tincidunt urna, auctor et, nunc rhoncus. Mus! Lacus? Nisi magna eu in magna habitasse, auctor platea porttitor, pellentesque elementum natoque? Parturient lorem dignissim odio montes tempor, risus lacus amet nisi cras auctor aliquam, nisi rhoncus? Sit. Sed natoque. Quis magnis! Natoque porttitor augue risus, rhoncus lorem, et in! Vel et nec ultricies integer ut? Dis? Quis! Sed hac quis odio? Pulvinar aliquet, pulvinar turpis massa, auctor, hac, nunc, parturient dolor egestas, ac? Pid tortor. Integer dictumst enim turpis scelerisque nascetur, ac, vut enim et ac in, tincidunt ac, porta elementum ac magnis cras in nisi? Lorem porttitor sagittis! Ridiculus nisi enim. In. Et nunc.</p>', '2012-09-12 09:19:00', 34, 1, 1, 'e41d3ce65bc8d985fee0b3f9e5894315.jpg', 1, 0, NULL),
	(179, '111', '<p>111</p>', '2012-09-30 21:08:26', 37, 1, 0, NULL, 1, 0, 'http://site.ru');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;


-- Dumping structure for table esro_site.news_tags
CREATE TABLE IF NOT EXISTS `news_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.news_tags: ~5 rows (approximately)
/*!40000 ALTER TABLE `news_tags` DISABLE KEYS */;
REPLACE INTO `news_tags` (`id`, `news_id`, `tag_id`) VALUES
	(62, 45, 3),
	(76, 175, 2),
	(77, 175, 5),
	(78, 41, 5),
	(79, 179, 5);
/*!40000 ALTER TABLE `news_tags` ENABLE KEYS */;


-- Dumping structure for table esro_site.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.orders: 0 rows
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Dumping structure for table esro_site.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang` char(2) DEFAULT 'ru' COMMENT 'Язык',
  `title` varchar(200) NOT NULL COMMENT 'Наименование статьи',
  `url` varchar(250) DEFAULT NULL COMMENT 'Адрес',
  `text` text NOT NULL COMMENT 'Текст',
  `is_published` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Опубликована',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Создана',
  `ord` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lang` (`lang`)
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.pages: 1 rows
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
REPLACE INTO `pages` (`id`, `lang`, `title`, `url`, `text`, `is_published`, `updated_at`, `ord`) VALUES
	(87, 'ru', 'Статический раздел О СРО', '/about', '<p>Et ac adipiscing lorem hac vel a platea, nec auctor aenean, ridiculus enim egestas. Augue, aenean sit porttitor est. Parturient, egestas nisi, diam! In, dolor mid urna. Sociis in enim, adipiscing non. Rhoncus porta habitasse odio, magnis parturient, et, montes nunc aliquam enim lundium, pulvinar ac a vel, in, eros facilisis. Augue, ut amet? Lundium lundium aliquet vut pellentesque vut. Natoque amet ultrices. Sit, phasellus odio egestas facilisis sit in ac lundium! Magna et enim tortor ut et? Turpis augue, nec habitasse tortor adipiscing etiam, urna hac auctor aliquet ac! Lectus sed? Lorem sagittis lundium pid et odio, velit pid et ut? Dapibus aliquet, non elementum turpis elementum platea adipiscing odio odio, pulvinar integer odio dolor, etiam lundium velit, lacus.</p>\r\n<p>Scelerisque augue sit habitasse odio? Porttitor auctor, vel vel, risus proin porttitor magna dolor. Mid enim a montes adipiscing sociis ridiculus? Ut massa, et ac turpis, in integer nunc! Enim nisi diam? Integer. Purus facilisis! Dapibus aenean mid nec, pid nunc porta adipiscing a, hac nunc urna, aenean vel, urna ut urna in turpis integer lundium eu turpis ut, aliquet cum cum ac in tincidunt elementum? Porttitor cras proin? Lectus eros, et nunc nec, mattis? Aenean vel, lorem enim nunc nisi nisi mid adipiscing odio pid lectus in? Aenean? Pulvinar pulvinar eros habitasse auctor odio, ultricies pulvinar, diam cum! Elementum est facilisis amet, auctor? Diam montes pellentesque ac mus magnis vel, eu sit, tristique turpis turpis cum! Ac lorem.</p>', 1, '2012-09-26 01:41:24', 87);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;


-- Dumping structure for table esro_site.pages_blocks
CREATE TABLE IF NOT EXISTS `pages_blocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang` char(2) DEFAULT 'ru' COMMENT 'Язык',
  `title` varchar(250) NOT NULL COMMENT 'Заголовок',
  `name` varchar(50) NOT NULL COMMENT 'Название (англ.)',
  `text` longtext NOT NULL COMMENT 'Текст',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Добавлено',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_2` (`lang`,`title`),
  UNIQUE KEY `lang_3` (`lang`,`name`),
  KEY `lang` (`lang`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.pages_blocks: 3 rows
/*!40000 ALTER TABLE `pages_blocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_blocks` ENABLE KEYS */;


-- Dumping structure for table esro_site.portfolio
CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `names` varchar(50) NOT NULL COMMENT 'Имена',
  `url` varchar(255) NOT NULL COMMENT 'Адрес',
  `name` varchar(50) NOT NULL COMMENT 'Название свадьбы',
  `description` text NOT NULL COMMENT 'Описание',
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `review` int(11) NOT NULL COMMENT 'Связь с отзывом',
  `is_published` tinyint(1) NOT NULL COMMENT 'Показывать на сайте?',
  `order` int(11) NOT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.portfolio: 19 rows
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;
/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;


-- Dumping structure for table esro_site.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT 'Заголовок',
  `url` varchar(250) DEFAULT NULL COMMENT 'Адрес',
  `text` text NOT NULL COMMENT 'Текст',
  `date` date NOT NULL COMMENT 'Дата публикации',
  `order` int(11) NOT NULL COMMENT 'Сортировка',
  `is_published` tinyint(1) NOT NULL COMMENT 'Показывать на сайте?',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.posts: 4 rows
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;


-- Dumping structure for table esro_site.press
CREATE TABLE IF NOT EXISTS `press` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `names` varchar(50) NOT NULL COMMENT 'Публикация',
  `url` varchar(250) DEFAULT NULL COMMENT 'Адрес',
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `description` text NOT NULL COMMENT 'Описание',
  `date` date NOT NULL COMMENT 'Дата публикации',
  `is_published` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Показывать на сайте?',
  `order` int(11) NOT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.press: 2 rows
/*!40000 ALTER TABLE `press` DISABLE KEYS */;
/*!40000 ALTER TABLE `press` ENABLE KEYS */;


-- Dumping structure for table esro_site.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `names` varchar(50) NOT NULL COMMENT 'Имена',
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `description` text NOT NULL COMMENT 'Описание',
  `portfolio` int(11) NOT NULL COMMENT 'Связь с портфолио',
  `is_published` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Показывать на сайте?',
  `order` int(11) NOT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.reviews: 7 rows
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;


-- Dumping structure for table esro_site.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'Название услуги',
  `url` varchar(255) NOT NULL COMMENT 'Адрес',
  `description` text NOT NULL COMMENT 'Описание',
  `image` varchar(255) NOT NULL COMMENT 'Иконка',
  `is_published` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Отображать на сайте?',
  `order` int(11) NOT NULL COMMENT 'Порядок',
  PRIMARY KEY (`id`),
  KEY `is_published` (`is_published`),
  KEY `order` (`order`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.services: 22 rows
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;


-- Dumping structure for table esro_site.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` varchar(50) NOT NULL COMMENT 'Модуль',
  `code` varchar(50) NOT NULL COMMENT 'Код',
  `name` varchar(100) NOT NULL COMMENT 'Заголовок',
  `value` text NOT NULL COMMENT 'Значение',
  `element` enum('text','textarea','editor') NOT NULL COMMENT 'Элемент',
  `hidden` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Скрыта',
  PRIMARY KEY (`id`),
  UNIQUE KEY `const` (`code`),
  UNIQUE KEY `title` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.settings: 31 rows
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
REPLACE INTO `settings` (`id`, `module_id`, `code`, `name`, `value`, `element`, `hidden`) VALUES
	(24, 'mailer', 'from_email', 'От кого(Email)', 'no-reply@esro.ru', 'text', 0),
	(25, 'comments', 'admin_email', 'E-mail адрес для уведомлений', 'admin@esro.ru', 'text', 0),
	(26, 'faq', 'faq_email', 'Email для отправки вопросов', 'faq@esro.ru', 'text', 0),
	(41, 'faq', 'faq_mail_body', 'Текст письма при отправке нового вопроса', '<p>Вопрос:</p><p>{QUESTION}</p><p><a href="{ADMIN_ANSWER_URL}">Подготовить ответ</a></p>', 'editor', 0),
	(35, 'users', 'registration_mail_body', 'Текст письма после регистрации', '<p>Здравствуйте, <span>{FIRST_NAME}</span>!<br /> Добро пожаловать на сайт&nbsp;<span><a href="{SITE_NAME}">{SITE_NAME}</a></span><br /> Ваш логин:&nbsp;<span>{USER_EMAIL}</span><br /><br /> Ваш пароль для входа в личный кабинет:&nbsp;{NATIVE_PASSWORD}<br /><br /></p>\r\n<hr />\r\n<p>Данное  письмо  было  сгенерировано  службой  автоматической  рассылки.  Просим  Вас  не отвечать на него. Если у Вас возник вопрос по работе с сайтом, просим Вас обращаться по адресу:&nbsp;<span>{SUPPORT_EMAIL}</span></p>', 'editor', 0),
	(34, 'users', 'registration_mail_subject', 'Тема сообщения при успешной регистрации', 'Информационный портал ESRO.ru - Регистрация', 'text', 0),
	(36, 'users', 'registration_done_message', 'Флеш сообщение для пользователя при успешной регистрации', 'Поздравляю, Вы успешно зарегистрированы!', 'text', 0),
	(37, 'users', 'change_password_request_mail_subject', 'Тема сообщения при восстановлении пароля', 'Восстановление пароля от информационного портала ESRO.ru', 'text', 0),
	(38, 'users', 'change_password_request_mail_body', 'Текст письма для восстановления пароля', '<p>Уважаемый, <span>{FIRST_NAME}</span><br /> Вы запросили восстановление логина и пароля для сайта<br />{SITE_NAME}. <br /> Ваши учетные данные для авторизации на сайте: <br /> Пароль: <span>{NATIVE_PASSWORD}</span><br /><br /></p>\r\n<hr />\r\n<p>Данное  письмо  было  сгенерировано  службой  автоматической  рассылки.  Просим  Вас  не отвечать на него. Если у Вас возник вопрос по работе с сайтом, просим Вас обращаться по адресу: <span>{SUPPORT_EMAIL}</span></p>', 'editor', 0),
	(42, 'faq', 'faq_mail_subject', 'Тема письма при отправке нового вопроса', 'Вопрос от пользователя с сайта - {SITE_NAME}', 'editor', 0),
	(43, 'faq', 'question_send_done', 'Сообщение, которое выводится при успешной отправки вопроса', 'Ваш вопрос был успешно отправлен. После обработки его Администратором, он будет опубликован.', 'editor', 0),
	(44, 'faq', 'faq_notify_mail_body', 'Текст уведомления при ответе на вопрос', '<p>На сайте {SITE_NAME} был опубликован  ответ на Ваш вопрос: <br/><p>{QUESTION}</p><br/><a href="{ANSWER_URL}">Просмотреть ответ</a></p><br/><hr/><p>Данное  письмо  было  сгенерировано  службой  автоматической  рассылки. Просим  Вас  не  отвечать  на  него.  Если  у  Вас  возник  вопрос  по  работе  с сайтом, просим Вас обращаться по адресу: {SUPPORT_EMAIL}</p>', 'editor', 0),
	(45, 'faq', 'faq_notify_mail_subject', 'Тема уведомления при ответе на вопрос', 'Ответ на Ваш вопрос - {SITE_NAME}', 'editor', 0),
	(46, 'comments', 'comments_mail_body', 'Текст письма при добавлении нового комментария', '<p>На вашем сайте в статье <a href="{SRO_HREF}">{SRO_NAME}</a> был опубликован комментарий:</p>\r\n<p><a href="{SRO_HREF}">{COMMENT_TEXT}</a></p>\r\n<p>Пользователем:&nbsp;{FIRST_NAME},&nbsp;{USER_EMAIL}</p>', 'editor', 0),
	(47, 'comments', 'comments_mail_subject', 'Тема письма при добавлении нового комментария', 'Новый комментарий к статье {SRO_NAME}', 'editor', 0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;


-- Dumping structure for table esro_site.site_actions
CREATE TABLE IF NOT EXISTS `site_actions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL COMMENT 'Пользователь',
  `title` varchar(200) NOT NULL COMMENT 'Заголовок',
  `module` varchar(50) NOT NULL COMMENT 'Модуль',
  `controller` varchar(50) NOT NULL COMMENT 'Контроллер',
  `action` varchar(50) NOT NULL COMMENT 'Действие',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.site_actions: 0 rows
/*!40000 ALTER TABLE `site_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_actions` ENABLE KEYS */;


-- Dumping structure for table esro_site.sro_categories
CREATE TABLE IF NOT EXISTS `sro_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Сферы деятельности СРО';

-- Dumping data for table esro_site.sro_categories: ~3 rows (approximately)
/*!40000 ALTER TABLE `sro_categories` DISABLE KEYS */;
REPLACE INTO `sro_categories` (`id`, `name`) VALUES
	(3, 'Изыскание'),
	(2, 'Проектирование'),
	(1, 'Строительство');
/*!40000 ALTER TABLE `sro_categories` ENABLE KEYS */;


-- Dumping structure for table esro_site.sro_cities
CREATE TABLE IF NOT EXISTS `sro_cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `region_id` (`region_id`),
  CONSTRAINT `sro_cities_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `sro_regions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Города';

-- Dumping data for table esro_site.sro_cities: ~3 rows (approximately)
/*!40000 ALTER TABLE `sro_cities` DISABLE KEYS */;
REPLACE INTO `sro_cities` (`id`, `name`, `region_id`) VALUES
	(1, 'Абаза', 1),
	(2, 'Абакан', 1),
	(3, 'Абдулино', 2);
/*!40000 ALTER TABLE `sro_cities` ENABLE KEYS */;


-- Dumping structure for table esro_site.sro_regions
CREATE TABLE IF NOT EXISTS `sro_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Регионы РФ';

-- Dumping data for table esro_site.sro_regions: ~2 rows (approximately)
/*!40000 ALTER TABLE `sro_regions` DISABLE KEYS */;
REPLACE INTO `sro_regions` (`id`, `name`) VALUES
	(2, 'Приволжский'),
	(1, 'Сибирский');
/*!40000 ALTER TABLE `sro_regions` ENABLE KEYS */;


-- Dumping structure for table esro_site.sro_sro
CREATE TABLE IF NOT EXISTS `sro_sro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL COMMENT 'Сфера деятельности',
  `name` varchar(300) NOT NULL COMMENT 'Полное наименование',
  `short_name` varchar(300) NOT NULL COMMENT 'Сокращенное наименование',
  `register_number` varchar(255) DEFAULT NULL COMMENT 'Номер в Реестре',
  `register_date` date DEFAULT NULL COMMENT 'Дата регистрации',
  `register_full_number` varchar(255) DEFAULT NULL COMMENT 'Номер в Реестре',
  `city_id` int(10) unsigned NOT NULL COMMENT 'Город',
  `members_count` int(10) unsigned DEFAULT NULL COMMENT 'Количество членов',
  `rating` int(10) unsigned DEFAULT NULL COMMENT 'Рейтинг',
  `site` varchar(255) DEFAULT NULL COMMENT 'Web-сайт',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail',
  `phone1` varchar(255) DEFAULT NULL COMMENT 'Телефон 1',
  `phone2` varchar(255) DEFAULT NULL COMMENT 'Телефон 2',
  `phone3` varchar(255) DEFAULT NULL COMMENT 'Телефон 3',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Факс',
  `order` int(11) NOT NULL COMMENT 'Сортировка',
  `views` int(11) NOT NULL DEFAULT '0' COMMENT 'Просмотров',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `sro_sro_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `sro_categories` (`id`),
  CONSTRAINT `sro_sro_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `sro_cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.sro_sro: ~45 rows (approximately)
/*!40000 ALTER TABLE `sro_sro` DISABLE KEYS */;
REPLACE INTO `sro_sro` (`id`, `category_id`, `name`, `short_name`, `register_number`, `register_date`, `register_full_number`, `city_id`, `members_count`, `rating`, `site`, `email`, `phone1`, `phone2`, `phone3`, `fax`, `order`, `views`) VALUES
	(1, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 1', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 1', 'СРО-И-001-28042009', '2002-02-02', 'СРО-П-СРО-И-001-28042009-02022002', 2, 7481, 987, 'http://www.yandex.ru', 'qwerqw@asdf.tu', '8 (495) 1234567', '8 (495) 1234566', '8 (495) 1234565', '8 (495) 1234564', 1, 0),
	(2, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 2', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 2', NULL, '2002-03-03', 'СРО-И-03032002', 3, 5266, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0),
	(3, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 3', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 3', NULL, '2002-04-04', 'СРО-С-04042002', 1, 9270, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0),
	(4, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 4', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 4', NULL, '2002-05-05', 'СРО-П-05052002', 2, 1822, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 0),
	(5, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 5', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 5', NULL, '2002-06-06', 'СРО-И-06062002', 3, 4296, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0),
	(6, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 6', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 6', NULL, '2002-07-07', 'СРО-С-07072002', 1, 3746, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 0),
	(7, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 7', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 7', NULL, '2002-08-08', 'СРО-П-08082002', 2, 6739, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 0),
	(8, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 8', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 8', NULL, '2002-09-09', 'СРО-И-09092002', 3, 6621, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 0),
	(9, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 9', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 9', NULL, '2002-10-10', 'СРО-С-10102002', 1, 5467, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 0),
	(10, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 10', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 10', NULL, '2002-11-11', 'СРО-П-11112002', 2, 6972, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0),
	(11, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 11', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 11', NULL, '2002-01-12', 'СРО-И-12012002', 3, 1533, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 0),
	(12, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 12', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 12', NULL, '2002-02-13', 'СРО-С-13022002', 1, 9760, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 0),
	(13, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 13', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 13', NULL, '2002-03-14', 'СРО-П-14032002', 2, 5680, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 0),
	(14, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 14', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 14', NULL, '2002-04-15', 'СРО-И-15042002', 3, 5882, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, 0),
	(15, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 15', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 15', NULL, '2002-05-16', 'СРО-С-16052002', 1, 5838, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, 0),
	(16, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 16', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 16', NULL, '2002-06-17', 'СРО-П-17062002', 2, 9676, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 0),
	(17, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 17', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 17', NULL, '2002-07-18', 'СРО-И-18072002', 3, 4624, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17, 0),
	(18, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 18', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 18', NULL, '2002-08-19', 'СРО-С-19082002', 1, 6413, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 0),
	(19, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 19', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 19', NULL, '2002-09-20', 'СРО-П-20092002', 2, 6843, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19, 0),
	(20, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 20', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 20', NULL, '2002-10-21', 'СРО-И-21102002', 3, 9806, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, 0),
	(21, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 21', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 21', NULL, '2002-11-22', 'СРО-С-22112002', 1, 6987, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21, 0),
	(22, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 22', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 22', NULL, '2002-01-23', 'СРО-П-23012002', 2, 9503, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 22, 0),
	(23, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 23', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 23', NULL, '2002-02-24', 'СРО-И-24022002', 3, 6734, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 23, 0),
	(24, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 24', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 24', NULL, '2002-03-25', 'СРО-С-25032002', 1, 8589, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24, 0),
	(25, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 25', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 25', NULL, '2002-04-26', 'СРО-П-26042002', 2, 2454, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 25, 0),
	(26, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 26', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 26', NULL, '2002-05-27', 'СРО-И-27052002', 3, 1088, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26, 0),
	(27, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 27', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 27', NULL, '2002-06-28', 'СРО-С-28062002', 1, 2700, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 27, 0),
	(28, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 28', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 28', NULL, '2002-07-01', 'СРО-П-01072002', 2, 9462, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 28, 0),
	(29, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 29', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 29', NULL, '2002-08-02', 'СРО-И-02082002', 3, 5714, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29, 0),
	(30, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 30', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 30', NULL, '2002-09-03', 'СРО-С-03092002', 1, 2107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 0),
	(31, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 31', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 31', NULL, '2002-10-04', 'СРО-П-04102002', 2, 6927, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31, 0),
	(32, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 32', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 32', NULL, '2002-11-05', 'СРО-И-05112002', 3, 861, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, 0),
	(33, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 33', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 33', NULL, '2002-01-06', 'СРО-С-06012002', 1, 6456, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 33, 0),
	(34, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 34', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 34', NULL, '2002-02-07', 'СРО-П-07022002', 2, 8498, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 0),
	(35, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 35', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 35', NULL, '2002-03-08', 'СРО-И-08032002', 3, 6604, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 0),
	(36, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 36', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 36', NULL, '2002-04-09', 'СРО-С-09042002', 1, 6227, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 36, 0),
	(37, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 37', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 37', NULL, '2002-05-10', 'СРО-П-10052002', 2, 9365, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, 0),
	(38, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 38', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 38', NULL, '2002-06-11', 'СРО-И-11062002', 3, 1197, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38, 0),
	(39, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 39', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 39', NULL, '2002-07-12', 'СРО-С-12072002', 1, 3917, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39, 0),
	(40, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 40', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 40', NULL, '2002-08-13', 'СРО-П-13082002', 2, 3994, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40, 0),
	(41, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 41', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 41', NULL, '2002-09-14', 'СРО-И-14092002', 3, 4129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41, 0),
	(42, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 42', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 42', NULL, '2002-10-15', 'СРО-С-15102002', 1, 6142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 42, 0),
	(43, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 43', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 43', NULL, '2002-11-16', 'СРО-П-16112002', 2, 6054, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 43, 0),
	(44, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 44', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 44', NULL, '2002-01-17', 'СРО-И-17012002', 3, 7602, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44, 0),
	(45, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 45', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 45', NULL, '2002-02-18', 'СРО-С-18022002', 1, 5436, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, 0),
	(46, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 46', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 46', NULL, '2002-03-19', 'СРО-П-19032002', 2, 4271, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46, 0),
	(47, 3, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 47', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 47', NULL, '2002-04-20', 'СРО-И-20042002', 3, 5204, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 47, 0),
	(48, 1, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 48', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 48', NULL, '2002-05-21', 'СРО-С-21052002', 1, 5486, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48, 0),
	(49, 2, 'Саморегулируемая организация «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 49', 'СРО «Объединение организаций выполняющих инженерные изыскания при архитектурно строительном проектировании, строительстве, реконструкции, капитальном ремонте объектов атомной отрасли «СОЮЗАТОМГЕО» 49', NULL, '2002-06-22', 'СРО-П-22062002', 2, 2974, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 49, 0);
/*!40000 ALTER TABLE `sro_sro` ENABLE KEYS */;


-- Dumping structure for table esro_site.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT 'Тег',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.tags: ~3 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
REPLACE INTO `tags` (`id`, `name`) VALUES
	(2, 'СРО'),
	(3, 'Госорганы'),
	(4, 'asdf'),
	(5, 'НОИЗ');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;


-- Dumping structure for table esro_site.tbl_migration
CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.tbl_migration: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
REPLACE INTO `tbl_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1348907766),
	('m120926_145031_sro', 1348907768),
	('m120928_103916_add_sort_to_sro', 1348907770);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;


-- Dumping structure for table esro_site.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL COMMENT 'Имя',
  `last_name` varchar(40) DEFAULT NULL COMMENT 'Фамилия',
  `patronymic` varchar(40) DEFAULT NULL COMMENT 'Отчество',
  `email` varchar(200) NOT NULL COMMENT 'Email',
  `phone` varchar(50) DEFAULT NULL COMMENT 'Мобильный телефон',
  `password` varchar(32) NOT NULL COMMENT 'Пароль',
  `birthdate` date DEFAULT NULL COMMENT 'Дата рождения',
  `gender` enum('man','woman') DEFAULT 'man' COMMENT 'Пол',
  `status` enum('active','new','blocked') DEFAULT 'active' COMMENT 'Статус',
  `activate_code` varchar(32) DEFAULT NULL COMMENT 'Код активации',
  `activate_date` datetime DEFAULT NULL COMMENT 'Дата активации',
  `password_recover_code` varchar(32) DEFAULT NULL,
  `password_recover_date` datetime DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL COMMENT 'Аватар пользователя',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Зарегистрирован',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- Dumping data for table esro_site.users: 7 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `first_name`, `last_name`, `patronymic`, `email`, `phone`, `password`, `birthdate`, `gender`, `status`, `activate_code`, `activate_date`, `password_recover_code`, `password_recover_date`, `avatar`, `date_create`) VALUES
	(1, 'Администратор', NULL, NULL, 'admin@esro.ru', NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, 'man', 'active', NULL, NULL, NULL, NULL, NULL, '2011-05-19 03:25:50'),
	(2, 'moderOne', NULL, NULL, 'moder1@moder.ru', NULL, '9ab97e0958c6c98c44319b8d06b29c94', NULL, 'man', 'active', NULL, NULL, NULL, NULL, NULL, '2011-09-22 20:19:48'),
	(3, 'Пользователь', NULL, NULL, 'user@user.ru', NULL, 'ee11cbb19052e40b07aac0ca060c23ee', NULL, 'man', 'active', NULL, NULL, NULL, NULL, NULL, '2011-09-30 15:23:57'),
	(38, 'Stephen', NULL, NULL, 'ryzezypykosa@hotmail.com', NULL, '5324e078ff39acf1bfb709a1675c1a86', NULL, 'man', 'active', '94728d5347d29db8ddfb2730551949a4', NULL, NULL, NULL, NULL, '2012-09-25 21:23:38'),
	(39, 'Ferdinand', NULL, NULL, 'fafogyfapek@yahoo.com', NULL, '5324e078ff39acf1bfb709a1675c1a86', NULL, 'man', 'active', 'e0b75ca5144e31a9fe143588ee143750', NULL, NULL, NULL, NULL, '2012-09-25 21:24:05'),
	(40, 'Austin', NULL, NULL, 'joverag@yahoo.com', NULL, 'bcc7dc5e6ba242693ef64f426e3ff3bf', NULL, 'man', 'active', '7b45b9e061abb908e755bacfbf27cc08', NULL, NULL, '2012-10-01 23:00:48', NULL, '2012-10-01 22:21:25'),
	(37, 'moderTwo', NULL, NULL, 'moder2@moder.ru', NULL, '9ab97e0958c6c98c44319b8d06b29c94', NULL, 'man', 'active', 'af2cc81ba6271b2b0f4e486e7a5878b4', NULL, NULL, NULL, NULL, '2012-09-23 23:13:32');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
