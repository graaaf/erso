<?php

class m121001_035328_sro_grants extends CDbMigration
{
	public function up()
	{
        $this->execute("
            INSERT INTO auth_items(NAME, description, allow_for_all) VALUES
            ('Sro_Index', 'СРО - поиск', 1),
            ('Sro_View', 'СРО - просмотр карточки организации', 1)
        ");
	}

	public function down()
	{
		echo "m121001_035328_sro_grants does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}