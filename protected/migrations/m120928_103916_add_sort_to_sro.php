<?php

class m120928_103916_add_sort_to_sro extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE sro_sro ADD `order` INT(11) NOT NULL COMMENT 'Сортировка'");
	}

	public function down()
	{
		echo "m120928_103916_add_sort_to_sro does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}