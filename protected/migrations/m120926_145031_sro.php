<?php

class m120926_145031_sro extends CDbMigration {

    public function up() {
        $this->execute("
            -- DROP TABLE IF EXISTS sro_sro;
            -- DROP TABLE IF EXISTS sro_categories;
            -- DROP TABLE IF EXISTS sro_cities;
            -- DROP TABLE IF EXISTS sro_regions;

            CREATE TABLE sro_regions(
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NOT NULL,
            PRIMARY KEY(id),
            UNIQUE(`name`)
            ) ENGINE = INNODB CHARSET=utf8 COMMENT 'Регионы РФ';

            INSERT INTO sro_regions(id, `name`) VALUES
            (1, 'Сибирский'),
            (2, 'Приволжский')
            ;

            CREATE TABLE sro_cities(
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NOT NULL,
            `region_id` INT UNSIGNED NOT NULL,
            PRIMARY KEY(id),
            UNIQUE(`name`),
            FOREIGN KEY(`region_id`) REFERENCES sro_regions(id)
            ) ENGINE = INNODB CHARSET=utf8 COMMENT 'Города';

            INSERT INTO sro_cities(`region_id`, `name`) VALUES
            (1, 'Абаза'),
            (1, 'Абакан'),
            (2, 'Абдулино');

            CREATE TABLE sro_categories (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NOT NULL,
            PRIMARY KEY(id),
            UNIQUE(`name`)
            ) ENGINE = INNODB CHARSET=utf8 COMMENT 'Сферы деятельности СРО';

            INSERT INTO sro_categories(id, NAME) VALUES
            (1, 'Строительство'),
            (2, 'Проектирование'),
            (3, 'Изыскание');

            CREATE TABLE sro_sro (
            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `category_id` INT UNSIGNED NOT NULL COMMENT 'Сфера деятельности',
            `name` VARCHAR(300) NOT NULL COMMENT 'Полное наименование',
            `short_name` VARCHAR(300) NOT NULL COMMENT 'Сокращенное наименование',
            `register_number` VARCHAR(255) NULL COMMENT 'Номер в Реестре',
            `register_date` DATE COMMENT 'Дата регистрации',
            `register_full_number` VARCHAR(255) NULL COMMENT 'Номер в Реестре',
            `city_id` INT UNSIGNED NOT NULL COMMENT 'Город',
            `members_count` INT UNSIGNED NULL COMMENT 'Количество членов',
            `rating` INT UNSIGNED NULL COMMENT 'Рейтинг',
            `site` VARCHAR(255) NULL COMMENT 'Web-сайт',
            `email` VARCHAR(255) NULL COMMENT 'E-mail',
            `phone1` VARCHAR(255) NULL COMMENT 'Телефон 1',
            `phone2` VARCHAR(255) NULL COMMENT 'Телефон 2',
            `phone3` VARCHAR(255) NULL COMMENT 'Телефон 3',
            `fax` VARCHAR(255) NULL COMMENT 'Факс',
            PRIMARY KEY(id),
            FOREIGN KEY(category_id) REFERENCES `sro_categories`(id),
            FOREIGN KEY(city_id) REFERENCES `sro_cities`(id)
            ) ENGINE = INNODB CHARSET=utf8;
        ");
    }

    public function down() {
        echo "m120926_145031_sro does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}