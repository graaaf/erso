<?php

class m121010_144007_add_memebers_count_date_to_sro extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE sro_sro ADD `members_count_date` DATE NULL COMMENT 'Количество членов (дата)' AFTER `members_count` ");
	}

	public function down()
	{
		echo "m121010_144007_add_memebers_count_date_to_sro does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}