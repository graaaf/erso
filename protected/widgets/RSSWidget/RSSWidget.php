<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ilya
 * Date: 11.10.12
 * Time: 12:26
 * To change this template use File | Settings | File Templates.
 */

class RSSWidget extends CWidget {

    /**
     * Pages on which widget will be displayed
     *
     *  array(
     *      'controller_id' => 'action_id',
     *      'controller_id' => 'action_id',
     *      'controller_id' => 'action_id',
     *  )
     */
    private $_viewPages = array(
        'news/index',
        'news/view',
        'faq/index',
    );


    public function init()
    {
    }


    public function run()
    {
        $currentPage = Yii::app()->controller->id . '/' . Yii::app()->controller->action->id;

        if( in_array($currentPage, $this->_viewPages) )
        {
            $this->render('view');
        }
    }
}