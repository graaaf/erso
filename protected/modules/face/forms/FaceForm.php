<?php

return array(
    'activeForm' => array(
        'id'                   => 'face-form',
        'htmlOptions'          => array(
            'enctype' => 'multipart/form-data'
        ),
        'enableAjaxValidation' => false,
    ),
    'elements'   => array(
        'fio'         => array('type' => 'text'),
        'description' => array('type' => 'textarea'),
        'photo'       => array(
            'type' => 'imageCrop',
            'hint' => 'Рекомендуется загружать файл размером 170x170 px.<br/>Форматы загружаемого файла: *JPEG, *GIF, *PNG.'
        ),
        'is_active'   => array('type' => 'checkbox'),
    ),
    'buttons'    => array(
        'submit' => array(
            'type'  => 'submit',
            'value' => $this->model->isNewRecord ? 'создать' : 'сохранить'
        )
    )
);
