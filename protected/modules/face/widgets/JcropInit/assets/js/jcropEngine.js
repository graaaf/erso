function createJcrop( absFileName, coords )
{
    var fileName = absFileName.match( /[a-z0-9]{32}.[a-z0-9]{3,4}$/i );

    // Hide upload area
    $('#uploadFile').slideUp(200);


    // Set value to hidden field
    $('#Face_photo').attr('value', fileName);


    // Create image to crop
    var image = $('<img>').attr(
    {
        'src': absFileName,
        'id': 'image_crop'
    }
    ).appendTo('#image_crop_wrapper');


    // Jcrop init
    image.Jcrop({
        'aspectRatio' : 1,
        'minSize' : [170, 180],
        'maxSize' : [800, 800],
        'setSelect' : coords || [0, 0, 170, 170],
        'onChange' : function(c) {
            ejcrop_getCoords(c,'image_crop');
            ejcrop_showThumb(c,'image_crop');
        }
    });
}