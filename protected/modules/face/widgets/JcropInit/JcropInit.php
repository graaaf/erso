<?php

/**
 * Jcrop Yii extension
 * 
 * Select a cropping area fro an image using the Jcrop jQuery tool and crop
 * it using PHP's GD functions.
 *
 * @copyright © Digitick <www.digitick.net> 2011
 * @license GNU Lesser General Public License v3.0
 * @author Jacques Basseck
 * @author Ianaré Sévi
 *
 */
Yii::import('zii.widgets.jui.CJuiWidget');

class JcropInit extends CJuiWidget
{
    public $id;

	public function init()
	{
		$assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets');

        $id = $this->id;

		echo CHtml::hiddenField($id . '_x', 0, array('class' => 'coords'));
		echo CHtml::hiddenField($id . '_y', 0, array('class' => 'coords'));
		echo CHtml::hiddenField($id . '_w', 0, array('class' => 'coords'));
		echo CHtml::hiddenField($id . '_h', 0, array('class' => 'coords'));
		echo CHtml::hiddenField($id . '_x2', 0, array('class' => 'coords'));
		echo CHtml::hiddenField($id . '_y2', 0, array('class' => 'coords'));

		$cls = Yii::app()->getClientScript();
		$cls->registerScriptFile($assets . '/js/jquery.Jcrop.min.js');
		$cls->registerScriptFile($assets . '/js/ejcrop.js', CClientScript::POS_HEAD);
		$cls->registerScriptFile($assets . '/js/jcropEngine.js', CClientScript::POS_HEAD);
		$cls->registerCssFile($assets . '/css/jquery.Jcrop.css');
	}


}
