<?php

$this->tabs = array(
    'управление лицами' => $this->createUrl('manage'),
    'редактировать'     => $this->createUrl('update', array('id' => $model->id))
);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'fio'),
        array('name' => 'description'),
        array('name' => 'is_active', 'value' => $model->is_active ? "Да" : "Нет"),
        array(
            'name' => 'photo',
            'value' => $model->image,
            'type' => 'raw'
        ),
    ),
));



