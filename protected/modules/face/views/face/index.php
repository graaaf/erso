<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        'Лица СРО',
    ),
));
?>

<h3>Лица СРО</h3>
<div class = "b-people">
    <?php foreach ($faces as $item): ?>
        <section class="b-people_list">
            <div class = "b-people_list_img">
                <?php echo $item->image ?>
            </div>
            <?php echo CHtml::link( $item->fio, array('#')); ?>
            <p><?php echo $item->description; ?></p>
        </section>
    <?php endforeach; ?>
    <div class = "g-clear_fix"></div>
</div><!--end b-news-->

<div class="pager">
    <?php $this->widget('CLinkPager', array(
        'pages' => $pagination,
        'cssFile' => false,
        'nextPageLabel' => CHtml::image('/images/pager_arrow.png') . ' Следующая',
        'prevPageLabel' => 'Предыдущая ' . CHtml::image('/images/pager_arrow_back.png'),
        'header' => '<p>Страницы:</p>',
    )) ?>
</div>