<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        'Новости'=>array('/news/news/index'),
        $model->title,
    ),
));
?>

<?php  $this->page_title = $model->title . ' | Информационный портал СРО'; ?>

<h3><?php echo $model->title; ?></h3>
<div class = "b-view">
    <div class = "b-view_tags">
        <?php echo HtmlHelper::timeTag($model->date, 'd MMMM y года, H:mm', array('class'=>'l-date')) ?>
        <nav class = "b-tags">
            <?php $this->widget('news.widgets.TagsWidget', array(
                'tagElements' => $model->news_tags,
                'beforeElement' => CHtml::image('/images/tag_icon.png'),
            ));?>
        </nav>
    </div>

    <div class = "g-clear_fix"></div>
    <?php echo $model->text; ?>
    <?php if( !empty($model->source_link) ): ?>
        <p>
            Оригинал статьи:
            <?php echo $model->source; ?></a>
        </p>
    <?php endif; ?>
</div>

<div class = "g-clear_fix"></div>

<div class = "b-view_soc_share">
	<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="link" data-yashareQuickServices="vkontakte,facebook,twitter"></div> 
	<span class="l-p_with_img">
		<img src="/images/news_view_icon.png">
		Просмотров: <?php echo $model->views; ?>
	</span>
    <div class = "g-clear_fix"></div>
</div>