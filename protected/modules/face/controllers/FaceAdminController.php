<?php

class FaceAdminController extends AdminController
{

    public static function actionsTitles()
    {
        return array(
            'View' => 'Просмотр лица',
            'Create' => 'Создание лица',
            'Update' => 'Редактирование лица',
            'Delete' => 'Удаление лица',
            'Manage' => 'Управление лицами',
            'Upload' => 'Загрузка фото',
        );
    }


    public function actions(){
        return array(
            'upload' => array(
                'class' => 'ext.EAjaxUpload.EAjaxUploadAction',
                'uploadDir' => FileUploadHelper::relToAbsPathDir( Face::IMAGES_DIR ),
                'allowedExtensions' => Face::$allowedExtensions,
            ),
        );
    }


    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


    public function actionCreate()
    {
        $model = new Face(Face::SCENARIO_CREATE);

        if (isset($_POST['Face']))
        {
            $model->fio = $_POST['Face']['fio'];
            $model->photo = $_POST['Face']['photo'];
            $model->is_active = $_POST['Face']['is_active'];
            $model->description = $_POST['Face']['description'];

            if ($model->save())
            {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $form = new BaseForm('face.FaceForm', $model);

        $this->render('create', array(
            'form' => $form,
        ));
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = Face::SCENARIO_UPDATE;

        if (isset($_POST['Face']))
        {
            $model->attributes = $_POST['Face'];

            if ($model->save())
            {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $form = new BaseForm('face.FaceForm', $model);

        $this->render('update', array(
            'form' => $form,
        ));
    }


    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }


    public function actionManage()
    {
        $model = new Face('search');
        $model->unsetAttributes();
        if (isset($_GET['Face']))
        {
            $model->attributes = $_GET['Face'];
        }

        $this->render('manage', array(
            'model' => $model,
        ));
    }


    public function loadModel($id)
    {
        $model = Face::model()->findByPk((int) $id);
        if ($model === null)
        {
            $this->pageNotFound();
        }

        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'face-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
