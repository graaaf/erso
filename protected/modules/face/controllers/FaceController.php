<?php

class FaceController extends BaseController
{

    public $layout = '//layouts/content';

    public $facePerPage = 10;

    public static function actionsTitles()
    {
        return array(
            "View" => "Просмотр лица",
            "Index" => 'Список лиц',
        );
    }


    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $this->render('view', array(
            'model' => $model
        ));
    }


    public function actionIndex()
    {
        $faceScopes = Face::model()->active()->ordered();

        $models = $faceScopes->findAll();

        $pagination = new CPagination( count($models) );
        $pagination->pageSize = $this->facePerPage;
        $pagination->applyLimit( $faceScopes->getDbCriteria() );

        $this->render('index', array(
            'faces' => $models,
            'pagination' => $pagination,
        ));
    }


}
