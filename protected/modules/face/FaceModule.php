<?php

class FaceModule extends WebModule
{

    public static function name()
    {
        return 'Лица';
    }


    public static function description()
    {
        return 'управление лицами';
    }


    public static function version()
    {
        return '1.0';
    }


    public function init()
    {
        $this->setImport(array(
            'face.models.*',
            'face.components.*',
        ));
    }


    public static function adminMenu()
    {
        return array(
            'Управление лицами' => '/face/faceAdmin/manage',
            'Добавить лицо' => '/face/faceAdmin/create',
        );
    }


}
