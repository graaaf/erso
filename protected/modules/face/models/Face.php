<?php

class Face extends ActiveRecordModel
{

    const SIZE_WIDTH = 170;
    const IMAGES_DIR = '/upload/face/';
    const DEFAULT_IMAGE = 'without_face.jpg';
    const PAGE_SIZE = 10;

    public static $allowedExtensions = array("jpg", "jpeg", "gif", "png");

    public $date_active;
    public $src;
    public $pages;


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'face';
    }


    public function name()
    {
        return 'Лица';
    }


    public function rules()
    {
        return array(
            array('fio, description', 'required'),
            array('is_active', 'numerical', 'integerOnly' => true),
            array('fio', 'length', 'max' => 50),
            array('description', 'length', 'max' => 1000),
            array('photo', 'default', 'value' => self::DEFAULT_IMAGE),
            array('photo', 'required', 'on' => self::SCENARIO_CREATE),
            array('id, fio, photo, is_active, description', 'safe', 'on' => 'search'),
        );
    }


    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => 'is_active = 1',
            ),
        );
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['Sortable'] = array(
            'class' => 'ext.sortable.SortableBehavior'
        );

        return $behaviors;
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('fio', $this->fio, true);
        $criteria->compare('photo', $this->photo, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('description', $this->description);
        $criteria->order = '`order`';

        return new ActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }


    public function getImage()
    {
        return CHtml::image( self::IMAGES_DIR . $this->photo, '', array('class' => 'maskable') );
    }


    public function beforeSave()
    {
        // Crop photo
        if( $_POST['image_crop_w'] != 0 && $_POST['image_crop_h'] != 0)
        {
            Yii::import( 'application.modules.face.widgets.JcropInit.EJCropper' );

            $jcropper = new EJCropper();
            $jcropper->thumbPath = FileUploadHelper::relToAbsPathDir( self::IMAGES_DIR );

            $jcropper->jpeg_quality = 95;
            $jcropper->png_compression = 8;

            $coords = $jcropper->getCoordsFromPost( 'image_crop' );

            $absFile = FileUploadHelper::relToAbsPathFile( self::IMAGES_DIR . $this->photo );
            $success = (bool)$jcropper->crop( $absFile, $coords );

            if ( !$success )
            {
                $this->addError('photo', 'При сохранении изображения произошла ошибка');
            }
        }

        return parent::beforeSave();
    }


}