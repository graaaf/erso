<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        'Вопрос-ответ',
    ),
));
?>

<h3>Вопрос-ответ</h3>

<div class = "g-clear_fix"></div>

<section class="b-ask" xmlns="http://www.w3.org/1999/html">
    <span class="question_word">Вопрос</span>
    <?php echo HtmlHelper::timeTag($model->date_question, 'dd.MM.y, H:mm', array('class' => 'l-date')) ;?>
    <span class="l-page_back">
        <img src="/images/pager_arrow_back.png">
        <a href="<?php echo $this->createUrl('/faq/faq/index'); ?>">Вернуться к списку вопросов</a>
    </span>
    <article>
        <header></header>
        <p><?php echo $model->question; ?></p>
    </article>
</section>

<div class="g-clear_fix"></div>

<section class="b-answer g-clear_fix">
    <?php echo HtmlHelper::timeTag($model->date_answer, 'dd.MM.y, H:mm', array('class' => 'l-date')) ;?>
    <span class="answer_word">Ответ</span>
    <div class="g-clear_fix"></div>
    <article>
        <header></header>
        <?php echo $model->answer; ?>
        <div class="g-clear_fix"></div>
    </article>
</section>

<div class="g-clear_fix"></div>

<section class="b-ask_form">
    <h3>Задать вопрос</h3>
        <span class="l-page_registr">
            <a href = "<?php echo $this->createUrl('/users/user/registration'); ?>">Зарегистрироваться</a>
            <img src="/images/pager_arrow.png">
        </span>
    <div class="g-clear_fix"></div>

    <?php $form=$this->beginWidget('CActiveForm', array(
        'action' => $this->createUrl('/faq/faq/create'),
        'htmlOptions' => array(
            'class' => 'faq-form',
        ),
        'enableAjaxValidation' => true,
    )); ?>

        <?php echo $form->textArea(new Faq(), 'question', array('cols' => 70, 'rows'=>10)); ?>
        <br/>
        <input class="l-button native_submit" type="submit" name="send" value="Отправить">

    <?php $this->endWidget(); ?>

    <?php if (Yii::app()->user->hasFlash('question_send_done')): ?>
        <p><?php echo $this->msg(Yii::app()->user->getFlash('question_send_done'), 'ok'); ?></p>
    <?php endif; ?>

</section>