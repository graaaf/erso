<section class = "b-news_list">
    <article>
        <?php echo HtmlHelper::timeTag($faq->date_question, 'd MMMM y года, H:mm', array('class' => 'l-date')) ;?>
        <nav class = "b-tags">
            <img src = "images/tag_icon.png"/>
            <a href = "">Правовые вопросы</a>,
        </nav>
        <div class = "g-clear_fix"></div>
        <a href="<?php echo $this->createUrl('/faq/faq/view', array( 'id'=> $faq->id )); ?>" class = "question_text">
            <?php echo $faq->question; ?>
        </a>
    </article>
</section>