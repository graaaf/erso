<script type="text/javascript">
    var api = {};
    $(document).ready(function() {
        $('input[placeholder], textarea[placeholder]').placeholder();

        api = jQuery('.jClever').jClever({
            applyTo: {
                select: true,
                checkbox: false,
                radio: false,
                button: false,
                file: false,
                input: false,
                textarea: false
            }
        });

        $('.b-question_search .jClever-element-select-list > li').hover( function() {
            $('i', this).css({'color': '#fff'});
        },
        function() {
            $('i', this).css({'color': '#393939'});
        })
    })
</script>


<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        'Вопрос-ответ',
    ),
));
?>


<h3>Вопрос-ответ</h3>
<div class = "g-clear_fix"></div>


<?php $this->renderPartial('_search', array(
    'model' => $model,
)); ?>


<div class="b-news b-question_list">

    <?php if( count($faqs) ): ?>


        <?php foreach($faqs as $faq): ?>

            <?php $this->renderPartial('_list', array(
                'faq' => $faq,
            )); ?>

        <?php endforeach; ?>


    <?php else: ?>


        <p>В данном разделе вопросы отсутствуют!</p>


    <?php endif; ?>

    <div class="g-clear_fix"></div>

</div>


<div class="pager">
    <?php $this->widget('CLinkPager', array(
        'pages' => $pagination,
        'cssFile' => false,
        'nextPageLabel' => CHtml::image('/images/pager_arrow.png') . ' Следующая',
        'prevPageLabel' => 'Предыдущая ' . CHtml::image('/images/pager_arrow_back.png'),
        'header' => '<p>Страницы:</p>',
    )) ?>
</div>