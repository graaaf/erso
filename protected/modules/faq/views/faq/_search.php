<div class = "b-search b-question_search">
    <?php
    echo CHtml::beginForm('', 'post', array(
        'class' => 'search_form jClever',
    ));
    ?>
        <div class = "search_filters">
            <?php
                echo CHtml::activeTextField($model, 'searchWord', array(
                    'class' => 'search_input',
                    'placeholder' => 'Введите ключевое слово для поиска...',
                ));
            ?>
            <?php
                echo CHtml::tag('button', array(
                    'class' => 'l-button',
                ), 'Найти');
            ?>
            <div class = "row b-question_search_select">
                <select>
                    <option>Правовые вопросы</option>
                    <option>Экспертиза</option>
                    <option>Кадровые вопросы</option>
                    <option>Финансовые вопросы</option>
                    <option>Техническое регулирование</option>
                </select>
            </div>

        </div>
    <?php echo CHtml::endForm(); ?>
</div>
