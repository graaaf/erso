<?php

class LastFaq extends Portlet
{

    public $itemsCount = 3;
    public $viewFile = 'last_faq';

    public function renderContent()
    {
        $this->render($this->viewFile, array(
            'items' => Faq::model()->published()->last()->limit($this->itemsCount)->findAll(),
        ));
    }


}