<?php

class FaqWidget extends Portlet
{

    public function renderContent()
    {
        $model = new Faq;

        $this->render('Faq', array(
            'model' => $model,
        ));
    }


}
