<h1 class="part_title">Вопросы</h1>
<div class="news_list">
    <?php foreach ($items as $model): ?>
        <div class="item">
            <div class="date"><?php echo date('d.m.Y', strtotime($model->date_question)); ?></div>
            <a href="<?php echo '/faq/' . $model->id; ?>">
                 <?php echo Yii::app()->text->cut($model->question, 280, "., -:;", " ..."); ?>
            </a>
        </div>
    <?php endforeach; ?>
    <a href="<?php echo $this->url('/faq'); ?>" style="color: #4A62C1;" class="more">Все вопросы</a><a href="" style="text-decoration: none; color: #4A62C1">&#8594;</a>
</div>