<?php if (Yii::app()->user->hasFlash('feedback_done')): ?>
    
    <div class='message ok' id="feedback" style='display: block;'>
        <p><?php echo Yii::app()->user->getFlash('feedback_done'); ?></p>
    </div>
<?php endif ?>

 <?php $form=$this->beginWidget('CActiveForm', array(
        'id' => 'faq-form',
        'enableAjaxValidation' => true,
     'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>false,
            //'beforeValidate'=>"js:beforeValidate",
        ),
     'htmlOptions' => array('class'=> 'feedback',),
        'action'=>array('/faq/faq/index', '#'=>'faq-form')
    )); ?>
    <?php if (Yii::app()->user->hasFlash('feedback_done')): ?>
    <div class="form_msg">
        <?php echo $this->msg(Yii::app()->user->getFlash('feedback_done'), 'ok'); ?>
    </div>
    <?php else: ?>

    <label>Имя *</label>
    <div>
        <?php echo $form->textField($model, 'first_name', array('size' => 30, 'class' => 'text')); ?>
    </div>
    <div class="clear"></div>
    <div class="form_notification">
        <?php echo $form->error($model,'first_name'); ?>
    </div>

    <label>Фамилия</label>
    <div>
        <?php echo $form->textField($model, 'last_name', array('size' => 30, 'class' => 'text')); ?>
    </div>
    <div class="clear"></div>
    <div class="form_notification">
        <?php echo $form->error($model,'last_name'); ?>
    </div>

    <label>E-mail *</label>
    <div>
        <?php echo $form->textField($model, 'email', array('size' => 30, 'class' => 'text')); ?>
    </div>
    <div class="clear"></div>
    <div class="form_notification">
        <?php echo $form->error($model,'email'); ?>
    </div>

    <label>Телефон</label>
    <div>
        <?php echo $form->textField($model, 'phone', array('size' => 30, 'class' => 'text')); ?>
    </div>
    <div class="clear"></div>
    <div class="form_notification">
        <?php echo $form->error($model,'phone'); ?>
    </div>

    <label>Текст вопроса *</label>
    <div>
        <?php echo $form->textArea($model, 'question', array('cols' => 70, 'rows'=>10)); ?>
    </div>
    <div class="clear"></div>
    <div class="form_notification">
        <?php echo $form->error($model,'question'); ?>
    </div>

    
    <dl>
            <dd><label>* - поле, обязательное для заполнения</label></dd>
         </dl>
    
    <div class="row buttons">
        <input type="submit" name="send" value="Отправить">
    </div>
    <?php endif; ?>
    <?php $this->endWidget(); ?>