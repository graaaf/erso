<?php

class FaqAdminController extends AdminController
{

    const SETTING_NOTIFY_MAIL_SUBJECT = 'faq_notify_mail_subject';
    const SETTING_NOTIFY_MAIL_BODY = 'faq_notify_mail_body';
    const SETTING_NOTIFY_QUESTION_SEND_DONE = 'question_notify_send_done';


    public static function actionsTitles() 
    {
        return array(
            "View"   => "Просмотр вопроса",
            "Create" => "Добавление вопроса",
            "Update" => "Редактирование вопроса",
            "Delete" => "Удаление вопроса",
            "Manage" => "Управление вопросами",
            "Notify" => "Почтовое уведомление",
        );
    }

    public function actionNotify($id)
    {
        $model = $this->loadModel($id);

        // Если пользователь не указал свой e-mail - возвращаем сообщение об ошибке
        if( empty($model->email) )
        {
            Yii::app()->user->setFlash('faq_notify_done', 'Пользователь не указал свой E-mail!');
        }

        // Если пользователь указал свой e-mail и в БД есть запрошенный вопрос - отправляем сообщение пользователю
        elseif (!empty($model))
        {
            $mailer_letter = MailerLetter::model();

            $settings = Setting::model()->findCodesValues();

            $subject = $mailer_letter->compileText($settings[self::SETTING_NOTIFY_MAIL_SUBJECT]);

            $body = $mailer_letter->compileText($settings[self::SETTING_NOTIFY_MAIL_BODY], array(
                'question' => $model->question,
                'answerUrl' => $this->createAbsoluteUrl('/faq/faq/view/', array('id' => $model->id)),
            ));

            $emailFrom = Setting::getValue(MailerModule::SETTING_FROM_EMAIL);

            $success = MailHelper::send($model->email, $subject, $body, $emailFrom);

            if( $success )
            {
                Yii::app()->user->setFlash('faq_notify_done', 'Уведомление успешно отправлено!');
            }
            else
            {
                Yii::app()->user->setFlash('faq_notify_done', 'При отправке уведомления произошла ошибка!');
            }

            $model->date_notify=date('Y-m-d H:i:s');
            $model->save();
        }

        $this->redirect('/faq/faqAdmin/manage');
    }


	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}


	public function actionCreate()
	{
		$model = new Faq;

		$form = new BaseForm('faq.FaqForm', $model);
		
		$this->performAjaxValidation($model);

		if(isset($_POST['Faq']))
		{
			$model->attributes = $_POST['Faq'];
			if($model->save())
            {
                $this->redirect(array('view', 'id' => $model->id));
            }
		}

		$this->render('create', array(
			'form' => $form,
		));
	}


	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

        if( empty($model->date_answer) || ($model->date_answer == '0000-00-00 00:00:00'))
        {
            $model->date_answer = date("Y-m-d H:i:s");
        }

		$form = new BaseForm('faq.FaqForm', $model);
		
		$this->performAjaxValidation($model);

		if(isset($_POST['Faq']))
		{
			$model->attributes = $_POST['Faq'];
			if($model->save())
            {
                $this->redirect(array('view', 'id'=>$model->id));
            }
		}

		$this->render('update', array(
			'form' => $form,
		));
	}


	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel($id)->delete();

			if(!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
		}
		else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
	}


	public function actionManage()
	{
		$model=new Faq('search');
		$model->unsetAttributes();
		if(isset($_GET['Faq']))
        {
            $model->attributes = $_GET['Faq'];
        }

		$this->render('manage', array(
			'model' => $model,
		));
	}


	public function loadModel($id)
	{
		$model=Faq::model()->findByPk((int) $id);
		if($model===null)
        {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'faq-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
