<?php

class FaqController extends BaseController
{

    public $layout = '//layouts/content';

    public $faqPerPage = 5;

    const SETTING_MAIL_SUBJECT = 'faq_mail_subject';
    const SETTING_MAIL_BODY = 'faq_mail_body';
    const SETTING_QUESTION_SEND_DONE = 'question_send_done';


    public static function actionsTitles()
    {
        return array(
            "Index" => "Просмотр списка вопросов",
            "View" => "Просмотр вопроса",
            "Create" => "Добавление вопроса",
        );
    }


    public function actionView($id)
    {
        $model = Faq::model()->published()->findByPk($id);

        if ($model === null)
        {
            $this->pageNotFound();
        }

        $faqs = Faq::model()->last()->published()->notEqual('id', $id)->limit(5)->findAll();

        $this->render('view', array(
            'faqs' => $faqs,
            'model' => $model,
        ));
    }

    public function actionIndex()
    {
        $count = Faq::model()->published()->last()->count();

        $pagination = new CPagination($count);
        $pagination->pageSize = $this->faqPerPage;
        $pagination->applyLimit( Faq::model()->published()->last()->getDbCriteria() );

        $this->render('index', array(
            'faqs' => Faq::model()->published()->last()->findAll(),
            'pagination' => $pagination,
            'model' => new Faq,
        ));
    }


    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'faq-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionCreate()
    {
        $model = new Faq('ClientSide');

        if (isset($_POST['Faq']))
        {
            $model->attributes = $_POST['Faq'];
            if ($model->save())
            {
                $mailer_letter = MailerLetter::model();

                $settings = Setting::model()->findCodesValues();

                $subject = $mailer_letter->compileText($settings[self::SETTING_MAIL_SUBJECT]);

                $body = $mailer_letter->compileText($settings[self::SETTING_MAIL_BODY], array(
                    'question' => $model->question,
                    'adminAnswerUrl' => $this->createAbsoluteUrl('/faq/faqAdmin/update/', array('id' => $model->id)),
                ));

                $emailFrom = Setting::getValue(MailerModule::SETTING_FROM_EMAIL);

                MailHelper::send(Setting::getValue('faq_email'), $subject, $body, $emailFrom);


                Yii::app()->user->setFlash('question_send_done', Setting::getValue(self::SETTING_QUESTION_SEND_DONE));
            }
        }

        $this->redirect( $_SERVER['HTTP_REFERER'] );
    }


}
