<?php

class Banner extends ActiveRecordModel
{

    const SIZE_WIDTH = 275;
    const IMAGES_DIR = '/upload/banners/';
    const PAGE_SIZE = 10;

    public static $positions = array(
        'top' => array(
            'label' => 'Над шапкой сайта',
            'width' => 980,
        ),
        'middle' => array(
            'label' => 'Центральная область',
            'width' => 732,
        ),
        'right1' => array(
            'label' => 'Правая колонка, верх',
            'width' => 223,
        ),
        'right2' => array(
            'label' => 'Правая колонка, низ',
            'width' => 223,
        ),
    );

    public $date_active;
    public $src;
    public $pages;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'banners';
    }


    public function name()
    {
        return 'Баннеры';
    }


    public function rules()
    {
        return array(
            array('name', 'required'),
            array('is_active, page_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 50),
            array(
                'image',
                'file',
                'types' => 'jpg, jpeg, png, gif',
                'allowEmpty' => false,
                'on' => self::SCENARIO_CREATE
            ),
            array(
                'image',
                'file',
                'types' => 'jpg, jpeg, png, gif',
                'allowEmpty' => true,
                'on' => self::SCENARIO_UPDATE
            ),
            array('position', 'in', 'range' => array_keys(self::$positions)),
            array('date_start', 'validateDateInterInterVal'),
            array('url', 'length', 'max' => 500),
            array('date_start, date_end, date_active', 'safe'),
            array('roles, position', 'safe'),
            array('id, name, image, url, is_active, date_start, date_end', 'safe', 'on' => 'search'),
        );
    }


    public function validateDateInterInterVal($attr)
    {
        if ($this->date_active)
        {
            if (!$this->$attr || !$this->date_end)
            {
                $this->addError($attr, 'Укажите и дату начала и дату окончания показа!');
                return;
            }

            if ($this->$attr && $this->date_end)
            {   //v($this->$attr);
                //v($this->date_end);
                //die;
                $date_start = strtotime($this->$attr);
                $date_end = strtotime($this->date_end);

                if ($date_start > $date_end)
                {
                    $this->addError($attr, 'Дата окончания показа не должна быть раньше даты начала показа!');
                }
            }
        }
    }


    public function relations()
    {
        return array(
            'banners_roles' => array(self::HAS_MANY, 'BannerRole', 'banner_id'),
            'roles' => array(self::HAS_MANY, 'AuthItem', 'role', 'through' => 'banners_roles'),
            'page' => array(self::BELONGS_TO, 'Page', 'page_id')
        );
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['Sortable'] = array(
            'class' => 'ext.sortable.SortableBehavior'
        );

        return $behaviors;
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('date_start', $this->date_start, true);
        $criteria->compare('date_end', $this->date_end, true);
        $criteria->order = 'priority';

        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


    public function uploadFiles()
    {
        return array(
            'image' => array('dir' => self::IMAGES_DIR)
        );
    }


    public function render($return = false)
    {
        if (preg_match('#.gif#i', $this->image)) {
                echo "<img src='" . self::IMAGES_DIR . $this->image . "'>";
                return '';
        }
        
        $thumb = ImageHelper::thumb(
                        self::IMAGES_DIR, $this->image, self::SIZE_WIDTH, null, false, 'alt = "' . $this->name . '"'
        );

        if ($return)
        {
            return $thumb;
        } else
        {
            echo $thumb;
        }
    }


    public function beforeSave()
    {
        // Get image info from $_FILES
        $image = CUploadedFile::getInstance($this, 'image');

        if ($image !== null)
        {
            // Define save directory. Example: /Users/John/Sites/esro.dev/uploads/banners
            $saveDir = Yii::getPathOfAlias('webroot') . self::IMAGES_DIR;

            // Define new image name. Example: ee8fe038e341c5b1a80cc67ff7818175.png
            $this->image = md5( $this->image . uniqid() ) . '.' . $image->getExtensionName();

            // Define new image file name. Example: /Users/John/Sites/esro.dev/uploads/banners/ee8fe038e341c5b1a80cc67ff7818175.png
            $imageFile = $saveDir . $this->image;

            // Define thumbnail image name. Exmaple: /Users/John/Sites/esro.dev/uploads/banners/275x0_ee8fe038e341c5b1a80cc67ff7818175.png
            $thumbFile = $saveDir . self::SIZE_WIDTH . 'x0_' . $this->image;



            // Move banner image from temporary directory to self::IMAGE_DIR
            if( !move_uploaded_file( $image->tempName, $imageFile ) )
            {
                throw new CException( 'Image upload error' );
            }


            // Resize banner image
            $width = self::$positions[$this->position]['width'];
            Yii::app()->image->
                load( $saveDir . $this->image )->
                resize( $width, 0 )->
                save( $imageFile );


            // Resize and save thumbnail.
            Yii::app()->image->
                load( $saveDir . $this->image )->
                resize( self::SIZE_WIDTH, 0 )->
                save( $thumbFile );
        }

        return true;
    }


    public function afterSave()
    {
        if (in_array($this->scenario, array(self::SCENARIO_CREATE, self::SCENARIO_UPDATE)))
        {
            BannerPage::model()->deleteAll('banner_id = ' . $this->id);

            if ($this->pages)
            {
                foreach ($this->pages as $page)
                {
                    $banner_role = new BannerPage;
                    $banner_role->banner_id = $this->id;
                    $banner_role->route = $page;
                    $banner_role->save();
                }
            }
        }

        parent::afterSave();
    }


    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['pages'] = 'Отображается на страницах';
        $labels['date_active'] = 'Активировать по заданной дате';
        return $labels;
    }


    public function getPositionLabel()
    {
        return self::$positions[$this->position]['label'];
    }


    public function getHref()
    {
        if ($this->page)
        {
            return $this->page->href;
        } else
        {
            return $this->url;
        }
    }


    public function afterFind()
    {
        $this->pages = array();
        $selected_pages = BannerPage::model()->findAll("banner_id = {$this->id}");

        foreach ($selected_pages as $page)
            $this->pages[] = $page->route;

        return true;
    }


    public static function getAllPositionLabels()
    {
        $retArray = array();

        foreach( self::$positions as $k => $v )
        {
            $retArray[$k] = $v['label'];
        }

        return $retArray;
    }

}