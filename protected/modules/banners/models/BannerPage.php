<?php

class BannerPage extends ActiveRecordModel
{

    const PAGE_SIZE = 10;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'banners_pages';
    }


    public function name()
    {
        return 'Страницы баннера';
    }


    public function rules()
    {
        return array(
            array('banner_id, route', 'required'),
            array('banner_id', 'length', 'max' => 11),
            array('route', 'length', 'max' => 64),
            array('id, banner_id, route', 'safe', 'on' => 'search'),
        );
    }


    public function relations()
    {
        return array(
            'banner' => array(self::BELONGS_TO, 'Banners', 'banner_id'),
        );
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('banner_id', $this->banner_id, true);
        $criteria->compare('route', $this->role, true);

        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


}