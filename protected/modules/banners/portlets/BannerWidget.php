<?php

class BannerWidget extends Portlet
{
    public $position;

    public function renderContent()
    {

        if (empty($this->position))
        {
            return;
        }


        $criteria = new CDbCriteria();

        $criteria->condition = 'position = :position AND is_active = 1';

        $criteria->params = array(':position' => $this->position);

        $criteria->limit = 1;

        $banner = Banner::model()->find( $criteria );


        if (empty($banner))
        {
            return '';
        }


        if (empty($banner->image) || !file_exists($_SERVER['DOCUMENT_ROOT'] . Banner::IMAGES_DIR . $banner->image))
        {
            return '';
        }

        return $this->render('BannerWidget', array('banner' => $banner));
    }


}