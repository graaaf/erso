<div class="banner">
    <?php if ($banner->url): ?>
        <a href="<?php echo $banner->url; ?>"><img src="<?php echo Banner::IMAGES_DIR; ?>/<?php echo $banner->image; ?>"></a>
    <?php else: ?>
        <img src="<?php echo Banner::IMAGES_DIR; ?>/<?php echo $banner->image; ?>">
    <?php endif ?>
</div>