<?php
$pages = Page::model()->published()->findAll("", array('order' => 'title'));

return array(
    'activeForm' => array(
        'id'                   => 'banner-form',
        'htmlOptions'          => array(
            'enctype' => 'multipart/form-data'
        ),
        'enableAjaxValidation' => false,
    ),
    'elements'   => array(
        'name'        => array('type' => 'text'),
        'page_id'     => array(
            'type'   => 'dropdownlist',
            'prompt' => 'нет',
            'items'  => CHtml::listData($pages, 'id', 'title')
        ),
        'url'         => array(
            'type' => 'text',
            'hint' => 'например: http://website.ru'
        ),
        'image'       => array(
            'type' => 'file',
            'hint' => 'Рекомендованный размер изображения: 275px в ширину'
        ),
        'position'     => array(
            'type'   => 'dropdownlist',
            'items'  => Banner::getAllPositionLabels(),
        ),
        'is_active'   => array('type' => 'checkbox'),
        'src'         => array(
            'type'  => 'hidden',
            'value' => $this->model->render(true)
        )
    ),
    'buttons'    => array(
        'submit' => array(
            'type'  => 'submit',
            'value' => $this->model->isNewRecord ? 'создать' : 'сохранить'
        )
    )
);


