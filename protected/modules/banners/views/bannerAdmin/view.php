<?php
$this->tabs = array(
    'управление банерами' => $this->createUrl('manage'),
    'редактировать' => $this->createUrl('update', array('id' => $model->id))
);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'name'),
        array('name' => 'url'),
        array('name' => 'is_active', 'value' => $model->is_active ? "Да" : "Нет"),
        array('name' => 'position', 'value' => $model->positionLabel),
        array(
            'name' => 'image',
            'value' => $model->render(1),
            'type' => 'raw'
        ),
    ),
));



