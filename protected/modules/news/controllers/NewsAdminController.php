<?php

class NewsAdminController extends AdminController
{

    public static function actionsTitles()
    {
        return array(
            "View" => "Просмотр новости",
            "Create" => "Добавление новости",
            "Update" => "Редактирование новости",
            "Delete" => "Удаление новости",
            "Manage" => "Управление новостями"
        );
    }


    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


    public function actionCreate()
    {
        $model = new News(News::SCENARIO_CREATE);
        $model->date = Yii::app()->dateFormatter->format('y-MM-dd H:mm:ss', time());

        $form = new BaseForm('news.NewsForm', $model);
        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array(
                    'view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('create', array(
            'form' => $form,
        ));
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $params=array('news'=>$model);

        if( Yii::app()->user->role != 'admin' )
        {
            if( !Yii::app()->user->checkAccess('NewsAdmin_UpdateOwnNews',$params) )
            {
                $this->forbidden();
            }
        }

        $model->scenario = News::SCENARIO_UPDATE;
        $form = new BaseForm('news.NewsForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if (isset($_POST['News']['tags']))
            {
                $model->tags = $_POST['News']['tags'];
            }
            else
            {
                $model->tags = array();
            }

            if ($model->save())
            {
                $this->redirect(array(
                    'view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('update', array(
            'form' => $form,
        ));
    }


    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('manage'));
            }
        } else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }


    public function actionManage()
    {
        $model = new News('search');
        $model->unsetAttributes();

        if (isset($_GET[get_class($model)]))
        {
            $model->attributes = $_GET[get_class($model)];
        }

        $this->render('manage', array(
            'model' => $model,
        ));
    }


}