<?php

class TagsAdminController extends AdminController
{

    public static function actionsTitles()
    {
        return array(
            "Create" => "Добавление тега",
            "Update" => "Редактирование тега",
            "Delete" => "Удаление тега",
            "Manage" => "Управление тегами"
        );
    }


    public function actionCreate()
    {
        $model = new Tags();

        $form = new BaseForm('news.TagsForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array('manage'));
            }
        }

        $this->render('create', array(
            'form' => $form,
        ));
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $form = new BaseForm('news.TagsForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array('manage'));
            }
        }

        $this->render('update', array(
            'form' => $form,
        ));
    }


    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('manage'));
            }
        } else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }


    public function actionManage()
    {
        $model = new Tags('search');
        $model->unsetAttributes();

        if (isset($_GET[get_class($model)]))
        {
            $model->attributes = $_GET[get_class($model)];
        }

        $this->render('manage', array(
            'model' => $model,
        ));
    }


}
