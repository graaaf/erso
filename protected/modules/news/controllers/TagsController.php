<?php

Yii::import('application.modules.news.controllers.NewsController');

class TagsController extends NewsController
{

    public static function actionsTitles()
    {
        return array(
            "View" => 'Список новостей с указанным тегом',
        );
    }


    public function actionView($id)
    {
        $criteria = new CDbCriteria;
        $criteria->join = 'JOIN news_tags nt ON t.id=nt.news_id';
        $criteria->addCondition( 'nt.tag_id = :id' );
        $criteria->addCondition( 't.is_published = 1' );
        $criteria->params = array( ':id' => $id );

        $news = News::model()->published()->last()->findAll($criteria);

        $pagination = new CPagination( count($news) );
        $pagination->pageSize=$this->newsPerPage;
        $pagination->applyLimit($criteria);

        $this->render('/news/index', array(
            'news' => $news,
            'pagination' => $pagination,
        ));
    }


}
