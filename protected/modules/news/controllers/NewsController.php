<?php

class NewsController extends BaseController
{

    public $layout = '//layouts/content';

    public $newsPerPage = 3;

    public static function actionsTitles()
    {
        return array(
            "View" => "Просмотр новости",
            "Index" => 'Список новостей',
        );
    }


    public function actionView($id)
    {
        $model = $this->loadModel($id);

        if( (Yii::app()->user->role == 'user') || (Yii::app()->user->isGuest) )
        {
            $model->views ++;
            $model->update('views');
        }

        $this->render('view', array(
            'model' => $model
        ));
    }


    public function actionIndex()
    {
        $newsScopes = News::model()->published()->top()->last();

        $count = $newsScopes->count();

        $pagination = new CPagination($count);
        $pagination->pageSize=$this->newsPerPage;
        $pagination->applyLimit($newsScopes->getDbCriteria());

        $this->render('index', array(
            'news' => $newsScopes->findAll(),
            'pagination' => $pagination,
        ));
    }


}
