<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        'Новости',
    ),
));
?>

<h3>Новости</h3>
<div class = "b-news">
    <?php foreach ($news as $item): ?>
        <section class="b-news_list">
            <?php echo CHtml::link(ImageHelper::thumb(News::IMAGES_DIR, $item->image, null, 141, false, 'class="news_img"'), $item->href); ?>
            <article>
                <?php echo HtmlHelper::timeTag($item->date, 'd MMMM y года, H:mm', array('class'=>'l-date')) ?>
                <nav class = "b-tags">
                    <?php $this->widget('news.widgets.TagsWidget', array(
                        'tagElements' => $item->news_tags,
                        'beforeElement' => CHtml::image('/images/tag_icon.png'),
                    ));?>
                </nav>
                <div class = "g-clear_fix"></div>
                <?php echo CHtml::link($item->title, $item->href, array('class' => 'news_title g-clear_fix')); ?>
                <p>
                    <?php echo TextComponent::reduceContent($item->text, 50); ?>
                </p>
                <span class = "l-p_with_img">
                    <img src = "/images/news_view_icon.png"/>
                    Просмотров: <?php echo $item->views; ?>
                </span>
            </article>
        </section>
    <?php endforeach; ?>
    <div class = "g-clear_fix"></div>
</div><!--end b-news-->

<div class="pager">
    <?php $this->widget('CLinkPager', array(
        'pages' => $pagination,
        'cssFile' => false,
        'nextPageLabel' => CHtml::image('/images/pager_arrow.png') . ' Следующая',
        'prevPageLabel' => 'Предыдущая ' . CHtml::image('/images/pager_arrow_back.png'),
        'header' => '<p>Страницы:</p>',
    )) ?>
</div>