<?php

$this->widget('AdminGrid', array(
    'id' => 'news-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'date',
        array(
            'name' => 'image',
            'type' => 'html',
            'value' => 'ImageHelper::thumb(News::IMAGES_DIR, $data->image, 100, 100)',
        ),
        'title',
        array('name' => 'is_published', 'value' => 'News::$states[$data->is_published]', 'filter' => News::$states),
        array('name' => 'in_main_page', 'value' => 'News::$states[$data->in_main_page]', 'filter' => News::$states),
        'views',
        array(
            'name' => 'author',
            'value' => '$data->author->first_name',
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));