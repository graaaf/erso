<?php
$this->tabs = CMap::mergeArray(array(
            'Редактрование новости' => $this->url('update', array('id' => $model->id)),
                ), $this->tabs);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'title',
        array(
            'name' => 'text',
            'type' => 'html',
            'value' => Yii::app()->text->cut($model->text, 690, "., -:;", " ..."),
        ),
        array(
            'name' => 'is_published',
            'value' => News::$states[$model->is_published],
        ),
        array(
            'name' => 'in_main_page',
            'value' => News::$states[$model->in_main_page],
        ),
    ),
));