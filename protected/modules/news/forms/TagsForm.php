<?php
return array(
    'activeForm' => array(
        'id' => 'tags-form',
        'class' => 'CActiveForm',
    ),
    'elements' => array(
        'name' => array(
            'type' => 'text',
        ),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'value' => $this->model->isNewRecord ? 'Создать' : 'Сохранить'
        )
    )
);
