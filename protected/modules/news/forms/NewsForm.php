<?php
return array(
    'activeForm' => array(
        'id' => 'news-form',
        'class' => 'CActiveForm',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ),
    'elements' => array(
        'title' => array(
            'type' => 'text',
        ),
        'text' => array('type' => 'editor'),
        'source_link' => array(
            'type' => 'text',
            'hint' => 'Пример: http://site.ru',
        ),
        'source_text' => array(
            'type' => 'text',
            'hint' => 'Текст для ссылки-источника',
        ),
        'image' => array('type' => 'file'),
        'tags'       => array(
            'type'  => 'multi_select',
            'items' => $this->model->allTags,
        ),
        'is_published' => array(
            'type' => 'dropdownlist',
            'items' => News::$states
        ),
        'in_main_page' => array('type' => 'checkbox'),
        'date' => array('type' => 'time'),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'value' => $this->model->isNewRecord ? 'Создать' : 'Сохранить'
        )
    )
);
