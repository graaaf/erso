<?php

class NewsTags extends ActiveRecordModel
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'news_tags';
	}


    public function name()
    {
        return 'Теги новости';
    }


	public function rules()
	{
		return array(
			array('news_id, tag_id', 'required'),
			array('news_id, tag_id', 'numerical', 'integerOnly'=>true),
			array('news_id, tag_id', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
            'news' => array(self::BELONGS_TO, 'News', 'news_id'),
            'tag' => array(self::BELONGS_TO, 'Tags', 'tag_id'),
		);
	}


    public function getName()
    {
        return $this->tag->name;
    }


}