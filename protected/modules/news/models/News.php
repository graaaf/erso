<?php

class News extends ActiveRecordModel
{

    const STATE_PUBLISHED = 1;
    const STATE_NOT_PUNLISHED = 0;
    const PAGE_SIZE = 10;

    const IMAGES_DIR = '/upload/news/images';
    const IMAGE_WIDTH = 481;
    const IMAGE_HEIGHT = 306;


    public static $states = array(
        self::STATE_PUBLISHED => 'Да',
        self::STATE_NOT_PUNLISHED => 'Нет',
    );

    public $tags;

    public function name()
    {
        return 'Новости';
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function relations()
    {
        return array(
            'news_tags' => array(self::HAS_MANY, 'NewsTags', 'news_id'),
            'tags' => array(self::HAS_MANY, 'Tags', 'tag_id', 'through' => 'news_tags'),
            'author' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
                    'FileManager' => array('class' => 'application.components.activeRecordBehaviors.AttachmentBehavior', 'attached_model' => 'FileManager'),
                    'sortable' => array('class' => 'ext.sortable.SortableBehavior'),
                ));
    }


    public function tableName()
    {
        return 'news';
    }


    public function rules()
    {
        return array(
            array('title, text', 'required'),
            array('title', 'length', 'max' => 400),
            array('text', 'length', 'max'=>10000),
            array('source_link', 'url'),
            array('in_main_page', 'boolean'),
            array(
                'image',
                'file',
                'types' => 'jpg, jpeg, png, gif',
                'allowEmpty' => true,
                'maxSize' => 1024*1024*10,
            ),
            array('is_published, date, in_main_page, source_text', 'safe'),
        );
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->order = '`in_main_page` DESC, `date` DESC';
        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


    public function beforeSave()
    {
        if (parent::beforeSave())
        {
            if (!$this->date)
            {
                $this->date = date("Y-m-d");
            }

            return true;
        }
    }


    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'published' => array('condition' => $alias . '.is_published = 1'),
            'ordered' => array('order' => $alias . '.`order` DESC'),
            'last' => array('order' => $alias . '.date DESC'),
            'top' => array('condition' => $alias . '.in_main_page = 0'),
            'inMainPage' => array('condition' => $alias . '.in_main_page = 1'),
        );
    }


    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['tags'] = 'Теги';
        return $labels;
    }


    public function afterFind()
    {
        $this->tags = array();
        $selected_tags = NewsTags::model()->findAll("news_id = {$this->id}");

        foreach ($selected_tags as $tag)
            $this->tags[] = $tag->tag_id;

        return true;
    }


    public function afterSave()
    {
        if (in_array($this->scenario, array(self::SCENARIO_CREATE, self::SCENARIO_UPDATE)))
        {
            NewsTags::model()->deleteAll('news_id = ' . $this->id);

            if (!empty($this->tags))
            {
                foreach ($this->tags as $tag)
                {
                    $news_tag = new NewsTags;
                    $news_tag->news_id = $this->id;
                    $news_tag->tag_id = $tag;
                    $news_tag->validate();
                    $news_tag->save();
                }
            }
        }

        parent::afterSave();
    }


    public function beforeDelete()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'news_id=:news_id';
        $criteria->params= array(':news_id' => $this->id);

        if( count(NewsTags::model()->findAll($criteria)) )
        {
            return NewsTags::model()->deleteAll($criteria);
        }

        return parent::beforeDelete();
    }

    public function uploadFiles()
    {
        return array(
            'image' => array(
                'dir' => self::IMAGES_DIR,
            )
        );
    }


    public function getAllTags()
    {
        $tags = Tags::model()->findAll();

        return CHtml::listData($tags, 'id', 'name');
    }


    public function getHref()
    {
        return Yii::app()->controller->createUrl("/news/news/view", array('id' => $this->id));
    }


    public function getSource()
    {
        if( empty($this->source_text) )
        {
            $this->source_text = $this->source_link;
        }

        return CHtml::link( $this->source_text, $this->source_link );
    }

}
