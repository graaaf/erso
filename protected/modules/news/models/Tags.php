<?php

class Tags extends ActiveRecordModel
{
    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'tags';
	}


    public function name()
    {
        return 'Теги';
    }


    public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>50),
			array('id, name', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
            'news_tags' => array(self::HAS_MANY, 'NewsTags', 'tag_id'),
            'news' => array(self::HAS_MANY, 'News', 'news_id', 'through' => 'news_tags'),
        );
	}


    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['id'] = 'ID';
        return $labels;
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);

        return new ActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id',
            ),
        ));
    }


    public function beforeDelete()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'tag_id = :tag_id';
        $criteria->params= array(':tag_id' => $this->id);

        if( count(NewsTags::model()->findAll($criteria)) )
        {
            return NewsTags::model()->deleteAll($criteria);
        }

        return parent::afterDelete();
    }


}