<?php

class NewsModule extends WebModule
{
    public static function name()
    {
        return 'Новости';
    }


    public static function description()
    {
        return 'Новости сайта';
    }


    public static function version()
    {
        return '1.0';
    }


    public function init()
    {
        $this->setImport(array(
            'news.controllers.*',
            'news.models.*',
            'news.components.*',
        ));
    }


    public static function adminMenu()
    {
        return array(
            'Управление новостями' => '/news/newsAdmin/manage',
            'Добавить новость' => '/news/newsAdmin/create',
            'Управление тегами' => '/news/tagsAdmin/manage',
        );
    }


}
