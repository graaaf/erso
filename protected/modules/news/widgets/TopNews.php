<?php


class TopNews extends CWidget
{
    public $newsCount = 5;

    public $viewFile = 'top_news';



    public function init()
    {
    }


    public function run()
    {
        $this->render($this->viewFile, array(
            'news' => News::model()->published()->top()->last()->limit($this->newsCount)->findAll(),
        ));
    }
}