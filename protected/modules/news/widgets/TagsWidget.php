<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Admin
 * Date: 23.09.12
 * Time: 18:20
 * To change this template use File | Settings | File Templates.
 */
class TagsWidget extends CWidget
{
    public $tagElements;

    public $tagLabelAttribute = 'name';

    public $beforeElement = '';

    public function init()
    {}

    public function run()
    {
        $content = '';

        if( count($this->tagElements) )
        {
            if( !empty($this->beforeElement) )
            {
                $content .= $this->beforeElement;
            }

            foreach( $this->tagElements as $ind => $tag)
            {
                $content .= CHtml::link($tag->{$this->tagLabelAttribute},array('/news/tags/view', 'id'=>$tag->tag_id));
                $content .= ($ind != count($this->tagElements)-1) ? ', ' : '';
            }
        }

        echo $content;
    }


}
