<?php

class SlideNews extends CWidget
{
    public $newsCount = 3;

    public $viewFile = 'slide_news';


    public function init()
    {
    }


    public function run()
    {
        $this->render($this->viewFile, array(
            'news' => News::model()->published()->inMainPage()->last()->limit($this->newsCount)->findAll(),
        ));
    }
}