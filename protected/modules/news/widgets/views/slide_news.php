<div id="b-slider" class = "b-main_slider">
    <div class="slides_container">
        <?php foreach ($news as $model): ?>
            <div class="slide">
                <a href="<?php echo $model->href?>" target="_blank">
                    <?php echo ImageHelper::thumb(News::IMAGES_DIR, $model->image, News::IMAGE_WIDTH, News::IMAGE_HEIGHT) ?>
                </a>
                <div class="caption">
                    <section>
                        <a href = "<?php echo $model->href?>" class = "caption_title_link">
                            <?php echo $model->title; ?>
                        </a>
                        <?php echo HtmlHelper::timeTag($model->date, 'd MMMM y года, H:mm', array('class'=>'l-date')) ?>
                    </section>
                        <div class="b-tags">
                            <?php $this->widget('news.widgets.TagsWidget', array(
                                'tagElements' => $model->news_tags,
                                'beforeElement' => CHtml::image('/images/tag_icon.png'),
                            ));?>
                        </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <a class="prev" href="#"><img src = "images/slider_arrow_prev.png" /></a>
    <a class="next" href="#"><img src = "images/slider_arrow_next.png" /></a>
    <ul class = "b-slider_pagination">
        <li class = "h-selected"></li>
        <li></li>
        <li></li>
    </ul>
    <ul class="pagination">
        <?php foreach ($news as $model): ?>
            <li>
                <a href="#">
                    <p><?php echo $model->title; ?></p>
                    <?php echo HtmlHelper::timeTag($model->date, 'd MMMM y года, H:mm', array('class'=>'l-date')) ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>