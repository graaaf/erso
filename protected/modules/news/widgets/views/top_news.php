<div class = "b-main_news">
    <header>
        <h3>Новости</h3>
        <span class="l-view_all">
            <img src="/images/plus_icon.png">
            <?php echo CHtml::link('Архив', array('/news/news/index')); ?>
        </span>
    </header>
    <?php foreach( $news as $model ):?>
        <section <?php echo ($model == end($news)) ? 'class="h-last"' : ''; ?>>
            <a href = "<?php echo $model->href?>" class = "doc_link">
                <?php echo $model->title?>
            </a>
            <?php echo HtmlHelper::timeTag($model->date, 'd MMMM y года, H:mm', array('class'=>'l-date')) ?>
            <nav class="b-tags">
                <?php $this->widget('news.widgets.TagsWidget', array(
                    'tagElements' => $model->news_tags,
                    'beforeElement' => CHtml::image('/images/tag_icon.png'),
                ));?>
            </nav>
        </section>
    <?php endforeach?>
    <div class = "g-clear_fix"></div>
</div>