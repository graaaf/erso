<?php
$this->page_title = "Просмотр Пользователя: {$model->name}";

$this->tabs = array(
    "управление пользователями" => $this->createUrl("manage"),
    "редактировать" => $this->createUrl("update", array("id" => $model->id))
);

$this->widget('DetailView', array(
	'data'=>$model,
	'attributes'=>array(
        'first_name',
        'email',
        array(
            'name'  => 'role',
            'value' => isset($model->role->description) ? $model->role->description : null,
        ),
        'date_create',
        /*
        'last_name',
        'patronymic',
        'birthdate',
        array(
            'name'  => 'gender',
            'value' => User::$gender_list[$model->gender],
        ),
        array(
            'name'  => 'status',
            'value' => User::$status_list[$model->status],
        ),
        'phone',
        */
	),
));
?>
