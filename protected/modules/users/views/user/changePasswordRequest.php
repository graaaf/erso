<?php $this->page_title = 'Восстановление пароля'; ?>

<div class="user_form">
    <?php if (Yii::app()->user->hasFlash('done')): ?>

    <?php echo Yii::t('UsersModule.main', $this->msg(Yii::app()->user->getFlash('done'), 'ok')); ?>

    <?php else: ?>

        <?php if (isset($error)): ?>
            <?php echo $this->msg($error, 'error'); ?>
            <br />
        <?php endif ?>

        <?php echo $form; ?>

    <?php endif ?>
</div>

