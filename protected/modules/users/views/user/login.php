<?php $this->page_title = 'Авторизация'; ?>

<?php if (isset($auth_error)): ?>
    <?php echo $this->msg($auth_error, 'error'); ?>
<?php endif ?>

<div class="user_form">
    <?php echo $form; ?>

    <div>
        <?php echo CHtml::link('Зарегистрироваться', '/users/user/registration'); ?>
        &nbsp;|&nbsp;
        <?php echo CHtml::link('Забыли пароль?', '/users/user/changePasswordRequest'); ?>
    </div>
</div>