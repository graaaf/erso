<?php
$form = include "UserForm.php";

$form['activeForm']['enableAjaxValidation'] = false;

$form['elements'] = array(
    'email'    => $form['elements']['email'],
    'first_name'    => $form['elements']['first_name'],
    'password' => $form['elements']['password'],
    'password_c' => $form['elements']['password_c'],
);

$form['buttons']['submit']['value'] = 'Зарегистрироваться';

return $form;
