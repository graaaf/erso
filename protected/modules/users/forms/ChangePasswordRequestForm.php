<?php

$form = include "UserForm.php";

$form['activeForm']['enableAjaxValidation'] = false;

$form['elements'] = array(
    'email'    => $form['elements']['email']
);

$form['buttons']['submit']['value'] = 'Восстановить';

return $form;