<?php $this->beginContent('//layouts/main'); ?>

<?php
$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink' => CHtml::link('Главная', '/'),
    'separator' => '<span class="divider"></span>',
    'links' => $this->crumbs,
    'htmlOptions' => array(
        'class' => 'breadcrumb'
    )
));
?>
<?php echo $content; ?>

<?php $this->endContent(); ?>