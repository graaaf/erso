<?php $this->crumbs = array('Блог'); ?>

<div class="text reviews">
    <?php foreach ($posts as $post): ?>
        <?php $this->renderPartial('_view', array('model' => $post)); ?>
    <?php endforeach; ?>
</div>

<?php $this->renderPartial('//layouts/_pagination', array(
    'pages' => $pages,
)); ?>