<div class="item">
    <div class="author"><?php echo $model->name; ?></div>
    <div id="comment_<?php echo $model->id; ?>_text">
        <?php echo $model->text; ?>
    </div>
    <a href="javascript:void(0)" class="link_quote" onClick="$.fn.commentForm.quote('<?php echo $model->id; ?>');">цитировать</a>
    <div class="time"><?php echo MDate::timeAgo($model->date_created); ?></div>
    <div class="clear"></div>
</div>