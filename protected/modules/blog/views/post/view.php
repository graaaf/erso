<script type="text/javascript">
  VK.init({apiId: 2778189, onlyWidgets: true});
  $(function() {
      ODKL.init();
      $("#vk_like").css('clear', 'none').css('float', 'right');
  });

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style> /*{float: left; position: static;} */</style>
<?php $this->crumbs = array('Блог' => '/blog', $model->title); ?>

<h1 class="part_title"><?php echo $model->title; ?></h1>
<div class="text reviews">
    <?php echo $model->text; ?>
    <?php
    $this->widget('fileManager.portlets.FileList', array(
        'model' => $model,
        'tag' => 'files'
    ));
    ?>
    <div class="clear"></div>
</div>

<div class="social">
    <div class="fb-like" style="float: left; margin-right: 5px;" data-href="<?php echo $model->href; ?>" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false"></div>
    
    <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    
    

    <a style="float: left; margin-right: 5px;" class="odkl-klass-oc" href="<?php echo $model->href; ?>" onclick="ODKL.Share(this);return false;" ><span>0</span></a>

    <div id="vk_like" style="float: right; width: 180px; background: url(\"http://vk.com/images/upload.gif\") no-repeat scroll center center transparent; height: 22px; position: relative; clear: none;">
        <iframe id="vkwidget1" height="22px" frameborder="0" width="100%" scrolling="no" name="fXDd115d" src="http://vk.com/widget_like.php?app=2829154&width=100%&page=0&url=http%3A%2F%2Fnatali.ru.dev%2Fblog%2F2&type=button&verb=0&title=%D0%9F%D1%80%D0%BE%D1%81%D0%BC%D0%BE%D1%82%D1%80%20%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D0%B8&description=&image=&text=&h=22&13533722e32" style="overflow: hidden; height: 22px; width: 180px; z-index: 150;"></iframe>
    </div>
    <script type="text/javascript">
    //VK.Widgets.Like("vk_like", {type: "button"});
    </script>
</div>
<div class="clear"></div>


<div class="sidebar-right">
    <div class="last_comments">
        <div class="title">Последние комментарии</div>
        <div class="pointer"></div>
        <div class="clear"></div>
        <?php $this->widget('application.modules.blog.portlets.LastComments', array('viewFile' => 'LastComments2')); ?>
    </div>
</div>
<div class="sidebar-left">
    <div class="comments">
        
        <div class="text"><h2>Вы можете оставить комментарий к данной статье</h2></div>
        <?php
        $this->widget('comments.widgets.CommentForm', array(
            'model' => $model,
        ));
        ?>
        <div class="clear"></div>        
        <?php
        $this->widget('comments.widgets.CommentsList', array(
            'model' => $model,
            'viewFile' => 'application.modules.blog.views.post._view_comment',
        ));
        ?>

    </div>
</div>
<div class="clear"></div>