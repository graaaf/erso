<div class="item">
    <div class="reviewer"><h1><?php echo $model->title; ?></h1></div>
    <p>
        <?php echo Yii::app()->text->cut($model->text, 690, "., -:;", " ..."); ?>
    </p>
    <div class="clear"></div>

    <div class="more">
        <a href="<?php echo $model->href; ?>">Читать запись целиком</a>
    </div>

</div>