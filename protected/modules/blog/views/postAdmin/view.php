<?php
$this->tabs = CMap::mergeArray(array(
            'Редактрование записи' => $this->url('update', array('id' => $model->id)),
                ), $this->tabs);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'title',
        array(
            'name' => 'text',
            'type' => 'html',
            'value' => Yii::app()->text->cut($model->text, 690, "., -:;", " ..."),
        ),
        array('name' => 'is_published', 'value' => Post::$states[$model->is_published],),
        
    ),
));