<?php
$this->widget('AdminGrid', array(
    'id' => 'post-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'sortable' => true,
    'columns' => array(
        'title',
        array(
            'name' => 'text',
            'type' => 'html',
            'value' => 'Yii::app()->text->cut($data->text, 190, "., -:;", " ...")',
        ),
        array(
            'name' => 'is_published',
            'value' => 'Portfolio::$states[$data->is_published]',
            'filter' => Portfolio::$states
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
