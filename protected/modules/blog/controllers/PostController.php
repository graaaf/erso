<?php

class PostController extends BaseController
{

    public $layout = '/layouts/content';

    public static function actionsTitles()
    {
        return array(
            'Index' => 'Блог',
            'View' => 'Просмотр записи',
        );
    }
    
    public function actionIndex()
    {
        $model = Post::model()->published()->ordered();
        $criteria = $model->dbCriteria;
        
        $pages = new CPagination($model->count($criteria));
        $pages->pageSize = Post::PAGE_SIZE;
        $pages->applyLimit($criteria);
            
        $posts = Post::model()->findAll($criteria);
        
        $this->render('index', array(
            'posts' => $posts,
            'pages' => $pages,
        ));
    }


    public function actionView()
    {
        if (isset($_GET['id'])) {
            $model = $this->loadModel($_GET['id']);
        }
        elseif (isset($_GET['url'])) {
            $model = $this->loadModel($_GET['url'], array(), 'url');
        }
        
        $this->render('view', array(
            'model' => $model,
        ));
    }

}