<?php

class PostAdminController extends AdminController
{

    public static function actionsTitles()
    {
        return array(
            "View" => "Просмотр записи",
            "Create" => "Добавление записи",
            "Update" => "Редактирование записи",
            "Delete" => "Удаление записи",
            "Manage" => "Управление записями"
        );
    }


    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


    public function actionCreate()
    {
        $model = new Post;
        $form = new BaseForm('blog.PostForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array(
                    'view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('create', array(
            'form' => $form,
        ));
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $form = new BaseForm('blog.PostForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array(
                    'view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('update', array(
            'form' => $form,
        ));
    }


    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }


    public function actionManage()
    {
        $model = new Post('search');
        $model->unsetAttributes();

        if (isset($_GET[get_class($model)]))
        {
            $model->attributes = $_GET[get_class($model)];
        }

        $this->render('manage', array(
            'model' => $model,
        ));
    }


}