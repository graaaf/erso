<?php

class Post extends ActiveRecordModel
{

    const STATE_PUBLISHED = 1;
    const STATE_NOT_PUNLISHED = 0;
    const PAGE_SIZE = 10;

    public static $states = array(
        self::STATE_PUBLISHED => 'Да',
        self::STATE_NOT_PUNLISHED => 'Нет',
    );

    public function name()
    {
        return 'Блог';
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
                    'FileManager' => array('class' => 'application.components.activeRecordBehaviors.AttachmentBehavior', 'attached_model' => 'FileManager'),
                    'sortable' => array('class' => 'ext.sortable.SortableBehavior'),
                ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'posts';
    }


    public function rules()
    {
        return array(
            array('title, text', 'required'),
            array('is_published, url', 'safe'),
            array('title, text, is_published', 'safe', 'on' => 'search'),
        );
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('title', $this->title, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('is_published', $this->is_published, true);
        $criteria->order = '`order` DESC';
        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


    public function relations()
    {
        return array(
            'files' => array(
                self::HAS_MANY,
                'FileManager',
                'object_id',
                'condition' => 'files.model_id = "' . get_class($this) . '" AND files.tag="files"',
                'order' => 'files.order DESC'
            ),
        );
    }


    public function getHref()
    {
        if ($this->url != '')
            return "/blog/{$this->url}";
        return Yii::app()->controller->createAbsoluteUrl("/blog/post/view", array('id' => $this->id));
    }


}
