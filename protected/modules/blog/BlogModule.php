<?php

class BlogModule extends WebModule
{
    public static $active = false;

    public static function name()
    {
        return 'Блог';
    }

    public static function description()
    {
        return 'Блог';
    }

    public static function version()
    {
        return '1.0';
    }

    public function init()
    {
        $this->setImport(array(
            'blog.models.*',
            'blog.components.*',
        ));
    }

    public static function adminMenu()
    {
        return array(
            'Управление записями' => '/blog/postAdmin/manage',
            'Добавить запись' => '/blog/postAdmin/create',
        );
    }

}
