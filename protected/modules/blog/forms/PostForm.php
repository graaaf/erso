<?php
return array(
    'activeForm' => array(
        'id' => 'post-form',
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
        ),
        'enableAjaxValidation' => true,
    ),
    'elements' => array(
        'title' => array('type' => 'text'),
        'url' => array(
            'type'  => 'alias',
            'source'=> 'title'
        ),
        'text' => array('type' => 'editor'),
        'is_published' => array('type' => 'dropdownlist', 'items' => Post::$states),
        'files' => array('type' => 'file_manager', 'data_type' => 'any', 'title' => 'Файлы для скачивания', 'tag' => 'files'),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'value' => $this->model->isNewRecord ? 'создать' : 'сохранить'
        )
    )
);


