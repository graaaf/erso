<?php $this->page_title = 'Управление сообщениями'; ?>

<?php
$this->widget('AdminGrid', array(
    'id' => 'feedback-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'first_name',
        'email',
        array(
            'name' => 'comment',
            'type' => 'html',
            'value' => 'Yii::app()->text->cut($data->comment, 190, "., -:;", " ...")',
        ),
        'date_create',
        array(
            'class' => 'CButtonColumn',
            'template' => '{view} {delete}'
        ),
    ),
));
?>
