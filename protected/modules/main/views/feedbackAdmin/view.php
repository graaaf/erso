<?php
$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'first_name',
        'email',
        'comment',
        'date_create',
    ),
));
