<?php if (Yii::app()->user->hasFlash('feedback_done')): ?>
    
    <div class='message ok' id="feedback" style='display: block;'>
        <p><?php echo Yii::app()->user->getFlash('feedback_done'); ?></p>
    </div>
<?php endif ?>

<?php echo $form; ?>