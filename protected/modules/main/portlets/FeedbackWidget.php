<?php

class FeedbackWidget extends Portlet
{

    public function renderContent()
    {
        $model = new Feedback;
        $form = new BaseForm('main.FeedbackForm', $model);
        
        $this->render('Feedback', array(
            'form' => $form,
        ));
    }


}
