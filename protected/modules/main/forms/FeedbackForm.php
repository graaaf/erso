<?php
return array(
    'action' => '/main/feedback/create',
    'activeForm' => array(
        'id' => 'feedback-form',
        'class' => 'CActiveForm',
        'htmlOptions' => array(
            'class' => 'feedback',
        ),
        'enableAjaxValidation' => true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>false,
            //'beforeValidate'=>"js:beforeValidate",
        ),
    ),
    'elements' => array(
        'first_name' => array('type' => 'text'),
        'email' => array('type' => 'text'),
        'comment' => array('type' => 'textarea'),
        '<dl>
            <dd><label>* - поле, обязательное для заполнения</label></dd>
         </dl>',
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'value' => Yii::t('main', 'Отправить'),
            'id' => 'feedback_button'
        )
    )
);
