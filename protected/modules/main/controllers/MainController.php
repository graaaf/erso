<?php

class MainController extends BaseController
{

    public static function actionsTitles()
    {
        return array(
            "Error" => "Ошибка на странице",
            "Search" => "Поиск по сайту",
            'Redirect' => 'Редирект'
        );
    }


    public function actionSearch($query)
    {
        $models = array(
            "News" => array(
                'attributes' => array("title", "text"),
                'view_path' => 'application.modules.news.views.news._view'
            ),
            "Page" => array(
                'attributes' => array("title", "text"),
                'view_path' => 'application.modules.content.views.page._view'
            ),
        );

        $query = addslashes(strip_tags($query));

        $result = array();

        foreach ($models as $class => $data)
        {
            $criteria = new CDbCriteria;

            foreach ($data['attributes'] as $attribute)
            {
                $criteria->compare($attribute, $query, true, 'OR');
            }

            $model = new $class;

            $items = $model->findAll($criteria);
            if ($items)
            {
                $result[$data['view_path']] = $items;
            }
        }

        $this->render('search', array(
            'result' => $result,
            'query' => $query
        ));
    }


    public function actionRedirect($url)
    {
        $rules = array(
            'news08.php' => '/news/10',
            'news06.php' => '/news/11',
            'news09.php' => '/news/12',
            'news05.php' => '/news/13',
            'news13.php' => '/news/3',
            'news12.php' => '/news/4',
            'news11.php' => '/news/5',
            'news01.php' => '/news/6',
            'news.php' => '/news',
            'news02.php' => '/news/7',
            'news03.php' => '/news/8',
            'news04.php' => '/news/9',
            
            'about.php'  => '/ob-agentstve',
        );
        
        if (isset($rules[$url])) {
            $to = $rules[$url];
        }
        else {
            $to = '/';
        }
        
        var_dump($to);
    }


    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error)
        {
            if (Yii::app()->request->isAjaxRequest)
            {
                echo $error['message'];
            } else
            {
                $this->render('error', $error);
            }
        }
    }


}
