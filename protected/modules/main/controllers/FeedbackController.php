<?php

class FeedbackController extends BaseController
{   
    public static function actionsTitles() 
    {
        return array(
            "Create" => "Добавление сообщения"
        );
    }
    

    public function actionCreate()
    {   
        $model = new Feedback;
        $form  = new BaseForm('main.FeedbackForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            $model = $form->model;
            if ($model->save())
            {
                Yii::app()->user->setFlash('feedback_done', Yii::t('MainModule.main', 'Сообщение успешно отправлено!'));
                $email = Setting::model()->getValue('feedback_email');
                $text = "Вам было отправлено новое сообщение из формы обратной связи.<br/>
Пользователем: [{$model->first_name}], [{$model->email}]<br/>
[<a href='http://natali-art.ru/main/feedbackAdmin/view/id/{$model->id}'>Cмотреть подробнее</a>]<br/>_______________________<br/>Данное письмо было сгенерировано службой автоматической рассылки. Просим Вас не отвечать на него. Если у Вас возник вопрос по работе с сайтом, просим Вас обращаться по адресу: [" . Setting::getValue('feedback2') . "]";

                $title = 'Новое сообщение на сайте natali-art.ru';
                Yii::app()->getModule('mailer')->sendMail($email, $title, $text, 'Natali-art.ru Informer');
                $this->redirect($_SERVER['HTTP_REFERER']."#feedback");
            }
        }

        $this->render("create", array('form' => $form));
    }
}
