<?php
return array(
    'activeForm' => array(
        'id' => 'service-form',
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
        ),
        'enableAjaxValidation' => true,
    ),
    'elements' => array(
        'meta_tags'    => array('type' => 'MetaTags'),
        'names' => array('type' => 'text'),
        'url'          => array(
            'type'  => 'alias',
            'source'=> 'names'
        ),
        'description' => array('type' => 'editor'),
        'image' => array('type' => 'file'),
        'date' => array('type' => 'date'),
        'is_published' => array('type' => 'dropdownlist', 'items' => Article::$states),
        'files' => array('type' => 'file_manager', 'data_type' => 'any', 'title' => 'Файлы для скачивания', 'tag' => 'files'),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'value' => $this->model->isNewRecord ? 'создать' : 'сохранить'
        )
    )
);


