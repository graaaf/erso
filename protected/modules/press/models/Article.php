<?php

class Article extends ActiveRecordModel
{

    const UPLOAD_DIR = 'upload/press/images';
    const STATE_PUBLISHED = 1;
    const STATE_NOT_PUNLISHED = 0;
    const IMAGE_WIDTH = 695;
    const IMAGE_HEIGHT = 400;
    const PAGE_SIZE = 10;

    public static $states = array(
        self::STATE_PUBLISHED => 'Да',
        self::STATE_NOT_PUNLISHED => 'Нет',
    );

    public function name()
    {
        return 'Пресса';
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
                    'FileManager' => array('class' => 'application.components.activeRecordBehaviors.AttachmentBehavior', 'attached_model' => 'FileManager'),
                    'sortable' => array('class' => 'ext.sortable.SortableBehavior'),
                    'MetaTag' => array('class' => 'application.components.activeRecordBehaviors.MetaTagBehavior'),
                ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'press';
    }


    public function rules()
    {
        return array(
            array('names, description', 'required'),
            array('is_published, date, url', 'safe'),
            array('image', 'file', 'types' => 'jpg, jpeg, png, gif', 'allowEmpty' => false, 'on' => 'create'),
            array('image', 'file', 'types' => 'jpg, jpeg, png, gif', 'allowEmpty' => true, 'on' => 'update'),
        );
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->order = '`order` DESC';
        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


    public function relations()
    {
        return array(
            'files' => array(
                self::HAS_MANY,
                'FileManager',
                'object_id',
                'condition' => 'files.model_id = "' . get_class($this) . '" AND files.tag="files"',
                'order' => 'files.order DESC'
            ),
        );
    }


    public function uploadFiles()
    {
        return array(
            'image' => array(
                'dir' => self::UPLOAD_DIR
            )
        );
    }


    public function getHref()
    {
        if ($this->url != '')
            return "/press/{$this->url}";
            
        return Yii::app()->controller->createUrl("/press/article/view", array('id' => $this->id));
    }


}
