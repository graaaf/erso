<?php

class ArticleController extends BaseController
{

    public $layout = '/layouts/content';

    public static function actionsTitles()
    {
        return array(
            'Index' => 'Пресса',
            'View' => 'Просмотр отзыва',
        );
    }


    public function actionIndex()
    {
        $model = Article::model()->published()->ordered();
        $criteria = $model->dbCriteria;
        
        $pages = new CPagination($model->count($criteria));
        $pages->pageSize = Article::PAGE_SIZE;
        $pages->applyLimit($criteria);
            
        $articles = Article::model()->findAll($criteria);

        $this->render('index', array(
            'articles' => $articles,
            'pages' => $pages,
        ));
    }
    
    public function actionView() {
        if (isset($_GET['id'])) {
            $model = $this->loadModel($_GET['id']);
        }
        elseif (isset($_GET['url'])) {
            $model = $this->loadModel($_GET['url'], array(), 'url');
        }
        
        $this->render('view', array(
            'model' => $model,
        ));
    }


}