<?php

class PressModule extends WebModule
{
    public static $active = false;

    public static function name()
    {
        return 'Пресса';
    }

    public static function description()
    {
        return 'Пресса';
    }

    public static function version()
    {
        return '1.0';
    }

    public function init()
    {
        $this->setImport(array(
            'press.models.*',
            'press.components.*',
        ));
    }

    public static function adminMenu()
    {
        return array(
            'Управление прессой' => '/press/articleAdmin/manage',
            'Добавить публикацию' => '/press/articleAdmin/create',
        );
    }

}
