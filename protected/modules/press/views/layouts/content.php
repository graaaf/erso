<?php $this->beginContent('//layouts/main'); ?>
<div class="sidebar-right">
    <?php $this->widget('application.modules.banners.portlets.BannersList'); ?>
    <div class="part_title">Отправить заявку на свадьбу</div>
    <?php $this->widget('application.modules.main.portlets.FeedbackWidget'); ?>
</div>

<div class="sidebar-left">
    <?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
        'homeLink' => CHtml::link('Главная', '/'),
        'separator' => '<span class="divider"></span>',
        'links' => $this->crumbs,
        'htmlOptions' => array(
            'class' => 'breadcrumb'
        )
    ));
    ?>
    <?php echo $content; ?>
</div>
<?php $this->endContent(); ?>