<div class="item">
    <div class="name"><?php echo $model->names; ?></div>
    <span class="date"><?php echo $model->date; ?></span>
    <img src="/<?php echo Article::UPLOAD_DIR . '/' . $model->image; ?>" style="max-width: <?php echo Article::IMAGE_WIDTH; ?>px;">
    <p>
        <?php echo Yii::app()->text->cut($model->description, 690, "., -:;", " ..."); ?>
    </p>
    <div class="clear"></div>

    <div class="more">
        <a href="<?php echo $model->href; ?>">Смотреть публикацию полностью</a>
    </div>

</div>