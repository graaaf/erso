<?php $this->crumbs = array('Пресса' => '/press', $model->names); ?>
<div class="text press">
    <div class="item">
        <div class="name"><?php echo $model->names; ?></div>
        <span class="date"><?php echo $model->date; ?></span>
        <img src="/<?php echo Article::UPLOAD_DIR . '/' . $model->image; ?>" style="max-width: <?php echo Article::IMAGE_WIDTH; ?>px;">
        <p>
            <?php echo $model->description; ?>
        </p>
        <div class="clear"></div>
    </div>
</div>