<?php $this->crumbs = array('Пресса'); ?>

<div class="text press">
    <?php foreach ($articles as $article): ?>
        <?php $this->renderPartial('_view', array('model' => $article)); ?>
    <?php endforeach; ?>
</div>

<?php $this->renderPartial('//layouts/_pagination', array(
    'pages' => $pages,
)); ?>