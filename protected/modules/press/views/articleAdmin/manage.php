<?php
$this->tabs = array('Добавить публикацию' => '/press/articleAdmin/create');
$this->widget('AdminGrid', array(
    'id' => 'service-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'sortable' => true,
    'columns' => array(
        'names',
        array(
            'name' => 'description',
            'type' => 'html',
            'value' => 'Yii::app()->text->cut($data->description, 190, "., -:;", " ...")',
        ),
        array(
            'name' => 'image',
            'type' => 'html',
            'value' => 'ImageHelper::thumb(Portfolio::UPLOAD_DIR, $data->image, 100, 100)',
        ),
        'date',
        array('name' => 'is_published', 'value' => 'Service::$states[$data->is_published]', 'filter' => Service::$states),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
