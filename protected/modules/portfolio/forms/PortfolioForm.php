<?php
return array(
    'activeForm' => array(
        'id' => 'service-form',
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
        ),
        'enableAjaxValidation' => true,
    ),
    'elements' => array(
        'meta_tags'    => array('type' => 'MetaTags'),
        'names' => array('type' => 'text'),
        'name' => array('type' => 'text'),
        'url' => array(
            'type'  => 'alias',
            'source'=> 'name'
        ),
        'image' => array('type' => 'file', 'hint' => 'Рекомендованный размер для загрузки изображения: 456x284 px'),
        'description' => array('type' => 'editor'),
        'is_published' => array('type' => 'dropdownlist', 'items' => Service::$states),
        'review' => array('type' => 'dropdownlist', 'items' => Portfolio::getReviewItems()),
        '<span style="color: gray;">Рекомендуемый размер для загрузки фотографий не менее 650px x 433px</span>',
        'images' => array('type' => 'file_manager', 'data_type' => 'image', 'title' => 'Изображения', 'tag' => 'images', 'hint' => 'Рекомендованный размер для загрузки изображения: 456x284 px'),
        'files' => array('type' => 'file_manager', 'data_type' => 'any', 'title' => 'Файлы для скачивания', 'tag' => 'files'),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'value' => $this->model->isNewRecord ? 'создать' : 'сохранить'
        )
    )
);


