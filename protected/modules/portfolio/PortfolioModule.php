<?php

class PortfolioModule extends WebModule
{
    public static $active = false;

    public static function name()
    {
        return 'Портфолио';
    }

    public static function description()
    {
        return 'Портфолио свадеб';
    }

    public static function version()
    {
        return '1.0';
    }

    public function init()
    {
        $this->setImport(array(
            'portfolio.models.*',
            'portfolio.components.*',
        ));
    }

    public static function adminMenu()
    {
        return array(
            'Управление работами' => '/portfolio/portfolioAdmin/manage',
            'Добавить работу' => '/portfolio/portfolioAdmin/create',
        );
    }

}
