<?php
$this->tabs = CMap::mergeArray(array(
            'Редактрование работы' => $this->url('update', array('id' => $model->id)),
                ), $this->tabs);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'names',
        array(
            'name' => 'description',
            'type' => 'html',
            'value' => Yii::app()->text->cut($model->description, 690, "., -:;", " ..."),
        ),
        array(
            'name' => 'image',
            'type' => 'html',
            'value' => ImageHelper::thumb(Portfolio::UPLOAD_DIR, $model->image, Portfolio::GALLERY_SMALL_WIDTH, Portfolio::GALLERY_SMALL_HEIGHT),
        ),
        array('name' => 'is_published', 'value' => Service::$states[$model->is_published],),
        
    ),
));