<?php $this->crumbs = array('Портфолио' => '/portfolio', $model->name); ?>

<h1 class="part_title"><?php echo $model->name; ?></h1>

<?php 
if (FileManager::model()->findAll("model_id='Portfolio' AND object_id={$model->id} AND tag='images'") != null) {
$this->widget('portfolio.widgets.ImagesGallery', array(
    'model' => $model,
    )); 
}?>

<div class="text reviews">
    <?php echo $model->description; ?>
    
     <?php
    $this->widget('fileManager.portlets.FileList', array(
        'model' => $model,
        'tag' => 'files'
    ));
    ?>
    
    <div class="clear"></div>
    
    <?php if ($model->review): ?>
        <div class="more right">
            <a href="<?php echo Review::model()->findByPk($model->review)->href; ?>">Прочитать отзыв о проведении свадьбы</a>
        </div>
    <?php endif; ?>
   
    <div class="clear"></div>
</div>