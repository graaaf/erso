<li style="float: left; ">
    <a class="thumb" href="/<?php echo $image->path . '/' . $image->name; ?>" data-big-img="/<?php echo $image->path . '/' . $image->name ?>">
        <?php echo ImageHelper::thumb($image->path, $image->name, Portfolio::GALLERY_SMALL_WIDTH, Portfolio::GALLERY_SMALL_HEIGHT, true, 'style=""', true) ?>
    </a>
</li>