<a href="<?php echo $model->href; ?>" class="item<?php if ($even == true) echo ' last'; ?>">
    <span class="shadow"></span>
    <?php echo ImageHelper::thumb(Portfolio::UPLOAD_DIR, $model->image, Portfolio::IMAGE_WIDTH, Portfolio::IMAGE_HEIGHT); ?>
    <span class="description">
        <span class="name"><?php echo $model->names; ?></span>
        <b>"<?php echo $model->name; ?>"</b>
    </span>
</a>