<?php $this->crumbs = array('Портфолио'); ?>

<div class="portfolio">
    <?php
    $even = false;
    foreach ($portfolio as $item)
    {
        $this->renderPartial('_view', array('model' => $item, 'even' => $even));
        $even = ($even == true) ? false : true;
    }
    ?>

    <div class="clear"></div>
    <?php
    $this->renderPartial('//layouts/_pagination', array(
        'pages' => $pages,
    ));
    ?>
</div>