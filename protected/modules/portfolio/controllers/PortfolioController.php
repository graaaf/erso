<?php

class PortfolioController extends BaseController
{

    public $layout = '/layouts/content';


    public static function actionsTitles()
    {
        return array(
            'Index' => 'Список работ',
            'View'  => 'Просмотр работы'
        );
    }


    public function actionIndex()
    {
        $model = Portfolio::model()->published()->ordered();
        $criteria = $model->dbCriteria;
        
        $pages = new CPagination($model->count($criteria));
        $pages->pageSize = Portfolio::PAGE_SIZE;
        $pages->applyLimit($criteria);
            
        $portfolio = Portfolio::model()->findAll($criteria);
        
        $this->render('index', array(
            'portfolio' => $portfolio,
            'pages' => $pages,
        ));
    }


    public function actionView()
    {
        if (isset($_GET['id'])) {
            $model = $this->loadModel($_GET['id']);
        }
        elseif (isset($_GET['url'])) {
            $model = $this->loadModel($_GET['url'], array(), 'url');
        }
        
        $this->render('view', array(
            'model' => $model,
        ));
    }

}