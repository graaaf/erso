<?php

class PortfolioAdminController extends AdminController
{

    public static function actionsTitles()
    {
        return array(
            "View" => "Просмотр работы",
            "Create" => "Добавление работы",
            "Update" => "Редактирование работы",
            "Delete" => "Удаление работы",
            "Manage" => "Управление работами"
        );
    }


    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


    public function actionCreate()
    {
        $model = new Portfolio('create');
        $form = new BaseForm('portfolio.PortfolioForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array(
                    'view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('create', array(
            'form' => $form,
        ));
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $form = new BaseForm('portfolio.PortfolioForm', $model);

        $this->performAjaxValidation($model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array(
                    'view',
                    'id' => $model->id
                ));
            }
        }

        $this->render('update', array(
            'form' => $form,
        ));
    }


    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }


    public function actionManage()
    {
        $model = new Portfolio('search');
        $model->unsetAttributes();

        if (isset($_GET[get_class($model)]))
        {
            $model->attributes = $_GET[get_class($model)];
        }

        $this->render('manage', array(
            'model' => $model,
        ));
    }


}