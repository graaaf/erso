<div class="galery">
    <div class="photo_small">
        <ul>
            <?php
            $even = false;
            foreach ($works as $work):
                ?>
                <li<?php if ($even == true) echo ' class="last"'; ?>>
                    <a href="<?php echo $work->href; ?>" class="shadow">
                        <?php echo ImageHelper::thumb(Portfolio::UPLOAD_DIR, $work->image, Portfolio::GALLERY_SMALL_WIDTH, Portfolio::GALLERY_SMALL_HEIGHT) ?>
                    </a>
                </li>
                <?php
                $even = ($even == true) ? false : true;
            endforeach;
            ?>

        </ul>
    </div>
    <div class="clear"></div>
</div>