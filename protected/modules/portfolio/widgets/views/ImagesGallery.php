<script type="text/javascript">

    $(document).ready(function($) {
        var onMouseOutOpacity = 0.65;
        $('.photo_small ul.thumbs li').opacityrollover({
            mouseOutOpacity:   onMouseOutOpacity,
            mouseOverOpacity:  1.0,
            fadeSpeed:         'fast',
            exemptionSelector: '.selected'
        });

        var gallery = $('.photo_small').galleriffic({
            delay:                     2500,
            numThumbs:                 10,
            preloadAhead:              10,
            enableTopPager:            true,
            enableBottomPager:         true,
            maxPagesToShow:            7,
            imageContainerSel:         '.photo_big',
            controlsContainerSel:      '#controls',
            captionContainerSel:       '#caption',
            loadingContainerSel:       '#loading',
            renderSSControls:          true,
            renderNavControls:         true,
            playLinkText:              'Play Slideshow',
            pauseLinkText:             'Pause Slideshow',
            prevLinkText:              '&lsaquo; Previous Photo',
            nextLinkText:              'Next Photo &rsaquo;',
            nextPageLinkText:          'Next &rsaquo;',
            prevPageLinkText:          '&lsaquo; Prev',
            enableHistory:             true,
            autoStart:                 false,
            syncTransitions:           true,
            defaultTransitionDuration: 900,
            onSlideChange:             function(prevIndex, nextIndex) {
                this.find('ul.thumbs').children()
                .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                .eq(nextIndex).fadeTo('fast', 1.0);
            },
            onPageTransitionOut:       function(callback) {
                this.fadeTo('fast', 0.0, callback);
            },
            onPageTransitionIn:        function() {
                this.fadeTo('fast', 1.0);
            },
            onPageTransitionIn:        function() {
                var prevPageLink = this.find('.prev').css('visibility', 'hidden');
                var nextPageLink = this.find('.next').css('visibility', 'hidden');

                if (this.displayedPage > 0)
                    prevPageLink.css('visibility', 'visible');

                var lastPage = this.getNumPages() - 1;
                if (this.displayedPage < lastPage)
                    nextPageLink.css('visibility', 'visible');

                this.fadeTo('fast', 1.0);
            }

            
        });
        gallery.find('.prev').click(function(e) {
            gallery.previousPage();
            e.preventDefault();
        });

        gallery.find('.next').click(function(e) {
            gallery.nextPage();
            e.preventDefault();
        });

    });
</script>
<div style="overflow: hidden; height: 478px; padding-top: 20px;">
<div class="galery">
    <div class="photo_small">
        <div class="arrow prev"></div>
        <div class="arrow next"></div>

        <ul class="thumbs" style="height: 462px; overflow: hidden;">
            <?php
            foreach ($images as $image)
            {
                $this->render('portfolio.views.portfolio.image_view', array(
                    'image' => $image,
                ));
            }
            ?>
        </ul>
    </div>

    <div class="photo_big"></div>
    <div class="clear"></div>
</div>
</div>