<?php

class LastWorks extends Portlet
{

    public $itemsCount = 10;

    public function renderContent()
    {
        $works = Portfolio::model()->published()->ordered()->limit($this->itemsCount)->findAll();

        $this->render('LastWorks', array(
            'works' => $works,
        ));
    }


}