<?php

class ImagesGallery extends Portlet
{

    public $model;
    public $assets;

    public function init()
    {
        parent::init();
        $this->registerScripts();
    }


    public function registerScripts()
    {
        $this->assets = Yii::app()->getModule('portfolio')->assetsUrl();
        Yii::app()->clientScript
                ->registerScriptFile($this->assets . '/js/jquery.galleriffic.js')
                ->registerScriptFile($this->assets . '/js/jquery.opacityrollover.js');
    }


    public function renderContent()
    {
        $this->render('ImagesGallery', array(
            'images' => FileManager::model()->findAll("model_id='Portfolio' AND object_id={$this->model->id} AND tag='images' ORDER BY `order` DESC"),
        ));
    }


}