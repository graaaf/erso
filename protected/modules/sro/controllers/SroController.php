<?php

class SroController extends BaseController
{

    public $layout = '//layouts/content';

    public $sroPerPage = 3;

    public static function actionsTitles()
    {
        return array(
            "View" => "Просмотр СРО",
            "Index" => 'Список СРО',
        );
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);

        if( (Yii::app()->user->role == 'user') || (Yii::app()->user->isGuest) )
        {
            $model->views ++;
            $model->update('views');
        }

        $this->render('view', array(
            'model' => $model
        ));
    }

    public function actionIndex()
    {
        $model = new Sro('search');

        $model->unsetAttributes();
        if (isset($_GET['search']))
        {
            $model->setAttributes($_GET['search']);
        }

        $regions = CHtml::listData(SroRegion::model()->findAll(), 'id', 'name');
        $cities = CHtml::listData(SroCity::model()->findAll(), 'id', 'name');

        $this->render('index', array(
            'model' => $model,
            'regions' => $regions,
            'cities' => $cities
        ));
    }

    public function sortLink($label, $attr)
    {
        $order = $_GET;
        $order["Sro_order"] = $attr;
        $order["Sro_order_by"] = (isset($_GET['Sro_order_by']) && $_GET['Sro_order_by'] == 'asc') ? 'desc' : 'asc';
        return CHtml::link($label, $this->createUrl('/sro/sro/index', $order));
    }

}
