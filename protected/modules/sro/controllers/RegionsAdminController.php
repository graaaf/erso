<?php

class RegionsAdminController extends AdminController
{
    public static function actionsTitles()
    {
        return array(
            "View"   => "Просмотр региона",
            "Create" => "Добавление региона",
            "Update" => "Редактирование региона",
            "Delete" => "Удаление региона",
            "Manage" => "Управление регионами",
        );
    }

	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}


	public function actionCreate()
	{
		$model = new SroRegion();

		$form = new BaseForm('sro.RegionForm', $model);

		// $this->performAjaxValidation($model);

		if(isset($_POST['SroRegion']))
		{
			$model->attributes = $_POST['SroRegion'];
			if($model->save())
            {
                $this->redirect(array('view', 'id' => $model->id));
            }
		}

		$this->render('create', array(
			'form' => $form,
		));
	}


	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$form = new BaseForm('sro.RegionForm', $model);

		// $this->performAjaxValidation($model);

		if(isset($_POST['SroRegion']))
		{
			$model->attributes = $_POST['SroRegion'];
			if($model->save())
            {
                $this->redirect(array('view', 'id'=>$model->id));
            }
		}

		$this->render('update', array(
			'form' => $form,
		));
	}


	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel($id)->delete();

			if(!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
		}
		else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
	}



	public function actionManage()
	{
		$model = new SroRegion('search');
		$model->unsetAttributes();
		if(isset($_GET['SroRegion']))
        {
            $model->attributes = $_GET['SroRegion'];
        }

		$this->render('manage', array(
			'model' => $model,
		));
	}


	public function loadModel($id)
	{
		$model = SroRegion::model()->findByPk((int) $id);
		if($model === null)
        {
            $this->pageNotFound();
        }

		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'Region-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
