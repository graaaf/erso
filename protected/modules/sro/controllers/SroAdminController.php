<?php

class SroAdminController extends AdminController
{
    public static function actionsTitles()
    {
        return array(
            "View"   => "Просмотр СРО",
            "Create" => "Добавление СРО",
            "Update" => "Редактирование СРО",
            "Delete" => "Удаление СРО",
            "Manage" => "Управление СРО",
        );
    }

	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model = new Sro();

		$form = new BaseForm('sro.SroForm', $model);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Sro']))
		{
			$model->attributes = $_POST['Sro'];
			if($model->save())
            {
                $this->redirect(array('view', 'id' => $model->id));
            }
		}

		$this->render('create', array(
			'form' => $form,
		));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$form = new BaseForm('sro.SroForm', $model);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Sro']))
		{
			$model->attributes = $_POST['Sro'];
			if($model->save())
            {
                $this->redirect(array('view', 'id'=>$model->id));
            }
		}

		$this->render('update', array(
			'form' => $form,
		));
	}


	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel($id)->delete();

			if(!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
		}
		else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
	}



	public function actionManage()
	{
		$model = new Sro('search');
		$model->unsetAttributes();
		if(isset($_GET['Sro']))
        {
            $model->attributes = $_GET['Sro'];
        }

		$this->render('manage', array(
			'model' => $model,
		));
	}


	public function loadModel($id, $scopes = array(), $attribute = null)
	{
		$model = Sro::model()->findByPk((int) $id);
		if($model === null)
        {
            $this->pageNotFound();
        }

		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'Sro-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
