<?php
$this->page_title = 'Управление СРО';

$this->tabs = array(
    "добавить СРО" => $this->createUrl("create")
);

$this->widget('AdminGrid', array(
	'id'=>'regions-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'sortable' => true,
	'columns'=>array(
        'register_number',
		'name',
        'members_count',
        'rating',
        'register_date',
        array(
			'class'=>'CButtonColumn',
		),
	),
));
?>
