<?php
$this->page_title = 'Просмотр';

$this->tabs = array(
    "Управление СРО"  => $this->createUrl('manage'),
    "редактировать"        => $this->createUrl('update', array('id' => $model->id))
);

$logo = $model->getFileUrl();
if ($logo)
{
    $logo = '<img src="' . $logo . '"></img>';
}

$this->widget('DetailView', array(
	'data' => $model,
	'attributes' => array(
        'category',
		'name',
        'short_name',
        'register_full_number',
        'register_date',
        'city',
        'members_count',
        'members_count_date',
        'rating',
        'site',
        'email',
        'phone1',
        'phone2',
        'phone3',
        'fax',
        array(
            'name' => 'logo',
            'type' => 'html',
            'value' => $logo,
        ),
	),
));
?>
