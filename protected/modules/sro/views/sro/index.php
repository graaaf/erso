<?php

Yii::app()->clientScript->registerScriptFile(Yii::app()->getModule('sro')->assetsUrl() . '/js/index.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->getModule('sro')->assetsUrl() . '/css/sro.css');
// Yii::app()->clientScript->registerScriptFile('/js/index.js');
// Yii::app()->clientScript->registerCssFile('/css/sro.css');

$this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink' => CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        'Реестр СРО',
    ),
));
?>

<h3>Реестр СРО</h3>
<div class="b-search">
    <h3>Поиск СРО</h3>

    <div class="g-clear_fix"></div>


    <form class="search_form" name="search" method="get">
        <span class="search_span">
            <input class="search_input" type="text" name="search[short_name]"
                   value="<?php echo CHtml::encode($model->short_name); ?>"
                   placeholder="Введите ключевое слово для поиска СРО..."/>
            <button class="l-button">Найти</button>
        </span>
<!--		<span class="l-view_all">-->
<!--		    <img src="images/plus_icon.png">-->
<!--			<a id="toggle-search" href="#">Расширенный поиск</a>-->
<!--		</span>-->
    </form>
    <div class="g-clear_fix"></div>
    <div class="search_filters">
        <form class="jClever" method="get">
            <div class="row b-search_filter1">
                <label>Наименование СРО</label>
                <input type="text" name="search[short_name]"
                       value="<?php echo CHtml::encode($model->short_name); ?>"
                       placeholder="Саморегулируемая организация Некоммерческое..."
                        />
            </div>
            <div class="row b-search_filter2">
                <label>Номер в Реестре</label>
                <input type="text" name="search[register_number]"
                       value="<?php echo CHtml::encode($model->register_number); ?>"
                       placeholder=""/>
            </div>
            <div class="g-clear_fix b-search_filters_sep"></div>
            <div class="row b-search_filter3">
                <label>Число членов</label>
                <input type="text" name="search[members_count_min]"
                       value="<?php echo CHtml::encode($model->members_count_min); ?>"
                       placeholder=""/>
                <span>по</span>
                <input type="text" name="" value="" placeholder=""/>
            </div>
            <div class="row b-search_filter4">
                <label>Федеральный регион / округ</label>
                <?php
                echo CHtml::dropDownList('search[region_id]',
                    CHtml::encode($model->region_id),
                    $regions,
                    array('empty' => '')
                );
                ?>
            </div>
            <div class="row b-search_filter5">
                <label>Город</label>
                <?php
                echo CHtml::dropDownList('search[city_id]',
                    CHtml::encode($model->city_id),
                    $cities,
                    array('empty' => '')
                );
                ?>
            </div>
            <div class="row b-search_filter6">
                <button class="l-button">Найти</button>
            </div>
            <div class="g-clear_fix"></div>
        </form>
    </div>
</div>

<div class="g-clear_fix"></div>
<div class="grid-view b-sort">
    <div class="b-sort_index">
        <span class="sort_index_word">Сортировать по: </span>

        <span class="l-page_registr">
            <?php echo $this->sortLink('Рейтингу', 'rating'); ?>
            <img src="images/sort_arrow.png"/>
	    </span>
        <span class="l-page_registr">
            <?php echo $this->sortLink('Количеству членов', 'members_count'); ?>
            <img src="images/sort_arrow.png">
        </span>
        <span class="l-page_registr">
            <?php echo $this->sortLink('Популярности', 'views'); ?>
            <img src="images/sort_arrow.png">
        </span>
        <span class="l-page_registr">
            <?php echo $this->sortLink('Номеру в реестре Ростехнадзора', 'register_full_number'); ?>
            <img src="images/sort_arrow.png">
        </span>
    </div>

    <table id="sro-sort_table" class='sort_table' cellpadding='0' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <td>
                №
            </td>
            <td>
                Номер в <br/> Реестре
            </td>
            <td>
                Наименование СРО
            </td>
            <td>
                Дата включения <br/>в реестр РТН
            </td>
            <td>
                Количество <br/> членов
            </td>
            <td>
                Город
            </td>
        </tr>
        </thead>
        <tbody>

        <?php $dataProvider = $model->search(); ?>

        <?php
        $data = $dataProvider->getData();
        $offset = $dataProvider->getPagination()->offset;
        ?>
        <?php foreach ($data as $item): ?>
        <tr>
            <td>
                <?php echo CHtml::link(++$offset, $item->href); ?>
            </td>
            <td>
                <?php echo CHtml::link(CHtml::encode($item->register_full_number), $item->href); ?>
            </td>
            <td>
                <?php echo CHtml::link(CHtml::encode($item->short_name), $item->href); ?>
            </td>
            <td>
                <?php echo CHtml::link(CHtml::encode($item->register_date_formatted()), $item->href); ?>
            </td>
            <td>
                <?php echo CHtml::link(CHtml::encode($item->members_count), $item->href); ?>
            </td>
            <td>
                <?php echo CHtml::link(CHtml::encode($item->city), $item->href); ?>
            </td>
        </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
    <div class="g-clear_fix"></div>

    <div class="pager">
        <?php $this->widget('CLinkPager', array(
        'pages' => $dataProvider->getPagination(),
        'cssFile' => false,
        'nextPageLabel' => CHtml::image('/images/pager_arrow.png') . 'Следующая',
        'prevPageLabel' => 'Предыдущая ' . CHtml::image('/images/pager_arrow_back.png'),
        'header' => '<p>Страницы:</p>',
    )); ?>
    </div>

</div>