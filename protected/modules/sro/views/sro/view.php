<?php

$this->page_title = $model->name;

$this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        $model->name,
    ),
));
?>

<h3><?php echo CHtml::encode($model->name); ?></h3>
<div class="b-reestr_view">
    <section class="b-reestr_info">
        <?php echo $model->logoAsTag; ?>
        <article>
            <p>
                Номер в реестре: <b><?php echo CHtml::encode($model->register_full_number); ?></b>
            </p>
            <p>
                Дата регистрации в РТН: <b><?php echo CHtml::encode($model->register_date); ?></b>
            </p>
            <p>
                Округ: <a href=""><?php echo CHtml::encode($model->city->region->name); ?></a>
            </p>
            <p>
                Регион: <a href=""><?php echo CHtml::encode($model->city->name); ?></a>
            </p>
            <p>
                Город: <a href=""><?php echo CHtml::encode($model->city->name); ?></a>
            </p>
            <p>
                Количество членов:
                <?php echo CHtml::encode($model->members_count);
                if (!empty($model->members_count_date))
                {
                    echo ' (на ' . $model->members_count_date_formatted() . ')';
                }
                ?>
            </p>
        </article>
    </section>
    <div class="g-clear_fix"></div>
    <section class="b-reestr_contacts">
        <h3>Контакты:</h3>
        <div class="b-reestr_contacts_icons">
							<span class="l-p_with_img reestr_contacts_icons1">
								<img src="/images/star_icon.png">
									<span>Рейтинг:</span>
									<b><?php echo CHtml::encode($model->rating); ?></b>
							</span>
							<span class="l-p_with_img">
								<img src="/images/loop_icon.png">
									Место в общем рейтинге: 94 567
							</span>
							<span class="l-p_with_img">
								<img src="/images/news_view_icon.png">
									Просмотров: <?php echo $model->views; ?>
							</span>
        </div>
        <div class="g-clear_fix"></div>
        <article>
            <p>
                Сайт: <?php echo CHtml::link(CHtml::encode($model->site), $model->site); ?>
            </p>
            <p>
                E-mail: <?php echo CHtml::link(CHtml::encode($model->email), CHtml::encode('mailto:' . $model->email)); ?>
            </p>
            <p>
                Телефон: <b><?php echo $model->phone1; ?></b>
            </p>
            <p>
                Факс: <b><?php echo $model->fax; ?></b>
            </p>
        </article>
    </section>
    <div class="g-clear_fix"></div>

    <?php $this->widget('application.modules.comments.widgets.CommentsList', array(
        'model' => $model,
    )); ?>

    <div class="g-clear_fix"></div>
    <section class="b-ask_form">
        <h3>Оставить отзыв</h3>
            <span class="l-page_registr">
                <a href="">Зарегистрироваться</a>
                <img src="/images/pager_arrow.png">
            </span>
        <div class="g-clear_fix"></div>
        <?php $this->widget('comments.widgets.CommentForm', array(
            'model' => $model,
        )); ?>
        <p>Ваш комментарий был успешно отправлен. После обработки его Администратором, он будет опубликован.</p>
    </section>
</div>