<?php
$this->page_title = 'Управление регионами';

$this->tabs = array(
    "добавить регион" => $this->createUrl("create")
);

$this->widget('AdminGrid', array(
	'id'=>'regions-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'sortable' => false,
	'columns'=>array(
		'name',
        array(
			'class'=>'CButtonColumn',
		),
	),
));
?>
