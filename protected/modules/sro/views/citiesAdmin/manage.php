<?php
$this->page_title = 'Управление городами';

$this->tabs = array(
    "добавить город" => $this->createUrl("create")
);

$this->widget('AdminGrid', array(
	'id'=>'cities-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'sortable' => false,
	'columns'=>array(
		'name',
		array(
            'name' => 'region',
            'value' => 'CHTML::encode($data->region)',
        ),
        array(
			'class'=>'CButtonColumn',
		),
	),
));
?>
