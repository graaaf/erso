<?php
$this->page_title = 'Просмотр';

$this->tabs = array(
    "Управление городами"  => $this->createUrl('manage'),
    "редактировать"        => $this->createUrl('update', array('id' => $model->id))
);

$this->widget('DetailView', array(
	'data' => $model,
	'attributes' => array(
		'name',
		array(
            'name' => 'region',
            'value' => $model->region,
        ),
	),
));
?>
