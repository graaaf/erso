<?php

return array(
	'activeForm' => array(
		'id'         => 'City-form',
		'class'      => 'CActiveForm',
	),
	'elements' => array(
		'name'      => array('type' => 'text'),
		'region_id' => array(
            'type' => 'dropdownlist',
            'items' => CHtml::listData(SroRegion::model()->findAll('', array('order' => 'name')), 'id', 'name'),
        ),
	),
	'buttons' => array(
		'submit' => array('type' => 'submit', 'value' => $this->model->isNewRecord ? 'Создать' : 'Сохранить')
	)
);
