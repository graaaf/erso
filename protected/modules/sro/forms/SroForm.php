<?php

return array(
	'activeForm' => array(
		'id'         => 'Sro-form',
		'class'      => 'CActiveForm',
        'htmlOptions'=> array('enctype'=> 'multipart/form-data'),
	),
	'elements' => array(
		'category_id' => array(
            'type' => 'dropdownlist',
            'items' => CHtml::listData(SroCategory::model()->findAll(''), 'id', 'name'),
        ),

		'name'      => array('type' => 'text'),
        'register_number' => array('type' => 'text'),
        'register_date' => array('type' => 'date'),
		'city_id' => array(
            'type' => 'dropdownlist',
            'items' => CHtml::listData(SroCity::model()->findAll('', array('order' => 'name')), 'id', 'name'),
        ),
        'members_count' => array('type' => 'text'),
        'members_count_date' => array('type' => 'date'),
        'rating' => array('type' => 'text'),
        'site' => array(
            'type' => 'text',
            'hint' => 'Например: www.yandex.ru',
        ),
        'email' => array('type' => 'text'),
        'phone1' => array(
            'type' => 'text',
            'hint' => 'Пример: + 7{пробел}(495){пробел}123{пробел}4567',
        ),
        'phone2' => array(
            'type' => 'text',
            'hint' => 'Пример: + 7{пробел}(495){пробел}123{пробел}4567',
        ),
        'phone3' => array(
            'type' => 'text',
            'hint' => 'Пример: + 7{пробел}(495){пробел}123{пробел}4567',
        ),
        'fax' => array(
            'type' => 'text',
            'hint' => 'Пример: + 7{пробел}(495){пробел}123{пробел}4567',
        ),
        'logo' => array(
			'type' => 'file',
		),
	),
	'buttons' => array(
		'submit' => array('type' => 'submit', 'value' => $this->model->isNewRecord ? 'Создать' : 'Сохранить')
	)
);
