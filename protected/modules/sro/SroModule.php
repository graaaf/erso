<?php

class SroModule extends WebModule
{
    public static function name()
    {
        return 'СРО';
    }


    public static function description()
    {
        return 'Управление СРО';
    }


    public static function version()
    {
        return '1.0';
    }


	public function init()
	{
		$this->setImport(array(
			'sro.models.*',
			'sro.components.*',
		));
	}

    public static function adminMenu()
    {
        return array(
			'Управление СРО'      => '/sro/sroAdmin/manage',
			'Управление городами' => '/sro/citiesAdmin/manage',
			'Управление регионами' => '/sro/regionsAdmin/manage',
        );
    }
}
