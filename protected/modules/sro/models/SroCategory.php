<?php

class SroCategory extends ActiveRecordModel
{
    const PAGE_SIZE = 10;


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'sro_categories';
	}


    public function name()
    {
        return 'Модель SroCategory';
    }


	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 255),

			array('id, name', 'safe', 'on' => 'search'),
		);
	}


	public function relations()
	{
		return array(
			'sroSros' => array(self::HAS_MANY, 'SroSro', 'category_id'),
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);

		return new ActiveDataProvider(get_class($this), array(
			'criteria' => $criteria
		));
	}
}