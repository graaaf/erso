<?php

class SroRegion extends ActiveRecordModel
{
    const PAGE_SIZE = 10;


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'sro_regions';
	}

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = 'Название';

        return $labels;
    }


    public function name()
    {
        return 'Модель SroRegion';
    }


	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 255),
            array('name', 'unique'),
			array('id, name', 'safe', 'on' => 'search'),
		);
	}


	public function relations()
	{
		return array(
			'sroCities' => array(self::HAS_MANY, 'SroCity', 'region_id'),
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);

		return new ActiveDataProvider(get_class($this), array(
			'criteria' => $criteria
		));
	}
}