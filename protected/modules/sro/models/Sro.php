<?php

class Sro extends ActiveRecordModel
{
    const PAGE_SIZE = 20;

    public $members_count_min;
    public $members_count_max;
    public $region_id;

    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'sro_sro';
	}


    public function name()
    {
        return 'Модель Sro';
    }

    public $logo;
	public function behaviors()
	{
		return array_merge(
                parent::behaviors(),
                array(
                    'fileBehavior' => array(
                        'class' => 'sro.behaviors.FileBehavior',
                        'fileDir' => '/upload/sro',
                        'uploadAttribute' => 'logo',
                    ),
                    'sortable' => array(
                        'class' => 'application.extensions.sortable.SortableBehavior'
                    ),
                )
         );
	}

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            array(
                'logo' => 'Логотип',
                'category' => 'Категория',
                'city' => 'Город',
            )
        );
    }

	public function rules()
	{
		return array(
			array('category_id, name, city_id', 'required'),
			array('category_id, city_id, members_count, rating', 'length', 'max' => 10),
			array('name, short_name', 'length', 'max' => 300),
			array('register_number, site, email, phone1, phone2, phone3, fax', 'length', 'max' => 255),
            array('members_count, rating', 'numerical', 'min' => 0, 'max' => 9999, 'integerOnly' => true),
            array('phone1, phone2, phone3, fax',
                'match',
		        'pattern' => '/^\+\s\d\s\(\d+\)\s\d+([\s-]\d+)?$/',
                'message'=>'Неправильно заполнен {attribute}. Пример: + 7{пробел}(495){пробел}123{пробел}4567'),
            array('site', 'url'),
            array('email', 'email'),
            array('logo',
                'file',
                'types' => 'jpg, jpeg, png, gif',
                'allowEmpty' => true,
                'safe' => true,
            ),
            array('register_date, members_count_date', 'date', 'format'=>array('dd.MM.yyyy','dd.M.yyyy', 'd.MM.yyyy', 'd.M.yyyy', 'yyyy-MM-dd'), 'allowEmpty'=>true),
			array('datetime', 'safe'),
			array('members_count_min, members_count_max, region_id, id, category_id, name, short_name, register_number, register_date, city_id, members_count, members_count_date, rating, site, email, phone1, phone2, phone3, fax, logo', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'category' => array(self::BELONGS_TO, 'SroCategory', 'category_id'),
			'city' => array(self::BELONGS_TO, 'SroCity', 'city_id'),
		);
	}

	protected function beforeSave()
	{
		if (parent::beforeSave())
		{
            $this->short_name = $this->name;
            $this->short_name = preg_replace( "/некоммерческое партнерство/ui", 'НП' , $this->short_name);
            $this->short_name = preg_replace( "/саморегулируемая организация/ui", 'СРО' , $this->short_name);

            $chunks = array('СРО', mb_substr($this->category->name, 0, 1, 'utf8'));
            if (!empty($this->register_number))
                $chunks[] = $this->register_number;

            if (!empty($this->register_date))
            {
                $dt = new DateTime($this->register_date);
                $chunks[] = $dt->format('dmY');
            }

            $this->register_full_number = implode('-', $chunks);

            return true;
		}
	}

    private function formattedDate($value)
    {
        if (empty($value))
            return '';

        $dt = new DateTime($value);
        return $dt->format('d/m/Y');
    }

    public function register_date_formatted()
    {
        return $this->formattedDate($this->register_date);
    }

    public function members_count_date_formatted()
    {
        return $this->formattedDate($this->members_count_date);
    }

	public function search()
	{
		$criteria = new CDbCriteria;

        $criteria->compare('category_id', $this->category_id, false);
        $criteria->compare('short_name', $this->short_name, true);
        $criteria->compare('register_number', $this->register_number, true);

        if (isset($this->members_count_min))
        {
            $criteria->addCondition('members_count >= :members_count_min');
            $criteria->params['members_count_min'] = $this->members_count_min;
        }

        if (isset($this->members_count_max))
        {
            $criteria->addCondition('members_count <= :members_count_max');
            $criteria->params['members_count_max'] = $this->members_count_max;
        }

        $criteria->with = 'city';

        if (isset($this->city_id))
        {
            $criteria->compare('city_id', $this->city_id);
        }

        if (isset($this->region_id))
        {
            $criteria->compare('city.region_id', $this->region_id);
        }

        $sortableFields = array('rating', 'members_count', 'views', 'region', 'register_full_number');
        if (isset($_GET['Sro_order']) && in_array($_GET['Sro_order'], $sortableFields))
        {
            $criteria->order = $_GET['Sro_order'] == 'region' ? 'city.name' : $_GET['Sro_order'];
            if (isset($_GET['Sro_order_by']) && $_GET['Sro_order_by'] == 'desc')
            {
                $criteria->order .= ' desc';
            }
        }
        else
        {
            $criteria->order = '`order`';
        }

		return new ActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize'=> self::PAGE_SIZE,
            ),
		));
	}

    public function getHref()
    {
        return Yii::app()->controller->createUrl("/sro/sro/view", array('id' => $this->id));
    }


    public function getLogoAsTag()
    {
        $logo = $this->getFileUrl();

        if ( empty($logo) )
        {
            return '';
        }

        $imageTag = CHtml::image($logo, '', array('width' => '141px'));

        return $imageTag;
        //return CHtml::link($imageTag, $this->href);
    }


}