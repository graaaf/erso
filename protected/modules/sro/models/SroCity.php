<?php

class SroCity extends ActiveRecordModel
{
    const PAGE_SIZE = 10;


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'sro_cities';
	}

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = 'Название';
        $labels['region'] = 'Регион';
        $labels['region_id'] = 'Регион';

        return $labels;
    }


    public function name()
    {
        return 'Модель SroCity';
    }


	public function rules()
	{
		return array(
			array('name, region_id', 'required'),
			array('name', 'length', 'max' => 255),
            array('name', 'unique'),
			array('region_id', 'length', 'max' => 10),
			array('id, name, region_id', 'safe', 'on' => 'search'),
		);
	}


	public function relations()
	{
		return array(
			'region' => array(self::BELONGS_TO, 'SroRegion', 'region_id'),
			'sroSros' => array(self::HAS_MANY, 'SroSro', 'city_id'),
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('region_id', $this->region_id, true);

		return new ActiveDataProvider(get_class($this), array(
			'criteria' => $criteria
		));
	}
}