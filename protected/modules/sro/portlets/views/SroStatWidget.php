<ul>
    <li class="h-first">
        <img src="/images/build_icon.png"/>
        <?php echo CHtml::link('Строительство:', Yii::app()->createUrl('/sro/sro/index', array('search[category_id]' => 1))); ?>
        <span><?php echo $items[1];?></span>
    </li>
    <li>
        <img src="/images/project_icon.png"/>
        <?php echo CHtml::link('Проектирование:', Yii::app()->createUrl('/sro/sro/index', array('search[category_id]' => 2))); ?>
        <span><?php echo $items[2];?></span>
    </li>
    <li>
        <img src="/images/find_icon.png"/>
        <?php echo CHtml::link('Изыскание:', Yii::app()->createUrl('/sro/sro/index', array('search[category_id]' => 3))); ?>
        <span><?php echo $items[3];?></span>
    </li>
</ul>