<?php

class SroStatWidget extends Portlet
{
    public function renderContent()
    {
        $sql = "
            SELECT
              c.id, COUNT(s.id) as cnt
            FROM
              sro_categories c
            LEFT JOIN  sro_sro s ON s.category_id = c.id
            GROUP BY c.id
        ";
        $query = Yii::app()->db->createCommand($sql);
        $rows = $query->queryAll();

        $items = array();
        foreach ($rows as $row)
            $items[$row['id']] = $row['cnt'];

        $this->render('SroStatWidget', array('items' => $items));
    }

}