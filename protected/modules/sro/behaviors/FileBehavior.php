<?php

/**
 * FileBehavior class
 *
 * @author Vorotilov Vadim <fant.geass@gmail.com>
 */
class FileBehavior extends CActiveRecordBehavior
{
	/**
	 * Model's attribute for uploading file
	 * @var string
	 */
	public $uploadAttribute = 'uploadFile';

	/**
	 * Relative directory where to store files
	 * @var string "/data/files"
	 */
	public $fileDir;

	/**
	 * If true:
	 *       All new files will be added to existed files
	 * Else:
	 *       New file will overwrite older file
	 * @var bool
	 */
	public $allowMultiple = false;

	protected $_pk;
	protected $_dirPath;
	protected $_dirUrl;
	protected $_filesPathes;
	protected $_filesUrls;


	/**
	 * After save event
	 *
	 * @param CModelEvent $event
	 */
	public function afterSave($event)
	{

		if ($this->owner->isAttributeSafe($this->uploadAttribute))
        {
			if ($this->allowMultiple) {
				$this->handleMultipleFiles();
			} else {
				$this->handleOneFile();
			}
		}

		parent::afterSave($event);
	}

	protected function handleOneFile()
	{
		$uploadedFile = CUploadedFile::getInstance($this->owner, $this->uploadAttribute);

		if ($uploadedFile !== null) {

			$dirPath = $this->getDirPath();
			if (file_exists($dirPath) || mkdir($dirPath, 0777) ) {

				$this->deleteFiles();
				$filepath = $this->getDirPath() . '/' . self::transliterate($uploadedFile->name);

				if (!$uploadedFile->saveAs( $filepath )) {
					throw new CException("Can not save file: " . $filepath);
				}

			} else {
				throw new CException("Can not create directory: " . $this->getDirPath());
			}

		}
	}

	protected function handleMultipleFiles()
	{
		$uploadedFiles = CUploadedFile::getInstances($this->owner, $this->uploadAttribute);

		if (!empty($uploadedFiles)) {

			$dirPath = $this->getDirPath();
			if (file_exists($dirPath) || mkdir($dirPath, 0777) ) {

				foreach ($uploadedFiles as $uploadedFile) { /** @var CUploadedFile $uploadedFile  */

					$filepath = $this->getDirPath() . '/' . $uploadedFile->name;

					if (!$uploadedFile->saveAs( $filepath )) {
						throw new CException("Can not save file: " . $filepath);
					}

				}

			} else {
				throw new CException("Can not create directory: " . $this->getDirPath());
			}

		}
	}


	/**
	 * After delete event
	 * @param CEvent $event
	 */
	public function afterDelete($event)
	{
		$this->deleteFiles();

		parent::afterDelete($event);
	}


	/**
	 * @return array
	 */
	public function getFilesUrls()
	{
		if ($this->_filesUrls === null) {
			$filesPathes = $this->getFilesPathes();

			$this->_filesUrls = array();
			foreach ($filesPathes as $filePath) {
				$this->_filesUrls[] = str_replace(Yii::getPathOfAlias('webroot'), '', $filePath);
			}
		}

		return $this->_filesUrls;
	}

	/**
	 * Get url to file
	 *
	 * @return string
	 */
	public function getFileUrl()
	{
		$filesUrls = $this->getFilesUrls();

		if (empty($filesUrls)) {
			return false;
		} else {
			return $filesUrls[0];
		}
	}

	/**
	 * Get filepath to file
	 *
	 * @param $formatName
	 *
	 * @return string
	 */
	public function getFilePath()
	{
		$filesPathes = $this->getFilesPathes();

		if (empty($filesPathes)) {
			return false;
		} else {
			return $filesPathes[0];
		}
	}

	/**
	 * Get files pathes for existed files
	 *
	 * @return array files pathes
	 */
	public function getFilesPathes($refresh = false)
	{
		if ($this->_filesPathes === null || $refresh) {
			$this->_filesPathes = glob($this->getDirPath() . '/*');
		}

		return $this->_filesPathes;
	}

	/**
	 *
	 */
	protected function deleteFiles()
	{
		$filesPathes = $this->getFilesPathes();

		foreach ($filesPathes as $filePath) {
			unlink($filePath);
		}
	}


	/**
	 * @return string
	 */
	protected function getDirPath()
	{
		if ($this->_dirPath === null) {
			$this->_dirPath = Yii::getPathOfAlias('webroot') . $this->fileDir . '/' . $this->getPk();
		}

		return $this->_dirPath;
	}

	/**
	 * @return string
	 */
	protected function getDirUrl()
	{
		if ($this->_dirUrl=== null) {
			$this->_dirUrl = $this->fileDir . '/' . $this->getPk();
		}

		return $this->_dirUrl;
	}


	/**
	 * Get primary key
	 * If composite, then implode
	 * @return string
	 */
	protected function getPk()
	{
		if ($this->_pk === null) {
			$this->_pk = $this->owner->primaryKey;
			if (is_array($this->_pk)) {
				$this->_pk = implode('_', $this->_pk);
			}
		}

		return $this->_pk;
	}

	/**
	 * Transliterate symbols
	 * @static
	 * @param string $str
	 * =
	 * @return string
	 */
	private static function transliterate($str)
	{
		$arr = array(
			"А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
			"Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
			"Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
			"О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
			"У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
			"Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
			"Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
			"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
			"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
			"ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
			" "=> "_", "."=> ".", "/"=> "_"
		);

		if (is_array($arr) && preg_match('/[^A-Za-z0-9_\-]/', $str)) {
			$str = strtr($str, $arr);
		}

		return $str;
	}

}
