<?php
/**
 * Обрезает $content используя плагин jTruncate: http://www.jeremymartin.name/projects.php?project=jTruncate
 */
class Truncate extends CWidget
{

    public $content;

    public $options = array();

    private $defaultOptions = array(
        'length'      => 500,
        'minTrail'    => 0,
        'moreText'    => "Подробнее",
        'lessText'    => "",
        'ellipsisText'=> "...",
        'moreAni'     => "fast",
        'lessAni'     => 2000
    );


    public function init()
    {
        parent::init();

        $this->initVars();
        $this->registerScripts();
    }


    public function initVars()
    {
        $this->options = CMap::mergeArray($this->defaultOptions, $this->options);
    }


    public function registerScripts()
    {
        $options = CJavaScript::encode($this->options);

        $cs = Yii::app()->clientScript;

        $cs->registerScriptFile('/js/plugins/jTruncate/jTruncate.js');

        $cs->registerScript($this->id.'_truncate', "
            //$('#{$this->id}').jTruncate({$options});
            //$('#{$this->id}').jTruncate({'length':500,'minTrail':0,'moreText':'Подробнее','lessText':'','ellipsisText':'...','moreAni':'fast','lessAni':2000});
            $('#{$this->id}').jTruncate({$options});
        ");
    }


    public function run()
    {
        echo CHtml::tag('div', array('id' => $this->id), $this->content);
    }


    public function getModuleId()
    {
        return 'content';
    }

}