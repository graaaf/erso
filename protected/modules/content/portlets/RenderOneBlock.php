<?php
/**
 * Выводит одну запрошенную страницу
 */
class RenderOneBlock extends CWidget
{
    public $blockID;

    private $_model;

    private $_content;


    public function init()
    {
        parent::init();

        $this->_model = PageBlock::model()->findByPk( $this->blockID );

        if( empty($this->_model) )
        {
            $this->_content = '';
        }
        else
        {
            $this->_content = $this->render('render_one_block', array(
                'model' => $this->_model,
            ), true);
        }
    }


    public function run()
    {
        echo $this->_content;
    }


}

?>