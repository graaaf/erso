<?php

class PageController extends BaseController
{
    public $layout = '//layouts/content';

    public $newsPerPage = 5;
    
    public static function actionsTitles()
    {
        return array(
            "Index" => "Просмотр всех страниц",
            "View" => "Просмотр страницы",
            "Main" => "Главная страница"
        );
    }


    public function actionIndex()
    {
        $pageScopes = Page::model()->published();

        $count = $pageScopes->count();

        $pagination = new CPagination($count);
        $pagination->pageSize = $this->newsPerPage;
        $pagination->applyLimit( $pageScopes->getDbCriteria() );

        $this->render('index', array(
            'pages' => $pageScopes->findAll(),
            'pagination' => $pagination,
        ));
    }


    public function actionView()
    {
        $id = $this->request->getParam("id");


        if ($id)
        {
            $page = Page::model()->published()->findByPk($id);
            if (!$page)
            {
                $this->pageNotFound();
            }
        }
        else
        {
            $url  = $this->request->getParam("url");
            $page = Page::model()->published()->findByAttributes(array("url" => $url));
            if (!$page)
            {
                $this->pageNotFound();
            }
        }

        $this->render("view", array("page" => $page));
    }


    public function actionMain()
    {
        $this->render('main');
    }
}
