<?php

class MainPageAdminController extends AdminController
{
    public static function actionsTitles()
    {
        return array(
            "View"      => "Просмотр мета-тегов главной страницы",
            "Update"    => "Редактирование мета-тегов главной страницы",
        );
    }


    public function actionView()
    {
        $model = $this->loadModel();

        $this->render('view', array('model' => $model));
    }


    public function actionUpdate()
    {
        $model = $this->loadModel();

        $form = new BaseForm('content.MainPageForm', $model);

        if ($form->submitted('submit'))
        {
            $model = $form->model;
            if ($model->save())
            {
                $this->redirect(array(
                    'view',
                ));
            }
        }

        $this->render('update', array(
            'form' => $form,
        ));
    }


    public function loadModel()
    {
        $model = MetaTag::model()->findByAttributes(array(
            'model_id' => 'MainPage',
            'object_id' => 1,
        ));

        if ($model === null)
        {
            $this->pageNotFound();
        }

        return $model;
    }

}
