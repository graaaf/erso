<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        'Статьи и интервью',
    ),
));
?>

<h3>Статьи и интервью</h3>
<div class = "b-news">
    <?php foreach ($pages as $page): ?>
        <section class="b-news_list">
            <div class = "g-clear_fix"></div>
            <?php echo CHtml::link($page->title, array('/content/page/view', 'id' => $page->id), array('class' => 'news_title g-clear_fix')); ?>
            <p>
                <?php echo TextComponent::reduceContent($page->text, 50); ?>
            </p>
        </section>
    <?php endforeach; ?>
    <div class = "g-clear_fix"></div>
</div><!--end b-news-->

<div class="pager">
    <?php $this->widget('CLinkPager', array(
        'pages' => $pagination,
        'cssFile' => false,
        'nextPageLabel' => CHtml::image('/images/pager_arrow.png') . ' Следующая',
        'prevPageLabel' => 'Предыдущая ' . CHtml::image('/images/pager_arrow_back.png'),
        'header' => '<p>Страницы:</p>',
    )) ?>
</div>