<?php $this->widget('zii.widgets.CBreadcrumbs', array(
    'htmlOptions' => array('class' => 'l-breadcrumbs'),
    'separator' => CHtml::image('/images/breadcrumbs.png'),
    'homeLink'=>CHtml::link('Главная страница', array('/content/page/main')),
    'links' => array(
        $page->title,
    ),
));
?>

<h3><?php echo $page->title; ?></h3>
<div class="b-view">
    <?php echo $page->content ?>

    <?php
    $this->widget('fileManager.portlets.FileList', array(
        'model' => $page,
        'tag' => 'files'
    ));
    ?>
</div>