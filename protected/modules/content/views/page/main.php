<div class = "b-main">
    <?php $this->widget('news.widgets.SlideNews'); ?>
    <div class = "g-clear_fix"></div>
    <?php $this->widget('news.widgets.TopNews'); ?>
    <div class = "b-main_docs">
        <header>
            <h3>Новые документы</h3>
                <span class="l-view_all">
                    <img src="images/plus_icon.png">
                    <a href="">Все документы</a>
                </span>
        </header>
        <section>
            <a href = "" class = "doc_link">
                Постановление правительства Российской
                федерации от 24 марта 2011 г.  №  207  «О
                минимально необходимых требованиях к выдаче
                саморегулируемыми организациями свидетельств
                о допуске к работам на особо опасных и
                технически сложных объектах капитального
                строительства, оказывающим влияние на
                безопасность указанных объектов»
            </a>
            <time datetime="2012-12-23T08:23:11+07:00" class="l-date">4 июня 2012 года, 11:12</time>
            <nav class="b-tags">
                <img src="images/tag_icon.png">
                <a href="">НОИЗ</a>,
                <a href="">СРО</a>,
                <a href="">Госорганы</a>
            </nav>
        </section>
        <section>
            <a href = "" class = "doc_link">
                Бедросу Киркорову присвоено почётное звание
                «Народный артист Российской Федерации»
            </a>
            <time datetime="2012-12-23T08:23:11+07:00" class="l-date">4 июня 2012 года, 11:12</time>
            <nav class="b-tags">
                <img src="images/tag_icon.png">
                <a href="">НОИЗ</a>,
                <a href="">СРО</a>,
                <a href="">Госорганы</a>
            </nav>
        </section>
        <section class = "h-last">
            <a href = "" class = "doc_link">
                Поздравление Королеве Дании Маргрете II с
                национальным праздником – Днём конституции
            </a>
            <time datetime="2012-12-23T08:23:11+07:00" class="l-date">4 июня 2012 года, 11:12</time>
            <nav class="b-tags">
                <img src="images/tag_icon.png">
                <a href="">НОИЗ</a>,
                <a href="">СРО</a>,
                <a href="">Госорганы</a>
            </nav>
        </section>
        <div class = "g-clear_fix"></div>
    </div>

    <div class = "g-clear_fix"></div>
    <div class = "b-tabs">
        <header>
            <h3>ТОП 5 СРО</h3>
                <span class="l-view_all">
                    <img src="images/plus_icon.png">
                    <a href="">Реестр СРО</a>
                </span>
        </header>
        <div class = "g-clear_fix"></div>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Строительство</a></li>
                <li><a href="#tabs-2">Проектирование</a></li>
                <li><a href="#tabs-3">Изыскание</a></li>
            </ul>
            <div class = "g-clear_fix"></div>
            <div class = "b-tabs_wrap" id="tabs-1">
                <ul>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">1</span>
                        </div>
                        <a href = "">Саморегулируемые организации арбитражных управляющих (СРО арбитражных управляющих);</a>
                    </li>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">2</span>
                        </div>
                        <a href = "">
                            Саморегулируемая организация некоммерческое партнерство «Объединение организаций выполняющих
                            проектные работы в газовой и нефтяной отрасли «Инженер-Проектировщик» (далее - СРО НП
                            «Инженер-Проектировщик».
                        </a>
                    </li>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">13</span>
                        </div>
                        <a href = "">Организация в области пищевой и перерабатывающий промышленности</a>
                    </li>
                </ul>
            </div>
            <div class = "b-tabs_wrap"  id="tabs-2">
                <ul>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">1</span>
                        </div>
                        <a href = "">Саморегулируемые организации арбитражных управляющих (СРО арбитражных управляющих);</a>
                    </li>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">2</span>
                        </div>
                        <a href = "">
                            Саморегулируемая организация некоммерческое партнерство «Объединение организаций выполняющих
                            проектные работы в газовой и нефтяной отрасли «Инженер-Проектировщик» (далее - СРО НП
                            «Инженер-Проектировщик».
                        </a>
                    </li>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">3</span>
                        </div>
                        <a href = "">Организация в области пищевой и перерабатывающий промышленности</a>
                    </li>
                </ul>
            </div>
            <div class = "b-tabs_wrap"  id="tabs-3">
                <ul>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">1</span>
                        </div>
                        <a href = "">Саморегулируемые организации арбитражных управляющих (СРО арбитражных управляющих);</a>
                    </li>
                    <li>
                        <div class = "b-tabs_list_number">
                            <span class = "tabs_list_number">2</span>
                        </div>
                        <a href = "">
                            Саморегулируемая организация некоммерческое партнерство «Объединение организаций выполняющих
                            проектные работы в газовой и нефтяной отрасли «Инженер-Проектировщик» (далее - СРО НП
                            «Инженер-Проектировщик».
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class = "g-clear_fix"></div>


    <div class = "b-main_banners">
        <?php $this->widget('application.modules.banners.portlets.BannerWidget', array('position' => 'middle')); ?>
    </div>

    <div class = "g-clear_fix"></div>

    <div class = "b-sro">
        <h3>СРО в лицах </h3>
        <section class = "c-sro_left">
            <figure><img src = "images/SRO_IMG.png" /></figure>
            <article>
                <a href = "" class = "sro_title">Баштанюк Геннадий Сергеевич</a>
                <p>
                    Вице – президент СРО «Центрстройэкспертиза – статус»
                </p>
                    <span class="l-view_all">
                        <img src="images/plus_icon.png">
                        <a href="">Полный перечень лиц</a>
                    </span>
            </article>
        </section>
        <section class = "c-sro_right">
            <?php
            $this->widget('content.portlets.RenderOneBlock', array(
                'blockID' => '8',
            ));
            ?>
        </section>
    </div>

    <div class = "g-clear_fix"></div>

    <div class = "b-view b-main_about">

        <?php
        ob_start();
            $this->widget('content.portlets.RenderOneBlock', array(
                'blockID' => '7',
            ));

            $aboutSiteContent = ob_get_contents();
        ob_end_clean();

        $this->widget('content.portlets.Truncate', array(
            'content' => $aboutSiteContent,
            'options' => array(
                'length' => 510,
                'moreText' => 'Подробнее'
            ),
        ));
        ?>

    </div>
</div><!--end b-main-->
