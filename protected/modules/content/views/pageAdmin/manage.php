<?php
$this->page_title = 'Страницы сайта';

$this->tabs = array(
    "добавить страницу" => $this->createUrl('create')
);

$this->widget('AdminGrid', array(
	'id' => 'page-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'columns' => array(
        'updated_at',
		array(
			'name' => 'title',
			'type' => 'raw'
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
));
?>

