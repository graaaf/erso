<?php
$this->tabs = array(
    "редактировать" => $this->createUrl('update', array('id' => $model->id))
);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'title',
        'keywords',
        'description',
    ),
));
?>
