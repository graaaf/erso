<?php

class Page extends ActiveRecordModel
{
    const PAGE_SIZE = 10;

    public $god;

    public function name()
    {
        return 'Страницы';
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'pages';
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
            'MetaTag' => array('class' => 'application.components.activeRecordBehaviors.MetaTagBehavior'),
            'FileManager' => array('class' => 'application.components.activeRecordBehaviors.AttachmentBehavior', 'attached_model' => 'FileManager'),
        ));
    }


    public function rules()
    {
        return array(
            array('title, lang', 'required'),
            array('is_published', 'numerical', 'integerOnly' => true),
            array('url', 'length', 'max' => 250),
            array('title', 'length', 'max' => 200),
            array('text, url', 'safe'),
            array('meta_tags, god', 'safe'),
            array('title', 'filter', 'filter' => 'strip_tags'),
            array('id, title, url, text, is_published, updated_at', 'safe', 'on' => 'search'),
        );
    }


    public function scope()
    {
        return array(
            'published' => array(
                'condition' => 'is_published = 1',
                'ord' => 'ord',
            ),
        );
    }



    public function relations()
    {
        return array(
            'language' => array(self::BELONGS_TO, 'Language', 'lang')
        );
    }


    public function in($row, $values, $operator = 'AND')
    {
        $this->getDbCriteria()->addInCondition($row, $values, $operator);
        return $this;
    }


    public function beforeSave()
    {
        $this->updated_at = new CDbExpression('NOW()');

        return parent::beforeSave();
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('is_published', $this->is_published);
        $criteria->compare('updated_at', $this->updated_at, true);

        return new ActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'ord',
            ),
        ));
    }


    public function getHref()
    {
        $url = trim($this->url);
        if ($url)
        {
            if ($url[0] != "/")
                $url = "/{$url}";

            return $url;
        }
        else
        {
            return "/page/" . $this->id;
        }
    }


    public function afterSave()
    {

            if ($this->url != '')
            {
                $routes = array_merge(array($this->url => "content/page/view/id/{$this->id}"), include(PROTECTED_PATH . 'config/routes.php'));
                file_put_contents(PROTECTED_PATH . 'config/routes.php', "<?php \n return " . var_export($routes, true) . ";");
            
            }
            
            parent::afterSave();
            return true;

    }


    public function getContent()
    {
        $content = $this->text;

        if (RbacModule::isAllow('PageAdmin_Update'))
        {
            $content.= "<br/><a href='/content/pageAdmin/update/id/{$this->id}' class='admin_link'>Редактировать</a>";
        }

        return $content;
    }


}
