<?php

return array(
    'activeForm'=> array(
        'id'                   => 'page-form',
        'class'                => 'CActiveForm',
        'enableAjaxValidation' => true,
    ),
    'elements'  => array(
        'meta_tags'    => array('type' => 'MetaTags'),
        'title'        => array('type' => 'text'),
        'url'          => array(
            'type'  => 'alias',
            'source'=> 'title',
        ),
        '<span style="color: gray;">Пример: razdel/podrazdel/nazvanie_stranici (/ вначале не нужен)</span>',
        'text'         => array('type' => 'editor'),
        'is_published' => array('type' => 'checkbox'),
        'files'        => array(
            'type'      => 'file_manager',
            'data_type' => 'any',
            'title'     => 'Файлы для скачивания',
            'tag'       => 'files'
        ),

    ),
    'buttons'   => array(
        'submit' => array(
            'type'  => 'submit',
            'value' => 'сохранить'
        )
    )
);

