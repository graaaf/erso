<?php

if ($this->model->isNewRecord)
{
    $items = array();
}
else
{
    $model = $this->model->model_id;
    $items = array();

    $objects = $model::model()->findAll();
    foreach ($objects as $i => $object)
    {
        $items[$object->id] = (string) $object;
    }
}

return array(
    'activeForm' => array(
        'id' => 'meta-tag-form',
        //'enableAjaxValidation' => true,
        //'clientOptions' => array(
        //	'validateOnSubmit' => true,
        //	'validateOnChange' => true
        //)
    ),
    'elements' => array(
        'title' => array(
            'type' => 'text'
        ),
        'description' => array(
            'type' => 'text'
        ),
        'keywords' => array(
            'type' => 'text'
        ),
    ),
    'buttons' => array(
        'submit' => array(
            'type'  => 'submit',
            'value' => $this->model->isNewRecord ? 'создать' : 'сохранить',
            'id'    => 'meta_tag_submit'
        )
    )
);


