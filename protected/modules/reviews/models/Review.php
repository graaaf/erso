<?php

class Review extends ActiveRecordModel
{

    const UPLOAD_DIR = 'upload/reviews/images';
    const STATE_PUBLISHED = 1;
    const STATE_NOT_PUNLISHED = 0;
    const IMAGE_WIDTH = 242;
    const IMAGE_HEIGHT = 183;
    const PAGE_SIZE = 10;

    public static $states = array(
        self::STATE_PUBLISHED => 'Да',
        self::STATE_NOT_PUNLISHED => 'Нет',
    );

    public function name()
    {
        return 'Отзывы';
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
                    'FileManager' => array('class' => 'application.components.activeRecordBehaviors.AttachmentBehavior', 'attached_model' => 'FileManager'),
                    'sortable' => array('class' => 'ext.sortable.SortableBehavior'),
                    'MetaTag' => array('class' => 'application.components.activeRecordBehaviors.MetaTagBehavior'),
                ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'reviews';
    }


    public function rules()
    {
        return array(
            array('names, description', 'required'),
            array('is_published, portfolio', 'safe'),
            array('image', 'file', 'types' => 'jpg, jpeg, png, gif', 'allowEmpty' => false, 'on' => 'create'),
            array('image', 'file', 'types' => 'jpg, jpeg, png, gif', 'allowEmpty' => true, 'on' => 'update'),
        );
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->order = '`order` DESC';
        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


    public function relations()
    {
        return array(
            'files' => array(
                self::HAS_MANY,
                'FileManager',
                'object_id',
                'condition' => 'files.model_id = "' . get_class($this) . '" AND files.tag="files"',
                'order' => 'files.order DESC'
            ),
        );
    }


    public function uploadFiles()
    {
        return array(
            'image' => array(
                'dir' => self::UPLOAD_DIR
            )
        );
    }


    public function getHref()
    {
        return Yii::app()->controller->createUrl("/reviews/review/view", array('id' => $this->id));
    }


    public static function getPortfolioItems()
    {
        $portfolio = Portfolio::model()->findAll();
        $items = array('0' => 'Не указано');

        foreach ($portfolio as $item)
        {
            $items[$item->id] = $item->name . ', ' . $item->names;
        }

        return $items;
    }


}
