<?php

class ReviewController extends BaseController
{

    public $layout = '/layouts/content';

    public static function actionsTitles()
    {
        return array(
            'Index' => 'Отзывы',
            'View' => 'Просмотр отзыва',
        );
    }


    public function actionIndex()
    {
        $model = Review::model()->published()->ordered();
        $criteria = $model->dbCriteria;
        
        $pages = new CPagination($model->count($criteria));
        $pages->pageSize = Review::PAGE_SIZE;
        $pages->applyLimit($criteria);
            
        $reviews = Review::model()->findAll($criteria);

        $this->render('index', array(
            'reviews' => $reviews,
            'pages' => $pages,
        ));
    }
    
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }


}