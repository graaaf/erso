<?php
$this->tabs = CMap::mergeArray(array(
            'Редактрование отзыва' => $this->url('update', array('id' => $model->id)),
                ), $this->tabs);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'names',
        array(
            'name' => 'description',
            'type' => 'html',
            'value' => Yii::app()->text->cut($model->description, 390, "., -:;", " ..."),
        ),
        array(
            'name' => 'image',
            'value' => ImageHelper::thumb(Review::UPLOAD_DIR, $model->image, Review::IMAGE_WIDTH, Review::IMAGE_HEIGHT),
            'type' => 'html'
        ),
    ),
));