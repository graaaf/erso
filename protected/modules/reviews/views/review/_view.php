<div class="item">
    <div class="reviewer"><?php echo $model->names; ?></div>
    <?php echo ImageHelper::thumb(Review::UPLOAD_DIR, $model->image, Review::IMAGE_WIDTH, Review::IMAGE_HEIGHT); ?>
    <p>
        <?php echo $model->description; ?>
    </p>
    <div class="clear"></div>


    <?php if ($model->portfolio): ?>
        <div class="more">
            <a href="<?php echo Portfolio::model()->findByPk($model->portfolio)->href; ?>">Смотреть всю свадьбу целиком</a>
        </div>
    <?php endif; ?>

</div>