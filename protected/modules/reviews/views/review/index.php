<?php $this->crumbs = array('Отзывы'); ?>

<div class="text reviews">
    <?php foreach ($reviews as $review): ?>
        <?php $this->renderPartial('_view', array('model' => $review)); ?>
    <?php endforeach; ?>
</div>

<?php $this->renderPartial('//layouts/_pagination', array(
    'pages' => $pages,
)); ?>