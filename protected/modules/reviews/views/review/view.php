<div class="text reviews">
    <div class="item">
        <div class="reviewer"><?php echo $model->names; ?></div>
        <?Php echo ImageHelper::thumb(Review::UPLOAD_DIR, $model->image, Review::IMAGE_WIDTH, Review::IMAGE_HEIGHT); ?>
        <p>
            <?php echo $model->description; ?>
        </p>
        <div class="clear"></div>

        <?php if ($model->portfolio): ?>
            <div class="more">
                <a href="<?php echo Portfolio::model()->findByPk($model->portfolio)->href; ?>">Смотреть работу в портфолио</a>
            </div>
        <?php endif; ?>
    </div>
</div>