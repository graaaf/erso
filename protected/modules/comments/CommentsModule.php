<?php
class CommentsModule extends WebModule
{
    public static $active = true;

    public function init()
    {
        $this->setImport(array(
            'comments.components.*',
            'comments.models.*'
        ));
    }

    public static function name()
    {
        return 'Комментарии';
    }


    public static function description()
    {
        return 'Комментарии';
    }


    public static function version()
    {
        return '0.1';
    }


    public static function adminMenu()
    {
        return array(
            'Управление комментариями' => '/comments/commentAdmin/Manage',
        );
    }
}