<section class="b-opinion">
    <h3>Отзывы (<?php echo $commentsCount; ?>)</h3>

    <?php foreach ($comments as $comment): ?>
        <article>
            <header>
                <?php echo $comment->name ?>
                <?php echo HtmlHelper::timeTag($comment->date_created, 'd MMMM y года, H:mm', array('class'=>'l-date')) ?>
            </header>
            <span class="opinion_top"></span>
            <p>
                <?php echo $comment->text; ?>
            </p>
        </article>
    <?php endforeach; ?>

</section>

<div class="pager">
    <?php $this->widget('CLinkPager', array(
        'pages' => $pagination,
        'cssFile' => false,
        'nextPageLabel' => CHtml::image('/images/pager_arrow.png') . ' Следующая',
        'prevPageLabel' => 'Предыдущая ' . CHtml::image('/images/pager_arrow_back.png'),
        'header' => '<p>Страницы:</p>',
    )) ?>
</div>