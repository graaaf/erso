<?php

class CommentsList extends CWidget
{

    public $model;

    public $commentsPerPage = 20;

    private $_viewFile = 'comments_list';


    public function init()
    {
    }


    public function run()
    {
        $model_name = get_class($this->model);
        $object_id = $this->model->id;

        $commentsScopes = Comment::model()->object($model_name, $object_id)->published()->last();

        $criteria = $commentsScopes->dbCriteria;

        $count = $commentsScopes->count( $criteria );

        $pagination = new CPagination( $count );
        $pagination->pageSize = $this->commentsPerPage;
        $pagination->applyLimit( $criteria );

        $this->render($this->_viewFile, array(
            'comments' => $commentsScopes->findAll( $criteria ),
            'commentsCount' => $count,
            'pagination' => $pagination,
        ));
    }


}