<?php
/**
 * Виджет вывода формы комментирования
 * В качестве параметров принимает модель, например:
 * 
 * <pre>
 *     $this->widget('comments.widgets.CommentForm', array(
 *         'model' => $model,
 *     ));
 * </pre>
 */
class CommentForm extends Portlet
{
    public $model;
    
    public function renderContent() 
    {
        $model_name = get_class($this->model);
        $object_id = $this->model->id;
        
        $model = new Comment;
        $model->model_name = $model_name;
        $model->object_id = $object_id;
        
        $form = new BaseForm('comments.AddCommentForm', $model);

        $this->render('comment_form', array(
            'model_name' => $model_name, 
            'object_id' => $object_id,
            'form' => $form,
        )); 
    }
}