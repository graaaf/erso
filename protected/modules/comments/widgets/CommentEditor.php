<?php

class CommentEditor extends InputWidget
{
    public $options = array();
    public $packageName = 'comment-editor';

    public function init()
    {
        parent::init();
        list($this->name, $this->id) = $this->resolveNameId();

        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($this->assets . '/redactor.js');
        $cs->registerScriptFile($this->assets . '/comments.js');
        $cs->registerCssFile($this->assets . '/css/redactor.css');
        $cs->registerScript('jQuery(#' . $this->getId() . ')', 'jQuery(' . CJavaScript::encode('#' . $this->getId()) . ').commentForm(' . CJavaScript::encode($this->options) . ');', CClientScript::POS_READY);
    }


    public function run()
    {
        echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
    }


}