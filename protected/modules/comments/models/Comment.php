<?php

class Comment extends ActiveRecordModel
{

    public $captcha;

    const STATE_PUBLISHED = 1;
    const STATE_NOT_PUNLISHED = 0;
    const PAGE_SIZE = 15;
    const MAX_CHARS = 2000;
    const MODERATE_ENABLED = true;

    const SETTING_MAIL_BODY = 'comments_mail_body';
    const SETTING_MAIL_SUBJECT = 'comments_mail_subject';

    public static $states = array(
        self::STATE_PUBLISHED => 'Да',
        self::STATE_NOT_PUNLISHED => 'Нет',
    );

    public function name()
    {
        return 'Комментарии';
    }


    function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['captcha'] = 'Код проверки';
        return $labels;
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['sortable'] = array('class' => 'ext.sortable.SortableBehavior');
        return $behaviors;
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'comments';
    }


    public function primaryKey()
    {
        return 'id';
    }


    public function rules()
    {
        return array(
            array(
                'text',
                'length',
                'min' => 2,
                'max' => self::MAX_CHARS,
                'tooShort' => 'Текст комментария не должен быть короче 2 символов.',
                'tooLong' => 'Текст комментария не должен быть длиннее ' . self::MAX_CHARS . ' символов.'
            ),
            array('id, date_created, date_moderated, author_id, model_name, object_id, is_published', 'safe'),
            array('text', 'required'),
            array('text', 'isOnlyQuote'),
            array('text', 'filter', 'filter' => 'strip_tags'),
        );
    }


    public function isOnlyQuote($attribute, $params)
    {
        if (trim(strip_tags(preg_replace('#<blockquote>.*?</blockquote>#is', '', $this->text))) == '')
            $this->addError('text', 'Текст не может состоять только из цитаты');
    }


    public function scopes()
    {
        return array(
            'last' => array('order' => 'date_created DESC'),
            'order' => array('order' => '`order` DESC')
        );
    }


    public function beforeSave()
    {
        if ($this->isNewRecord)
        {
            $this->name = User::model()->findByPk( Yii::app()->user->id )->name;

            $this->date_created = date('Y-m-d H:i:s');

            if (Comment::MODERATE_ENABLED == true)
            {
                $this->is_published = Comment::STATE_NOT_PUNLISHED;
                $this->date_moderated = '0000-00-00 00:00:00';
            } else
            {
                $this->is_published = Comment::STATE_PUBLISHED;
                $this->date_moderated = date('Y-m-d H:i:s');
            }
        }

        $this->text = nl2br( $this->text );

        return true;
    }


    public function object($model_name, $object_id)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition("model_name = '{$model_name}'");
        $criteria->addCondition("object_id = '{$object_id}'");
        $this->model()->getDbCriteria()->mergeWith($criteria);

        return $this;
    }


    public function in($row, $values, $operator = 'AND')
    {
        $this->getDbCriteria()->addInCondition($row, $values, $operator);
        return $this;
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->order = '`date_created` DESC';

        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


    public function afterSave()
    {
        if ($this->isNewRecord)
        {

            $mailer_letter = MailerLetter::model();

            $settings = Setting::model()->findCodesValues();

            $sro = Sro::model()->findByPk( $this->object_id );

            $subject = $mailer_letter->compileText($settings[self::SETTING_MAIL_SUBJECT], array(
                'sroName' => $sro->name,
            ));

            $body = $mailer_letter->compileText($settings[self::SETTING_MAIL_BODY], array(
                'sroName' => $sro->name,
                'sroHref' => Yii::app()->createAbsoluteUrl('/sro/sro/view/', array('id' => $sro->id)),
                'commentText' => $this->text,
                'user' => User::model()->findByPk( Yii::app()->user->id ),
            ));

            MailHelper::send(Setting::getValue('admin_email'), $subject, $body);
        }

        return true;
    }


}