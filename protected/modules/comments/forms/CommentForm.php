<?php

return array(
    'activeForm' => array(
        'id'                   => 'comments-form',
        'class'                => 'CActiveForm',
        'enableAjaxValidation' => true,
    ),
    'elements'   => array(
        'text'         => array('type' => 'editor'),
        'is_published' => array(
            'type'  => 'dropdownlist',
            'items' => Comment::$states
        ),
    ),
    'buttons'    => array(
        'submit' => array(
            'type'  => 'submit',
            'value' => $this->model->isNewRecord ? 'Создать' : 'Сохранить'
        )
    )
);
