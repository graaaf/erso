<?php
$formElements =  array(
    'action'     => '/comments/comment/add',
    'activeForm' => array(
        'id'                   => 'comment-form',
        'class'                => 'CActiveForm',
        'htmlOptions'          => array(
            'enctype' => 'multipart/form-data'
        ),
        'enableAjaxValidation' => true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'validateOnChange'=>false,
        ),

    ),
    'elements'   => array(
        '<textarea name="Comment[text]"></textarea>',
        'model_name' => array(
            'type'  => 'hidden',
            'value' => $this->model->model_name,
        ),
        'object_id'  => array(
            'type'  => 'hidden',
            'value' => $this->model->object_id,
        ),
    ),
);


if( Yii::app()->user->isGuest )
{
    $formElements['elements'][] = '<p class="comment_form_hint">Для отправки комментария Вам необходимо <a href="/login">авторизироваться</a> или <a href="/registration">зарегистрироваться</a></p>';
}
else
{
    $formElements['buttons' ] = array(
        'submit' => array(
            'type'  => 'submit',
            'value' => 'Отправить',
            'class' => 'l-button',
        ),
    );
}


return $formElements;