<?php
$this->page_title = 'Управление';

$this->widget('AdminGrid', array(
    'id' => 'comments-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'name',
        'email',
        array(
            'name' => 'text',
            'type' => 'html',
            'value' => 'Yii::app()->text->cut($data->text, 190, "., -:;", " ...")',
        ),
        array('name' => 'is_published', 'value' => 'Comment::$states[$data->is_published]', 'filter' => Comment::$states),
        'date_created',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));