<?php

$this->page_title = 'Просмотр комментария';

$this->tabs = array(
    'Управление комментариями' => $this->createUrl("manage")
);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'name',
        'email',
        array(
            'name' => 'text',
            'type' => 'html',
            'value' => Yii::app()->text->cut($model->text, 190, "., -:;", " ..."),
        ),
        array(
            'name' => 'Прошел модерацию?',
            'value' => Comment::$states[$model->is_published]
        )
    ),
));
?>
