<?php

class CommentController extends BaseController
{

    public static function actionsTitles()
    {
        return array(
            'Add' => 'Добавление комментария',
            'Captcha' => 'Капча',
        );
    }


    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CaptchaAction',
            ),
        );
    }


    public function actionAdd()
    {
        $model = new Comment;
        $model->attributes = $_POST['Comment'];

        if ($model->save())
        {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else
        {
            $this->render('_error', array('model' => $model));
        }
    }


}