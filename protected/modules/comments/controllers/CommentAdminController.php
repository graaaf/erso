<?php

class CommentAdminController extends AdminController
{

    public static function actionsTitles()
    {
        return array(
            "Update" => "Редактирование комментария",
            "Delete" => "Удаление комментария",
            "Manage" => "Управление комментариями",
            "View" => "Просмотр комментария"
        );
    }

    public function actionManage()
    {
        $model = new Comment('search');
        $model->unsetAttributes();
        if (isset($_GET[get_class($model)]))
        {
            $model->attributes = $_GET[get_class($model)];
        }

        $this->render('manage', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        $form = new BaseForm('comments.CommentForm', $model);

        if ($form->submitted('submit'))
        {
            if ($model->save())
            {
                $this->redirect(array('view', 'id'=>$model->id));
            }
        }

        $this->render('update', array(
            'form' => $form,
        ));
    }

    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            Comment::model()->deleteByPk($id);

            if (!isset($_GET['ajax']))
            {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('manage'));
            }
        } else
        {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

}