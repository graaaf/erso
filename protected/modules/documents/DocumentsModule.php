<?php
class DocumentsModule extends WebModule
{
    public static $active = true;


    public static function name()
    {
        return 'Документы';
    }

    public function description()
    {
        return 'Документы, законодательство, техническое регулирование';
    }

    public function init()
    {
    $this->setImport(array(
           'documents.controllers.*',
           'documents.models.*',
           'documents.components.*',
        ));
    }
}