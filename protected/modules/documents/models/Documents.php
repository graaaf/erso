<?php

class Documents extends ActiveRecordModel
{

    public function name()
    {
        return 'Документы';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'documents';
    }

    public function rules()
    {
        return array(
            array('title, date, description', 'required'),
            array('category, file, activity, on_main, archive', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>300),
            array('id, title, category, date, description, file, activity, on_main', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id'          =>  'ID',
            'title'       =>  'Наименование документа',
            'category'    =>  'Категория',
            'date'        =>  'Дата',
            'description' =>  'Описание',
            'file'        =>  'Файл',
            'activity'    =>  'Активность',
            'on_main'     =>  'На главную',
        );
    }


    public function search()
    {

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('category',$this->category);
        $criteria->compare('date',$this->date,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('file',$this->file);
        $criteria->compare('activity',$this->activity);
        $criteria->compare('on_main',$this->on_main);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}