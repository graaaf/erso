<?php
class FileManagerController extends BaseController
{
    public static function actionsTitles()
    {
        return array(
            "DownloadFile" => "Скачать файл",
        );
    }


    public $x_send_file_enabled = false;


    public function actionDownloadFile($hash)
    {
        list($hash, $id) = explode('x', $hash);

        $model = FileManager::model()->findByPk(intval($id));

        if (!$model || $model->getHash() != $hash || !$model->getIsFileExist())
        {
            $this->pageNotFound();
        }

        if ($this->x_send_file_enabled)
        {
            Yii::app()->request->xSendFile($model->server_path, array(
                'saveName' => $model->file_name ? $model->file_name : $model->name,
                'terminate'=> false,
            ));
        }
        else
        {
            $this->request->sendFile('/' . $model->path . '/' . $model->name, $model->content);
        }
    }

}
