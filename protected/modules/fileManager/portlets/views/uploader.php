<div id="<?php echo $this->id ?>" class="uploader portlet-content">
    <div class="fileupload-buttonbar">
        <label class="fileinput-button">
            <span>Добавить файлы...</span>
            <input type="file" name="file" multiple>
        </label>
        <button type="submit" class="start">Начать загрузку</button>
        <?php if($this->data_type == "image"){?>
            <input type="checkbox" name="useWatermark" checked="true" value="true" id="use-watermark" />
            <label for="use-watermark" style="display: inline; padding-bottom: 10px">Применить водяной знак</label>
        <? }?>
    </div>
    <div id="<?php echo $this->id ?>-drop-zone" class="drop-zone">
        Перетащите сюда файлы
    </div>
    <div class="fileupload-content">
        <table class="files">
            <thead>
                <tr>
                    <th></th>
                    <?php foreach ($this->fields as $header): ?>
						<th><?php echo $header['header'];?></th>
					<?php endforeach; ?>
                    <th>Сортировка</th>
                    <th>Размер</th>
                    <th></th>
                    <th>Удалить</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <div class="fileupload-progressbar"></div>
    </div>
</div>
<style type="text/css">

.files{
    margin: 0 !important;
}


.files .title span, .files .descr span{border-bottom: 1px dashed #48f; cursor: pointer}
.files .title, .files .descr{min-width:250px; min-height: 50px;}
.files .title, .files .descr{min-width:250px; min-height: 50px;}
/*.files img{margin-top: 12px}*/

</style>
