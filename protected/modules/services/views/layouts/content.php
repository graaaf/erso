<?php $this->beginContent('//layouts/main'); ?>
<div class="sidebar-right services">
    <?php $this->widget('application.modules.banners.portlets.BannersList'); ?>
    <div class="part_title"><span>Последние</span><br/> свадьбы</div>
    <?php $this->widget('application.modules.portfolio.widgets.LastWorks'); ?>
    <div class="part_title"><span>Комментарии</span><br/> в блоге</div>
    <?php $this->widget('application.modules.blog.portlets.LastComments'); ?>
</div>
<div class="sidebar-left services">
    <?php
    $this->widget('zii.widgets.CBreadcrumbs', array(
        'homeLink' => CHtml::link('Главная', '/'),
        'separator' => '<span class="divider"></span>',
        'links' => $this->crumbs,
        'htmlOptions' => array(
            'class' => 'breadcrumb'
        )
    ));
    ?>
    <?php echo $content; ?>
</div>
<?php $this->endContent(); ?>