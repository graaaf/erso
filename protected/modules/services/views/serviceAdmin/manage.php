<?php

$this->widget('AdminGrid', array(
    'id' => 'service-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'sortable' => true,
    'columns' => array(
        'name',
        array(
            'name' => 'description',
            'type' => 'html',
            'value' => 'Yii::app()->text->cut($data->description, 190, "., -:;", " ...")',
        ),
        array('name' => 'is_published', 'value' => 'Service::$states[$data->is_published]', 'filter' => Service::$states),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
