<?php
$this->tabs = CMap::mergeArray(array(
            'Редактрование услуги' => $this->url('update', array('id' => $model->id)),
                ), $this->tabs);

$this->widget('DetailView', array(
    'data' => $model,
    'attributes' => array(
        'name',
        array(
            'name' => 'description',
            'type' => 'html',
            'value' => Yii::app()->text->cut($model->description, 390, "., -:;", " ..."),
        ),
        array(
            'name' => 'image',
            'value' => '<img src="/' . Service::UPLOAD_DIR . '/' . $model->image . '">',
            'type' => 'html'
        )
    ),
));