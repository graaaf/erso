<?php $this->crumbs = array('Услуги'); ?>

<div class="part_title">Услуги</div>
<table class="service_list" width="100%" cellpadding="0" cellspacing="0">
    <?php foreach ($services as $service): ?>
        <?php $this->renderPartial('_view', array('model' => $service)); ?>
    <?php endforeach; ?>
</table>