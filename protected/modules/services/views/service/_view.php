<tr>
    <td class="photo">
        <a href="<?php echo $model->href; ?>">
            <img src="/<?php echo Service::UPLOAD_DIR . '/' . $model->image; ?>" alt="<?php echo $model->name; ?>" />
        </a>
    </td>
    <td>
        <a href="<?php echo $model->href; ?>"><?php echo $model->name; ?></a>
        <p>
            <?php echo preg_replace('#<img.*?>#is', '', Yii::app()->text->cut($model->description, 150, "., -:;", " ...")); ?>
        </p>
    </td>
</tr>