<?php
    $this->page_title = $model->name;
    $this->crumbs = array('Услуги' => '/uslugi', $model->name);
?>

<h1 class="part_title"><?php echo $model->name; ?></h1>
<table class="service_list full" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <p>
                <?php echo $model->description; ?>
            </p>
        </td>
    </tr>
</table>

<?php
    $this->widget('fileManager.portlets.FileList', array(
        'model' => $model,
        'tag' => 'files'
    ));
?>