<?php

class Service extends ActiveRecordModel
{

    const UPLOAD_DIR = 'upload/services/icons';
    const STATE_PUBLISHED = 1;
    const STATE_NOT_PUNLISHED = 0;

    public static $states = array(
        self::STATE_PUBLISHED => 'Да',
        self::STATE_NOT_PUNLISHED => 'Нет',
    );

    public function name()
    {
        return 'Услуги';
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
                    'FileManager' => array('class' => 'application.components.activeRecordBehaviors.AttachmentBehavior', 'attached_model' => 'FileManager'),
                    'sortable' => array('class' => 'ext.sortable.SortableBehavior'),
                    'MetaTag' => array('class' => 'application.components.activeRecordBehaviors.MetaTagBehavior'),
                ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return 'services';
    }


    public function rules()
    {
        return array(
            array('name, description', 'required'),
            array('is_published, url', 'safe'),
            array('image', 'file', 'types' => 'jpg, jpeg, png, gif', 'allowEmpty' => false, 'on' => 'create'),
            array('image', 'file', 'types' => 'jpg, jpeg, png, gif', 'allowEmpty' => true, 'on' => 'update'),
            array('name, description, is_published', 'safe', 'on' => 'search'),
        );
    }


    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('is_published', $this->is_published, true);
        $criteria->order = '`order` DESC';
        return new ActiveDataProvider(get_class($this), array(
                    'criteria' => $criteria
                ));
    }


    public function relations()
    {
        return array(
            'files' => array(
                self::HAS_MANY,
                'FileManager',
                'object_id',
                'condition' => 'files.model_id = "' . get_class($this) . '" AND files.tag="files"',
                'order' => 'files.order DESC'
            )
        );
    }


    public function uploadFiles()
    {
        return array(
            'image' => array(
                'dir' => self::UPLOAD_DIR
            )
        );
    }


    public function getHref()
    {
        if ($this->url != '')
            return "/uslugi/{$this->url}";
        return Yii::app()->controller->createUrl("/services/service/view", array('id' => $this->id));
    }


}
