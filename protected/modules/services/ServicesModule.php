<?php

class ServicesModule extends WebModule
{
    public static $active = false;

    public static function name()
    {
        return 'Услуги';
    }

    public static function description()
    {
        return 'Перечень оказываемых услуг';
    }

    public static function version()
    {
        return '1.0';
    }

    public function init()
    {
        $this->setImport(array(
            'services.models.*',
            'services.components.*',
        ));
    }

    public static function adminMenu()
    {
        return array(
            'Управление услугами' => '/services/serviceAdmin/manage',
            'Добавить услугу' => '/services/serviceAdmin/create',
        );
    }

}
