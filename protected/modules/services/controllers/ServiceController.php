<?php

class ServiceController extends BaseController
{

    public $layout = '/layouts/content';

    public static function actionsTitles()
    {
        return array(
            'Index' => 'Список услуг',
            'View' => 'Просмотр услуги',
        );
    }


    public function actionIndex()
    {
        $services = Service::model()->published()->ordered()->findAll();

        $this->render('index', array(
            'services' => $services,
        ));
    }


    public function actionView()
    {
        if (isset($_GET['id'])) {
            $model = $this->loadModel($_GET['id']);
        }
        elseif (isset($_GET['url'])) {
            $model = $this->loadModel($_GET['url'], array(), 'url');
        }
        
        $this->render('view', array(
            'model' => $model,
        ));
    }


}