<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Admin
 * Date: 26.09.12
 * Time: 22:44
 * To change this template use File | Settings | File Templates.
 */
class HtmlHelper
{
    public static function timeTag($date, $dateFormat='d MMMM y года, H:mm', $htmlOptions, $timeAttributeFormat='y-MM-dThh:mm:ssZ')
    {
        $htmlOptions = array_merge(
            array(
                'datetime' => Yii::app()->dateFormatter->format($timeAttributeFormat, $date)
            ),
            $htmlOptions
        );

        return CHtml::tag(
            'time',
            $htmlOptions,
            Yii::app()->dateFormatter->format($dateFormat, $date)
        );
    }
}
