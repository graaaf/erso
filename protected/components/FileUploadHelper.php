<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Admin
 * Date: 21.10.12
 * Time: 1:10
 * To change this template use File | Settings | File Templates.
 */
class FileUploadHelper
{
    /**
     * Returns converted relation path to absolute path,
     * and create directory if it not exists
     *
     * relation path: /upload/face
     * absolute path: /user/www/site.com/upload/face
     *
     */
    public static function relToAbsPathDir( $dir )
    {
        // Delete first slash
        if( substr($dir, 0, 1) == '/' )
        {
            $dir = substr( $dir, 1 );
        }

        // Concat 'Document root' and uploaded pats
        if( substr( $dir, 0, strlen($_SERVER['DOCUMENT_ROOT'])) != $_SERVER['DOCUMENT_ROOT'] )
        {
            $dir = $_SERVER['DOCUMENT_ROOT'] . $dir;
        }

        // Add slash at the end
        if( substr($dir, -1) != '/' )
        {
            $dir.= '/';
        }

        // Create dir if it not exists
        if( !is_dir($dir) )
        {
            mkdir( $dir, 0777);
        }

        return $dir;
    }


    public static function relToAbsPathFile( $file )
    {
        // Delete first slash
        if( substr($file, 0, 1) == '/' )
        {
            $file = substr( $file, 1 );
        }

        // Concat 'Document root' and uploaded pats
        if( substr( $file, 0, strlen($_SERVER['DOCUMENT_ROOT'])) != $_SERVER['DOCUMENT_ROOT'] )
        {
            $file = $_SERVER['DOCUMENT_ROOT'] . $file;
        }

        // Return false if file isn't exist
        if( !is_file($file) )
        {
            return false;
        }

        return $file;
    }


}
