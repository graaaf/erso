<?php

class MailHelper
{
    public static function send($emailTo, $subject='', $body='', $emailFrom='')
    {
        // Send message
        $headers = "MIME-Version: 1.0\r\nFrom: $emailFrom\r\nReply-To: $emailTo\r\nContent-Type: text/html; charset=utf-8";
        $message = str_replace("\n.", "\n..", $body);
        return mail($emailTo,'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);
    }
}

?>