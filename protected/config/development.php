<?php
return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
        array(
        'components' => array(
            'db' => array(
		        'connectionString' => 'mysql:host=localhost;dbname=esro_site;',
		        'emulatePrepare'   => true,
		        'username'         => 'root',
		        'password'         => 'development',
		        'charset'          => 'utf8',
		        'enableProfiling'  => true,
	        ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'        => 'CFileLogRoute',
                        'levels'       => 'trace, error, warning, info',
                        'enabled'      => true
                    )
                ),
            ),
	    ),
    )
);

