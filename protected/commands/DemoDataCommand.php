<?php

class DemoDataCommand extends CConsoleCommand
{

    public function actionIndex()
    {
        for ($i = 0; $i < 50; $i++) {
            $sro = Sro::model()->findByPk($i);
            if (!$sro) {
                $sro = new Sro();
                $sro->id = $i;
            }
            $sro->category_id = 1 + $i % 3;
            $sro->name =
                'Саморегулируемая организация «Объединение организаций'
                    . ' выполняющих инженерные изыскания при архитектурно строительном'
                    . ' проектировании, строительстве, реконструкции, капитальном ремонте'
                    . ' объектов атомной отрасли «СОЮЗАТОМГЕО» ' . $i;

            $sro->city_id = 1 + ($i % 3);

            $sro->register_date = '2002-' . (1 + $i % 11) . '-' . (1 + $i % 28);

            $sro->members_count = ceil(rand(0, 10000));

            if (!$sro->save()) {
                throw new \Exception(var_export($sro->getErrors(), true));
            }
        }

    }

}