<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo ($this->meta_title == null) ? $this->page_title : $this->meta_title; ?></title>

    <meta name="description" content="<?php echo $this->meta_description ?>"/>
    <meta name="keywords" content="<?php echo $this->meta_keywords ?>"/>

    <?php
    $cs = Yii::app()->clientScript;
    $cs->registerCssFile('/css/slider.css');
    $cs->registerCssFile('/css/jquery-ui-1.8.22.custom.css');
    $cs->registerCssFile('/css/jClever.css');
    $cs->registerCssFile('/css/pager.css');
    $cs->registerCssFile('/css/main.css');
    $cs->registerCoreScript('jquery');
    $cs->registerScriptFile('/js/jquery-ui-1.8.22.custom.min.js');
    $cs->registerScriptFile('/js/placeholder.js');
    $cs->registerScriptFile('/js/slides.min.jquery.js');
    $cs->registerScriptFile('/js/lib.jquery.js');
    $cs->registerScriptFile('/js/script.js');
    $cs->registerScriptFile('/js/jClever.js');
    $cs->registerScriptFile('/js/plugins/imageMask/imageMask.js');
    $cs->registerScriptFile('/js/plugins/jTruncate/jTruncate.js');
    $cs->registerScriptFile('//yandex.st/share/share.js');
    ?>

    <title><?php if(!empty($this->page_title)) echo CHtml::encode($this->page_title) . ' | eSRO.ru';?></title>
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if IE 9]>
        <style type="text/css">.g-main_menu a { padding:9px 13px 0; } </style>
    <![endif]-->
</head>
<body>
<!-- MASK -->
<div id="mask" class="g-mask" style="height: 2361px; display: none;"></div>

<!-- MODAL WINDOW -->
<div id="modal_window" class="g-modal_wrap" style="margin-top: 44.5px; display: none;">
    <div class="g-modal">
        <h4>Ваш вопрос был успешно отправлен.</h4>
        <a href="#" id="modal_close" class="g-modal_close"></a>
        <div class="g-clear_fix"></div>
        <p>	В ближайшее время мы ответим на Ваш вопрос и он появиться
            на сайте. Оставьте Ваш e-mail, что бы мы могли Вас уведомить
            о публикации ответа.
        </p>
        <input type="email" id="user_email" name="Faq[email]" form="faq-form" placeholder="esro@info.ru">
        <button id="modal_submit" class="l-button">
            Уведомить
        </button>
        <footer>
            <p class="error" style="display: none;"></p>
            <a href="">Задать еще вопрос</a>
        </footer>
    </div>
</div>

<div class = "g-container">
    <header class = "g-header">
        <div class = "g-header_top_wrap">
            <div class = "g-header_top g-site_width">
                <?php $this->widget('application.modules.banners.portlets.BannerWidget', array('position' => 'top')); ?>
            </div>
        </div>
        <div class = "g-header_mid_wrap">
            <div class = "g-header_mid g-site_width">
                <section class = "g-logo">
                    <a href = "/"><img src = "/images/logo.png" alt="" /></a>
                </section>
                <section class = "g-slogan">
                    <p>САМОРЕГУЛИРОВАНИЕ</p>
                    <p>Строительство</p>
                    <p>Проектирование</p>
                    <p>Изыскания</p>
                </section>
                <section class = "g-login">
                    <p>Вход в личный кабинет</p>
                    <form action = "#">
                        <div class = "g-login_links">
                            <?php if( Yii::app()->user->isGuest ): ?>
                                <img src = "/images/enter_icon.png" />
                                <a href = "<?php echo Yii::app()->createUrl('users/user/login'); ?>">Вход</a>
                                <span class="guest_separator"></span>
                                <a href = "<?php echo Yii::app()->createUrl('users/user/registration'); ?>">Регистрация</a>
                            <?php else: ?>
                                <a href = "<?php echo Yii::app()->createUrl('users/user/profile'); ?>">Личный кабинет</a>
                                <span class="logged_separator"></span>
                                <a href = "<?php echo Yii::app()->createUrl('users/user/logout'); ?>">Выход</a>
                            <?php endif; ?>
                        </div>
                    </form>
                </section>
                <section class = "g-search">
                    <form action = "#">
                        <a class = "search_dropdown" href = ""><img src = "/images/search_arrow_down.png"/></a>
                        <input type = "text" name = "" placeholder = "Поиск по сайту" />
                        <button></button>
                        <div class = "g-clear_fix"></div>
                        <section class = "g-search_advance">
                            <nav>
                                <a href = "">По документам</a>
                                <a href = "">По реестру СРО</a>
                            </nav>
                        </section>
                    </form>
                </section>
            </div>
        </div>
        <div class = "g-main_menu_wrap">
            <menu class = "g-main_menu g-site_width">
                <li><?php echo CHtml::link('О СРО', array('/content/page/view/id/87')); ?></li>
                <li><?php echo CHtml::link('Новости', array('/news/news/index')); ?></li>
                <li><?php echo CHtml::link('Статьи и интервью', array('/content/page/index')); ?></li>
                <li><?php echo CHtml::link('Реестр СРО', array('/sro/sro/index')); ?></li>
                <li><a href = "">Законодательство</a></li>
                <li><a href = "">Техническое регулирование</a></li>
                <li><a href = "">Документы</a></li>
                <li><?php echo CHtml::link('Вопрос-ответ', array('/faq/faq/index')); ?></li>
                <li><?php echo CHtml::link('Лица', array('/face/face/index'), array('class' => 'h-main_menu_last')); ?></li>
            </menu>
        </div>
    </header>
    <div class = "g-clear_fix"></div>
    <div class = "g-content g-site_width">
        <?php echo $content; ?>
    </div><!--end g-content-->
    <div class = "g-clear_fix"></div>
    <footer class = "g-footer_wrap">
        <div class = "g-footer g-site_width">
            <section class = "g-footer_mailer">
                <form>
                    <h3>Подписка</h3>
                    <input type = "text" placeholder = "Ваш e-mail" />
                    <button class = "l-button">Ok</button>
                    <p>
                        © eSRO 2008 - 2012 <br/>
                        Использование информации с сайта без разрешения запрещено
                    </p>
                </form>
            </section>
            <section class = "g-footer_soc">
                <ul>
                    <li>
                        <img src = "/images/rss_icon.png" />
                        <a href = "">Наш RSS канал</a>
                    </li>
                    <li>
                        <img src = "/images/twitter_icon.png" />
                        <a href = "">Мы в Twittere</a>
                    </li><li>
                    <img src = "/images/fb_icon.png" />
                    <a href = "">Наша страница на Facebook</a>
                </li><li>
                    <img src = "/images/google_icon.png" />
                    <a href = "">В Google+</a>
                </li>
                </ul>
            </section>
            <section class = "g-footer_menu">
                <menu>
                    <li><a href = "">О СРО </a></li>
                    <li><a href = "">Новости</a></li>
                    <li><a href = "">Статьи и интервью</a></li>
                    <li><a href = "">Реестр СРО</a></li>
                    <li><a href = "">Законодательство</a></li>
                </menu>
            </section>
            <section class = "g-footer_menu g-footer_menu_margin">
                <menu>
                    <li><a href = "">Техническое регулирование</a></li>
                    <li><a href = "">Документы</a></li>
                    <li><a href = "">Вопрос-ответ</a></li>
                    <li><a href = "">Лица</a></li>
                    <li><a href = "">Регистрация / Вход</a></li>
                </menu>
            </section>
            <div class = "g-clear_fix"></div>
        </div>
    </footer>
</div>
</body>
</html>