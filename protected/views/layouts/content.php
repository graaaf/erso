<?php $this->beginContent('//layouts/main'); ?>
<div class = "c-left_column">
    <?php echo $content; ?>
</div>
<div class = "c-right_column">

    <?php $this->widget('application.widgets.RSSWidget.RSSWidget'); ?>

    <div class = "l-right">
        <header>
            <h3>Реестр СРО</h3>
            <div class = "g-clear_fix"></div>
        </header>
        <section class = "l-right_content b-reestr">
            <?php $this->widget('application.modules.sro.portlets.SroStatWidget'); ?>
        </section>
    </div>

    <div class = "l-right b-question">
        <header>
            <h3>Задать вопрос эксперту:</h3>
            <div class = "g-clear_fix"></div>
        </header>

        <section class = "l-right_content">

            <?php $form=$this->beginWidget('CActiveForm', array(
                'action' => $this->createUrl('/faq/faq/create'),
                'enableAjaxValidation' => true,
                'htmlOptions' => array(
                    'class' => 'faq-form',
                ),
            )); ?>

                <?php echo $form->textArea(new Faq(), 'question', array(
                    'placeholder' => 'Возможно ли частному лицу вступить в СРО?',
                )); ?>

                <div class = "b-question_ask">
                    <div class = "b-hint">
                        <header></header>
                        <section>
                            <p>
                                Через данную форму Вы можете
                                задать свой вопрос, а наши
                                высококвалифицированные юристы
                                ответят на него в кратчайшие сроки.
                            </p>
                        </section>

                    </div>
                    <img src = "/images/question_icon.png" />
                    <?php echo CHtml::link('Вопрос-ответ', array('/faq/faq/index')); ?>

                </div>
                <input class="l-button native_submit" type="submit" name="send" value="Отправить">

            <?php $this->endWidget(); ?>
            <div class = "g-clear_fix"></div>
        </section>
    </div>

    <div class = "l-right b-right_event">
        <header>
            <h3>Мероприятия</h3>
            <span class = "l-view_all">
                <img src = "/images/plus_icon.png"/>
                <a href = "">Все</a>
            </span>
            <div class = "g-clear_fix"></div>
        </header>
        <section class = "l-right_content">
            <article class = "b-right_content_event1">
                <img src = "/images/PARTI_IMG.jpg" />
                <section class = "l-view_more_text">
                    <p>
                        III Всероссийская научно-практическая конференция
                        <a href = ""><img src = "/images/more_arrow.png"/></a>
                    </p>
                </section>
                <div class = "g-clear_fix"></div>
                <time class = "right_event_date1" datetime = "">17 июня 2012</time>
                <div class = "g-clear_fix"></div>
            </article>
            <article>
                <img src = "/images/PARTI_IMG.jpg" />
                <section class = "l-view_more_text">
                    <p>
                        Заседание
                        Общественного
                        совета ЦК КПРФ
                        <a href = ""><img src = "/images/more_arrow.png"/></a>
                    </p>
                </section>
                <div class = "g-clear_fix"></div>
                <time class = "right_event_date2" datetime = "">17 июня 2012</time>
                <div class = "g-clear_fix"></div>
            </article>
            <article>
                <img src = "/images/PARTI_IMG.jpg" />
                <section class = "l-view_more_text">
                    <p>
                        Комитет по земель-
                        ным отношениям и
                        строительству ГД
                        проводит парламентские слушания на тему: «Перспективы и проблемы деятельности саморегулируемых организаций в строительной области. Основные направления совершенствования законодательства»
                        <a href = ""><img src = "/images/more_arrow.png"/></a>
                    </p>
                </section>
                <div class = "g-clear_fix"></div>
                <time class = "right_event_date3" datetime = "">17 июня 2012</time>
                <div class = "g-clear_fix"></div>
            </article>
        </section>
    </div>

    <div class = "l-right b-what_read">
        <header>
            <h3>Что читать?</h3>
            <span class = "l-view_all">
                <img src = "/images/plus_icon.png"/>
                <a href = "">Ещё</a>
            </span>
            <div class = "g-clear_fix"></div>
        </header>
        <section class = "l-right_content">
            <ol type = "1">
                <li>
                    <a href = "">
                        Техническое регулирование.
                        Строительство,
                        проектирование, изыскания.
                    </a>
                </li>
                <li>
                    <a href = "">
                        Техническое регулирование.
                        Строительство,
                        проектирование, изыскания.
                    </a>
                </li>
                <li class = "h-last">
                    <a href = "" >
                        Техническое регулирование.
                        Строительство,
                        проектирование, изыскания.
                    </a>
                </li>
            </ol>
        </section>
    </div>

    <div class = "l-right b-right_close">
        <header>
            <h3>Закрытые материалы</h3>
            <div class = "g-clear_fix"></div>
        </header>
        <section class = "l-right_content">
            <ul>
                <li>
                    <span>-</span>
                    <a href = "">Материалы проверок СРО Ростехнадзором</a>
                </li>
                <li>
                    <span>-</span>
                    <a href = "">Материалы проверок СРО Ростехнадзором</a>
                </li>
                <li class = "h-last">
                    <span>-</span>
                    <a href = "">Материалы проверок СРО Ростехнадзором</a>
                </li>
            </ul>
            <span class = "l-view_all">
                <img src = "/images/plus_icon.png"/>
                <a href = "">Все материалы</a>
            </span>
            <div class = "g-clear_fix"></div>
        </section>
    </div>

    <div class = "l-right">
        <section class = "l-right_content">
            <img src = "/images/FACEBOOK_API.jpg"/>
        </section>
    </div>
    <div class = "b-banners">
        <?php $this->widget('application.modules.banners.portlets.BannerWidget', array('position' => 'right1')); ?>
        <?php $this->widget('application.modules.banners.portlets.BannerWidget', array('position' => 'right2')); ?>
    </div>
</div><!--end c-right_column-->
<?php $this->endContent(); ?>