<?php
$this->widget(
        'LinkPager', array(
    'pages' => $pages,
    'cssFile' => false,
    'header' => '',
    'footer' => '',
    'firstPageLabel' => '',
    'lastPageLabel' => '',
    'nextPageLabel' => 'Следующая >>',
    'prevPageLabel' => '<< Предыдущая',
    'htmlOptions' => array(
        'class' => 'pager'
    )
));