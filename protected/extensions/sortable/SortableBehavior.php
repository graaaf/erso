<?php
class SortableBehavior extends CActiveRecordBehavior
{
    //заполняем айдишниками
    public function fillOrderColumn($column)
    {
        $model = $this->getOwner();
        $c     = Yii::app()->db->commandBuilder->createSqlCommand(
            "UPDATE ".$model->tableName()." AS t SET t.{$column} = t.id");

        $c->execute();
    }

    public function setPositions($ids, $column, $start)
    {

        $model = $this->getOwner()->model();
        $table = $model->tableName();
        $in = SqlHelper::in('id', $ids, 't');

        // получаем из базы order для заданных id и переупорядочиваем согласно переданных ids
        $orders = Yii::app()->db->createCommand()->select($column)->from("$table t")->where($in)->order($column)->queryColumn();

        $newOrders = array();
        $i = 0;
        foreach($ids as $id)
        {
            $newOrders[$id] = $orders[$i];
            $i++;
        }

        // сохраняем
        $case = SqlHelper::arrToCase('id', $newOrders, $model->getTableAlias());
        $c = Yii::app()->db->commandBuilder->createSqlCommand("UPDATE {$table} AS t SET t.{$column} = {$case} WHERE {$in}");
        $c->execute();
    }

    public function beforeSave($event)
    {
        $model = $this->getOwner();

        if ($model->isNewRecord)
        {
            $column         = 'order';
            $i              = $model->max($column);
            $model->$column = ++$i;
        }
    }

}