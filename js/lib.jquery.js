//Функция центрирования
$.fn.center = function () {
	var height = $(this).height();
	if($(window).height() > height) {
		var heightDiff = ($(window).height() - height)/2;
		$(this).css({'margin-top': heightDiff});
	
	}
	
	return this;
}