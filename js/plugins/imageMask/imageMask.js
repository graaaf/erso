(function($){


    // Define plugin
    $.fn.maskable = function (opts) {
        var options = $.extend({
            maskSrc: '/path/to/mask.png'
        }, opts);

        $(this).each(function () {
            var $img = $(this);

            $img
            .css('background', 'url(' + $img.attr('src') + ') center center no-repeat')
            .css('background-size', '100%')
            .attr('src', options.maskSrc);
        });
    }

    // Run plugin
    $('.maskable').ready(function(){
        $('.maskable').maskable({ maskSrc: '/images/people_face_mask_not_transparent.png' });
    });

})(jQuery);

