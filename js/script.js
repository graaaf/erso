$(document).ready(function() {
    $('input[placeholder], textarea[placeholder]').placeholder();

    $('#b-slider').slides({
        preload: true,
        preloadImage: 'images/loading.gif',
        effect: 'slide, fade',
        crossfade: true,
        slideSpeed: 350,
        fadeSpeed: 500,
        generateNextPrev: false,
        generatePagination: false,
        animationComplete: function(current) {
            var eq = current - 1;
            $('.b-slider_pagination li').removeClass('h-selected');
            $('.b-slider_pagination li').eq(eq).addClass('h-selected');
        }
    });
    $('.b-question_ask img').hover( function() {
            $('.b-hint').stop(true, true).fadeIn(500);
        },
        function() {
            $('.b-hint').fadeOut(500);
        });
    // Tabs
    $('#tabs').tabs();
    $('.b-tabs_wrap li').hover(function() {
            $('a', this).css({'text-decoration':'underline', 'color': '#0069aa'});
            $('.tabs_list_number', this).css({'background':'#fff', 'color': '#0069aa'});
        },
        function() {
            $('a', this).css({'text-decoration':'none', 'color': '#262626'});
            $('.tabs_list_number', this).css({'background':'#E6EAEE', 'color': '#262626'});
        })
    //Modal
    $('.g-modal_wrap').center();
    $('.g-mask').height($(document).height());


    /*************** FAQ FORM ***************/

    // Define fade speed
    fadeSpeed = 100;

    // Handler native submit button
    $(".native_submit").click(function(){

        // Prevent send empty form
        if( $.trim( $(this).siblings('textarea').val() ) == '' )
        {
            return false;
        }

        // Add ID to form
        $(this).parents('form').attr('id', 'faq-form');

        // Show mask
        $('#mask').fadeIn(fadeSpeed);

        // Show modal window
        $('#modal_window').fadeIn(fadeSpeed);

        return false;
    });

    // Handler modal submit button
    $("#modal_submit").click(function(){

        $(".error").hide();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        var emailaddressVal = $("#user_email").val();
        if(emailaddressVal == '')
        {
            $(".error").text('Введите e-mail адрес.').show();
            return false;
        }
        else if(!emailReg.test(emailaddressVal))
        {
            $(".error").text('Вы ввели некорректный e-mail адрес.').show();
            return false;
        }

        $('#faq-form').submit();
        return false;
    });

    // Close modal window button handler
    $('#modal_close').click(function(){
        $('#faq-form').submit();
        $('#mask, #modal_window').hide();
        return false;
    });


})