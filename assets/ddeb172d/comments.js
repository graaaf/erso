function quoteComment(id) {
    redactor.setHtml('<blockquote>' + $("#comment_" + id + "_text").html() + '</blockquote>\n<p><br/></p>');//"<blockquote><p>" + $("#comment_" + id + "_text").html() + "</p></blockquote><p><br></p>");
    alert($("#comment_" + id + "_text").html());
}


(function($){
    $.fn.commentForm = function(options) {
        var options = $.extend({
            /**
             * Элемент на странице, в который будут вставляться данные о оставшихся символах
             */
            maxCharsContainer : '#comment_text_count',
            
            /**
             * Максимальное число символов
             */
            maxChars : 2000,
            
            /**
             * Процент, после которого будет отобраться счетчик
             */
            maxCharsPercent : 75
        }, options);
        
        var minCharsForVisible = options.maxChars / 100 * options.maxCharsPercent;
        
        var redactor = $(this).redactor({
            toolbar : 'mini',
            change : function() {
                if (redactor.getHtml().length >= minCharsForVisible) {
                    var charsLeft = options.maxChars - redactor.getHtml().length;
                    $(options.maxCharsContainer).html("Осталось " + charsLeft + " символов");
                    $(options.maxCharsContainer).show();
                }
            }
        }); 
        
        $(this).addClass('comment_editor').data('redactor', redactor);
    };
    
    $.fn.commentForm.quote = function(id) {
        var redactor = $('.comment_editor').data('redactor');
        redactor.setHtml('<blockquote>' + $("#comment_" + id + "_text").html() + '</blockquote>\n<p><br/></p>');
    };
})(jQuery);