$(function()
{
    function processFilters() {
        var toggle = function() {
            if ($('.search_span').is(':visible')) {
                $('.search_filters').show();
                $('.search_span').hide();
            }
            else {
                $('.search_filters').hide();
                $('.search_span').show();
            }
        }

        $('#toggle-search').click(function(e) {
            e.preventDefault();
            toggle();
        });

        $('.search_filters').hide();
        // query params for detailed filters
        if (window.location.search.substring(1).match(/search%5Bregister_number/)) {
            toggle();
        }
    }
    // processFilters();

    $('#sro-sort_table tbody tr td').click(function() {
        var link = $(this).find('a');
        if (link) {
            document.location = link.attr('href');
        }
    });
});
